For information about what this program does, visit: https://gwheel.pages.dev/
# Build Prerequisites
## All Platforms
- Rust and Cargo https://www.rust-lang.org/
- Cap'n Proto https://capnproto.org/index.html

## Windows
- vJoy SDK https://sourceforge.net/projects/vjoystick/

# Building
## Windows
Set the `VJOY_LIB_DIR` environment variable to the location of the libraries found in the vJoy SDK. For example, if
the target is 64-bit, set it to `{SDK_PATH}/lib/amd64` where `vJoyInterface.lib` is found.
```powershell
# Powershell example
$env:VJOY_LIB_DIR = "$SDK_PATH/lib/amd64"
```
## All Platforms
Set `APP_DATA_DIR` environment variable to change the path of the application's data directory. The path can be either
relative or absolute (relative to the executable's path at runtime).

For example, if the data directory will be put next to the the executable, then you must set the variable like the
following.
```sh
# Linux bash example
export APP_DATA_DIR=data
```
```powershell
# Windows powershell example
$env:APP_DATA_DIR = "data"
```
Finally, enter the following command.
```console
cargo build --release
```
# Post Build
## All Platforms
Move the data directory to its correct place if necessary.
## Windows
Either copy the DLL library `vJoyInterface.dll` next to the executable, or add its path to the `PATH` environment
variable. Be careful not to copy the DLL with the wrong architecture (32/64-bit).
