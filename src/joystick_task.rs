use std::time::Duration;

use tokio::sync::mpsc::{Receiver, Sender};

use crate::config::js_script::JoystickScriptConfig;
use crate::config::{AxisConfig, AxisUpdate, JoystickScriptUpdate, Update};
use crate::input_state::InputState;
use crate::message::{JoystickMessage, Message};
use crate::{error_message_chain, i18n};

use crate::controller::JoystickController;

enum State {
    Initializing(InitializingState),
    Uploading(UploadingState),
    Exit,
}

struct InitializingState {
    app_tx: Sender<Message>,
    js_rx: Receiver<JoystickMessage>,
    axis_config: AxisConfig,
    js_config: JoystickScriptConfig,
}

struct UploadingState {
    init: InitializingState,
    controller: JoystickController,
}

pub async fn run_task(
    axis_config: AxisConfig,
    js_config: JoystickScriptConfig,
    app_tx: Sender<Message>,
    js_rx: Receiver<JoystickMessage>,
) {
    let mut state = State::Initializing(InitializingState {
        app_tx,
        js_rx,
        axis_config,
        js_config,
    });
    loop {
        state = match state {
            State::Initializing(state) => initialize(state).await,
            State::Uploading(state) => upload(state).await,
            State::Exit => break,
        }
    }
}

async fn initialize(mut state: InitializingState) -> State {
    let controller = match JoystickController::new(&state.js_config) {
        Ok(dev) => dev,
        Err(err) => {
            send_info_message(&state.app_tx, error_message_chain(&Box::new(err)));
            match tokio::time::timeout(Duration::from_secs(2), state.js_rx.recv()).await {
                Ok(Some(msg)) => match msg {
                    JoystickMessage::UpdateConfig(update) => {
                        handle_config_update(update, &mut state.axis_config, &mut state.js_config);
                    }
                    JoystickMessage::Exit => {
                        return State::Exit;
                    }
                    _ => {}
                },
                _ => {}
            }
            return State::Initializing(state);
        }
    };
    send_info_message(&state.app_tx, i18n::working().to_owned());
    return State::Uploading(UploadingState {
        init: state,
        controller,
    });
}

async fn upload(mut state: UploadingState) -> State {
    if let Some(message) = state.init.js_rx.recv().await {
        match message {
            JoystickMessage::UpdateConfig(update) => {
                if handle_config_update(
                    update,
                    &mut state.init.axis_config,
                    &mut state.init.js_config,
                ) {
                    return State::Initializing(state.init);
                }
            }
            JoystickMessage::UpdateJoystick(mut data) => {
                adjust_all_axis(&mut data, &state.init.axis_config);
                if let Err(err) = state.controller.update(data.clone()) {
                    send_info_message(&state.init.app_tx, error_message_chain(&err));
                    return State::Initializing(state.init);
                }
                send_data_message(&state.init.app_tx, data);
                return State::Uploading(state);
            }
            JoystickMessage::Exit => return State::Exit,
        }
    }
    return State::Uploading(state);
}

fn send_info_message(app_tx: &Sender<Message>, msg: String) {
    let _ = app_tx.try_send(Message::UpdateJoystickStatus(msg));
    fltk::app::awake();
}

fn send_data_message(app_tx: &Sender<Message>, data: InputState) {
    let _ = app_tx.try_send(Message::UpdateData(data));
    fltk::app::awake();
}

/// Returns true if the task should restart
fn handle_config_update(
    update: Update,
    axis_config: &mut AxisConfig,
    js_config: &mut JoystickScriptConfig,
) -> bool {
    match update {
        Update::Full(val) => {
            axis_config.wheel.start = val.axis.wheel.start;
            axis_config.wheel.end = val.axis.wheel.end;
            axis_config.wheel.linearity = val.axis.wheel.linearity;

            axis_config.gas.start = val.axis.gas.start;
            axis_config.gas.end = val.axis.gas.end;
            axis_config.gas.linearity = val.axis.gas.linearity;

            axis_config.brake.start = val.axis.brake.start;
            axis_config.brake.end = val.axis.brake.end;
            axis_config.brake.linearity = val.axis.brake.linearity;

            axis_config.thumb_x.start = val.axis.thumb_x.start;
            axis_config.thumb_x.end = val.axis.thumb_x.end;
            axis_config.thumb_x.linearity = val.axis.thumb_x.linearity;

            axis_config.thumb_y.start = val.axis.thumb_y.start;
            axis_config.thumb_y.end = val.axis.thumb_y.end;
            axis_config.thumb_y.linearity = val.axis.thumb_y.linearity;
            let mut must_restart = false;
            if js_config.enabled != val.js_script.enabled {
                must_restart = true;
                js_config.enabled = val.js_script.enabled;
            }
            if js_config.path != val.js_script.path {
                must_restart = true;
                js_config.path = val.js_script.path;
            }
            return must_restart;
        }
        Update::Axis(AxisUpdate::WheelStart(val)) => axis_config.wheel.start = val,
        Update::Axis(AxisUpdate::WheelEnd(val)) => axis_config.wheel.end = val,
        Update::Axis(AxisUpdate::WheelLinearity(val)) => axis_config.wheel.linearity = val,
        Update::Axis(AxisUpdate::GasStart(val)) => axis_config.gas.start = val,
        Update::Axis(AxisUpdate::GasEnd(val)) => axis_config.gas.end = val,
        Update::Axis(AxisUpdate::GasLinearity(val)) => axis_config.gas.linearity = val,
        Update::Axis(AxisUpdate::BrakeStart(val)) => axis_config.brake.start = val,
        Update::Axis(AxisUpdate::BrakeEnd(val)) => axis_config.brake.end = val,
        Update::Axis(AxisUpdate::BrakeLinearity(val)) => axis_config.brake.linearity = val,
        Update::Axis(AxisUpdate::ThumbXStart(val)) => axis_config.thumb_x.start = val,
        Update::Axis(AxisUpdate::ThumbXEnd(val)) => axis_config.thumb_x.end = val,
        Update::Axis(AxisUpdate::ThumbXLinearity(val)) => axis_config.thumb_x.linearity = val,
        Update::Axis(AxisUpdate::ThumbYStart(val)) => axis_config.thumb_y.start = val,
        Update::Axis(AxisUpdate::ThumbYEnd(val)) => axis_config.thumb_y.end = val,
        Update::Axis(AxisUpdate::ThumbYLinearity(val)) => axis_config.thumb_y.linearity = val,
        Update::JoystickScript(JoystickScriptUpdate::Enabled(val)) => {
            js_config.enabled = val;
            return true;
        }
        Update::JoystickScript(JoystickScriptUpdate::Path(val)) => {
            js_config.path = val;
            return true;
        }
        _ => (),
    }
    false
}

macro_rules! adjust_pedal {
    ($config:expr, $data:expr) => {{
        // pedals are 32767 at rest and can go to -3276(7/8)
        let start_value = -65535.0 * $config.start as f64 / 100.0 + 32767.0;
        let end_value = -65535.0 * $config.end as f64 / 100.0 + 32767.0;
        if $data as f64 >= start_value {
            $data = 32767;
        } else if ($data as f64) < end_value {
            $data = -32768;
        } else {
            let start = start_value.max(end_value);
            let end = end_value.min(start_value);
            let exp = if $config.linearity == 50 {
                1.0
            } else if $config.linearity < 50 {
                1.0 / (((100.0 - $config.linearity as f64) / 50.0 - 1.0) * 2.0 + 1.0).abs()
            } else {
                (($config.linearity as f64 / 50.0 - 1.0) * 2.0 + 1.0).abs()
            };
            let distance = (end - start).abs();
            let ratio = 1.0 - (end - $data as f64).abs() / distance;
            $data = (-65535.0 * ratio.powf(exp) + 32767.0) as i16;
        }
    }};
}

macro_rules! adjust_centered {
    ($config:expr, $data:expr) => {
        let start_value = 32767.0 * $config.start as f64 / 100.0;
        let end_value = 32767.0 * $config.end as f64 / 100.0;
        if $data.abs() as f64 <= start_value {
            $data = 0;
        } else if $data.abs() as f64 > end_value {
            $data = $data.signum() * 32767;
        } else {
            let start = start_value.min(end_value);
            let end = end_value.max(start_value);
            let exp = if $config.linearity == 50 {
                1.0
            } else if $config.linearity < 50 {
                1.0 / (((100.0 - $config.linearity as f64) / 50.0 - 1.0) * 2.0 + 1.0).abs()
            } else {
                (($config.linearity as f64 / 50.0 - 1.0) * 2.0 + 1.0).abs()
            };
            let distance = end - start;
            let ratio = 1.0 - (end - $data.abs() as f64) as f64 / distance;
            $data = $data.signum() * (32767.0 * ratio.powf(exp)) as i16;
        }
    };
}

fn adjust_all_axis(data: &mut InputState, config: &AxisConfig) {
    adjust_centered!(config.wheel, data.wheel);
    adjust_pedal!(config.gas, data.gas);
    adjust_pedal!(config.brake, data.brake);
    adjust_centered!(config.thumb_x, data.thumb.0);
    adjust_centered!(config.thumb_y, data.thumb.1);
}
