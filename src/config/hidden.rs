use serde::{Deserialize, Serialize};

use crate::update_check::Version;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct HiddenConfig {
    /// Unix timestamp in seconds
    pub last_update_check_ts: u64,
    pub ignored_version: Option<Version>,
}

impl Default for HiddenConfig {
    fn default() -> Self {
        Self {
            last_update_check_ts: 0,
            ignored_version: None
        }
    }
}
