use std::path::PathBuf;

use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct JoystickScriptConfig {
    pub enabled: bool,
    pub path: Option<PathBuf>,
}
