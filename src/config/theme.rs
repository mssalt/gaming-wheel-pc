use std::{fmt::Display, str::FromStr};

use fltk_theme::ThemeType;
use serde::{Serialize, Deserialize};

use crate::i18n;

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub enum Theme {
    Classic = 0,
    Aero = 1,
    Metro = 2,
    AquaClassic = 3,
    Greybird = 4,
    Blue = 5,
    Dark = 6,
    HighContrast = 7,
}

impl Theme {
    pub fn all() -> Vec<&'static str> {
        vec![
            i18n::classic(),
            i18n::aero(),
            i18n::metro(),
            i18n::aqua_classic(),
            i18n::grey_bird(),
            i18n::blue(),
            i18n::dark(),
            i18n::high_contrast(),
        ]
    }

    pub fn positive_bar_color(&self) -> (u8, u8, u8) {
        match self {
            Theme::Classic | Theme::Greybird | Theme::Blue => (60, 242, 60),
            Theme::Aero | Theme::Metro | Theme::AquaClassic => (64, 255, 64),
            Theme::Dark => (58, 200, 58),
            Theme::HighContrast => (0, 255, 0),
        }
    }

    pub fn negative_bar_color(&self) -> (u8, u8, u8) {
        match self {
            Theme::Classic | Theme::Greybird | Theme::Blue => (242, 60, 60),
            Theme::Aero | Theme::Metro | Theme::AquaClassic => (255, 64, 64),
            Theme::Dark => (200, 58, 58),
            Theme::HighContrast => (255, 0, 0),
        }
    }

    pub fn bar_border_color(&self) -> (u8, u8, u8) {
        match self {
            Theme::Classic | Theme::Greybird | Theme::Blue => (72, 72, 72),
            Theme::Aero | Theme::Metro | Theme::AquaClassic => (64, 64, 64),
            Theme::Dark => (180, 180, 180),
            Theme::HighContrast => (255, 255, 255),
        }
    }
}

impl Display for Theme {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            Theme::Classic => i18n::classic(),
            Theme::Aero => i18n::aero(),
            Theme::Metro => i18n::metro(),
            Theme::AquaClassic => i18n::aqua_classic(),
            Theme::Greybird => i18n::grey_bird(),
            Theme::Blue => i18n::blue(),
            Theme::Dark => i18n::dark(),
            Theme::HighContrast => i18n::high_contrast(),
        })
    }
}

impl FromStr for Theme {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.eq(i18n::classic()) {
            Ok(Self::Classic)
        } else if s.eq(i18n::aero()) {
            Ok(Self::Aero)
        } else if s.eq(i18n::metro()) {
            Ok(Self::Metro)
        } else if s.eq(i18n::aqua_classic()) {
            Ok(Self::AquaClassic)
        } else if s.eq(i18n::grey_bird()) {
            Ok(Self::Greybird)
        } else if s.eq(i18n::blue()) {
            Ok(Self::Blue)
        } else if s.eq(i18n::dark()) {
            Ok(Self::Dark)
        } else if s.eq(i18n::high_contrast()) {
            Ok(Self::HighContrast)
        } else {
            return Err(())
        }
    }
}

impl From<&Theme> for ThemeType {
    fn from(value: &Theme) -> Self {
        match value {
            Theme::Classic => ThemeType::Classic,
            Theme::Aero => ThemeType::Aero,
            Theme::Metro => ThemeType::Metro,
            Theme::AquaClassic => ThemeType::AquaClassic,
            Theme::Greybird => ThemeType::Greybird,
            Theme::Blue => ThemeType::Blue,
            Theme::Dark => ThemeType::Dark,
            Theme::HighContrast => ThemeType::HighContrast,
        }
    }
}

impl Default for Theme {
    fn default() -> Self {
        Theme::Aero
    }
}
