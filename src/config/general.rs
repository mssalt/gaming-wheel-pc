use serde::{Deserialize, Serialize};

use super::theme::Theme;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GeneralConfig {
    pub check_update_start: bool,
    pub theme: Theme
}

impl Default for GeneralConfig {
    fn default() -> Self {
        Self {
            check_update_start: true,
            theme: Theme::default()
        }
    }
}
