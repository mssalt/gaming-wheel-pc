use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct NetworkConfig {
    pub address: String,
    pub port: u16,
    pub preferred_interface: String,
}

impl Default for NetworkConfig {
    fn default() -> Self {
        Self {
            address: String::from("234.0.0.55"),
            port: 41384,
            preferred_interface: Default::default(),
        }
    }
}
