use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Parameters {
    pub start: u8,
    pub end: u16,
    pub linearity: u8,
}

impl Default for Parameters {
    fn default() -> Self {
        Self {
            start: 0,
            end: 100,
            linearity: 50,
        }
    }
}

#[derive(Debug, Default, Clone, Copy, Serialize, Deserialize)]
pub struct AxisConfig {
    pub wheel: Parameters,
    pub gas: Parameters,
    pub brake: Parameters,
    pub thumb_x: Parameters,
    pub thumb_y: Parameters,
}
