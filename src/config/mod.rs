pub mod axis;
pub mod js_script;
pub mod network;
mod general;
mod hidden;
pub mod theme;

pub use axis::AxisConfig;
pub use network::NetworkConfig;
use std::path::PathBuf;

use crate::update_check::Version;

use self::{js_script::JoystickScriptConfig, general::GeneralConfig,
    hidden::HiddenConfig, theme::Theme};
use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct AppConfig {
    pub general: GeneralConfig,
    pub network: NetworkConfig,
    pub axis: AxisConfig,
    pub js_script: JoystickScriptConfig,
    pub hidden: HiddenConfig
}
#[derive(Debug, Clone)]
pub enum Update {
    Full(AppConfig),
    General(GeneralUpdate),
    Network(NetworkUpdate),
    Axis(AxisUpdate),
    JoystickScript(JoystickScriptUpdate),
    Hidden(HiddenUpdate)
}

#[derive(Debug, Clone, Copy)]
pub enum GeneralUpdate {
    CheckUpdateStart(bool),
    Theme(Theme),
}

#[derive(Debug, Clone)]
pub enum JoystickScriptUpdate {
    Enabled(bool),
    Path(Option<PathBuf>),
}

#[derive(Debug, Clone, Copy)]
pub enum AxisUpdate {
    WheelStart(u8),
    WheelEnd(u16),
    WheelLinearity(u8),
    GasStart(u8),
    GasEnd(u16),
    GasLinearity(u8),
    BrakeStart(u8),
    BrakeEnd(u16),
    BrakeLinearity(u8),
    ThumbXStart(u8),
    ThumbXEnd(u16),
    ThumbXLinearity(u8),
    ThumbYStart(u8),
    ThumbYEnd(u16),
    ThumbYLinearity(u8),
}

#[derive(Debug, Clone)]
pub enum NetworkUpdate {
    Address(String),
    Port(u16),
    PreferredInterface(String),
}

#[derive(Debug, Clone, Copy)]
pub enum HiddenUpdate {
    LastUpdateCheckTs(u64),
    IgnoredVersion(Option<Version>)
}

impl AppConfig {
    pub fn load() -> Self {
        confy::load(env!("CARGO_PKG_NAME"), None).unwrap_or_default()
    }

    pub fn update_with(&mut self, config_update: Update) {
        match config_update {
            Update::Full(config) => *self = config,
            Update::General(config) => match config {
                GeneralUpdate::CheckUpdateStart(val) => self.general.check_update_start = val,
                GeneralUpdate::Theme(val) => self.general.theme = val,
            }
            Update::Network(config) => match config {
                NetworkUpdate::Address(val) => self.network.address = val,
                NetworkUpdate::Port(val) => self.network.port = val,
                NetworkUpdate::PreferredInterface(val) => self.network.preferred_interface = val,
            },
            Update::Axis(config) => match config {
                AxisUpdate::WheelStart(val) => self.axis.wheel.start = val,
                AxisUpdate::WheelEnd(val) => self.axis.wheel.end = val,
                AxisUpdate::WheelLinearity(val) => self.axis.wheel.linearity = val,
                AxisUpdate::GasStart(val) => self.axis.gas.start = val,
                AxisUpdate::GasEnd(val) => self.axis.gas.end = val,
                AxisUpdate::GasLinearity(val) => self.axis.gas.linearity = val,
                AxisUpdate::BrakeStart(val) => self.axis.brake.start = val,
                AxisUpdate::BrakeEnd(val) => self.axis.brake.end = val,
                AxisUpdate::BrakeLinearity(val) => self.axis.brake.linearity = val,
                AxisUpdate::ThumbXStart(val) => self.axis.thumb_x.start = val,
                AxisUpdate::ThumbXEnd(val) => self.axis.thumb_x.end = val,
                AxisUpdate::ThumbXLinearity(val) => self.axis.thumb_x.linearity = val,
                AxisUpdate::ThumbYStart(val) => self.axis.thumb_y.start = val,
                AxisUpdate::ThumbYEnd(val) => self.axis.thumb_y.end = val,
                AxisUpdate::ThumbYLinearity(val) => self.axis.thumb_y.linearity = val,
            },
            Update::JoystickScript(config) => match config {
                JoystickScriptUpdate::Enabled(val) => self.js_script.enabled = val,
                JoystickScriptUpdate::Path(val) => self.js_script.path = val,
            },
            Update::Hidden(config) => match config {
                HiddenUpdate::LastUpdateCheckTs(val) => self.hidden.last_update_check_ts = val,
                HiddenUpdate::IgnoredVersion(val) => self.hidden.ignored_version = val,
            }
        }
    }

    pub fn save(&self) {
        confy::store(env!("CARGO_PKG_NAME"), None, self).unwrap();
    }
}
