use std::{
    net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr},
    str::FromStr,
    time::Instant,
};

use capnp::message::ReaderOptions;
use tokio::net::UdpSocket;

use crate::{
    config::NetworkConfig,
    error::{CreateClient, FindBestInterface, RunFrame},
    input_state::InputState,
    net_interfaces::{self, Interface},
    proto_capnp,
};

pub struct Client {
    socket: UdpSocket,
    address: SocketAddr,
    interface: Interface,
    is_multicast: bool,
    buffer: [u8; 1024],
    packet_number: Option<u8>,
    // Origin address and timestamp of the last effective packet received.
    last_effective_packet_info: Option<(SocketAddr, Instant)>,
}

impl Client {
    pub async fn new(config: &NetworkConfig) -> Result<Self, CreateClient> {
        let parsed_address = match (
            Ipv4Addr::from_str(&config.address),
            Ipv6Addr::from_str(&config.address),
        ) {
            (Ok(v4), Ok(_)) | (Ok(v4), Err(_)) => SocketAddr::new(IpAddr::V4(v4), config.port),
            (Err(_), Ok(v6)) => SocketAddr::new(IpAddr::V6(v6), config.port),
            (Err(err), Err(_)) => return Err(err.into()),
        };
        let interfaces = net_interfaces::get_all()?;
        let best_interface = find_best_interface(&interfaces, &config.preferred_interface)?;
        let is_multicast = parsed_address.ip().is_multicast();
        let socket = if is_multicast {
            #[cfg(target_os = "linux")]
            let multicast = UdpSocket::bind(parsed_address).await?;
            #[cfg(target_os = "windows")]
            let multicast = if parsed_address.is_ipv4() {
                UdpSocket::bind(&SocketAddr::new(
                    IpAddr::V4(Ipv4Addr::UNSPECIFIED),
                    config.port,
                ))
                .await?
            } else {
                UdpSocket::bind(&SocketAddr::new(
                    IpAddr::V6(Ipv6Addr::UNSPECIFIED),
                    config.port,
                ))
                .await?
            };
            match parsed_address.ip() {
                std::net::IpAddr::V4(ip_v4) => {
                    let address = find_ip4_in_interface(best_interface).ok_or(
                        CreateClient::NoIpV4AddressFound {
                            interface_name: best_interface.name.to_owned(),
                        },
                    )?;
                    multicast.join_multicast_v4(ip_v4, address)?;
                }
                std::net::IpAddr::V6(ip_v6) => {
                    multicast.join_multicast_v6(&ip_v6, best_interface.index as u32)?;
                }
            }
            multicast
        } else {
            match parsed_address.ip() {
                std::net::IpAddr::V4(_) => {
                    UdpSocket::bind(&SocketAddr::new(
                        IpAddr::V4(Ipv4Addr::UNSPECIFIED),
                        config.port,
                    ))
                    .await?
                }
                std::net::IpAddr::V6(_) => {
                    UdpSocket::bind(&SocketAddr::new(
                        IpAddr::V6(Ipv6Addr::UNSPECIFIED),
                        config.port,
                    ))
                    .await?
                }
            }
        };
        Ok(Client {
            socket,
            address: parsed_address,
            interface: best_interface.clone(),
            is_multicast,
            buffer: [0; 1024],
            packet_number: None,
            last_effective_packet_info: None,
        })
    }

    pub async fn receive_data(&mut self) -> Result<Option<InputState>, RunFrame> {
        match self.socket.recv_from(&mut self.buffer).await {
            Ok((_, origin)) => {
                let reader = capnp::serialize_packed::read_message(
                    &self.buffer[..],
                    ReaderOptions::default(),
                )?;
                let root: proto_capnp::input_state::Reader = reader.get_root()?;
                let packet_number = root.get_packet_number();
                if self.packet_number.is_none() || self.packet_number.unwrap() != packet_number {
                    let input = InputState::try_from(root)?;
                    self.packet_number = Some(input.packet_number);
                    self.last_effective_packet_info = Some((origin, Instant::now()));
                    Ok(Some(input))
                } else {
                    Ok(None)
                }
            }
            _ => Ok(None),
        }
    }

    pub fn is_multicast(&self) -> bool {
        self.is_multicast
    }

    pub fn address(&self) -> &SocketAddr {
        &self.address
    }

    pub fn interface(&self) -> &Interface {
        &self.interface
    }

    pub fn last_effective_packet_info(&self) -> Option<&(SocketAddr, Instant)> {
        self.last_effective_packet_info.as_ref()
    }
}

pub fn get_candidate_interfaces(interfaces: &[Interface]) -> Vec<&Interface> {
    interfaces
        .iter()
        .filter(|f| !f.is_loopback && f.supports_multicast && f.is_up && !f.addresses.is_empty())
        .collect()
}

fn find_best_interface<'a>(
    interfaces: &'a [Interface],
    preferred_interface: &str,
) -> Result<&'a Interface, FindBestInterface> {
    let candidates = get_candidate_interfaces(interfaces);
    if !preferred_interface.is_empty() {
        if let Some(best_of_best) = candidates.iter().find(|i| i.name.eq(preferred_interface)) {
            return Ok(best_of_best);
        }
    }
    let best = candidates
        .iter()
        .reduce(|acc, it| if acc.index < it.index { acc } else { it })
        .ok_or(FindBestInterface::NoSuitableInterface)?;
    Ok(best)
}

fn find_ip4_in_interface(interface: &Interface) -> Option<Ipv4Addr> {
    interface.addresses.iter().find_map(|addr| match addr {
        SocketAddr::V4(v4) => Some(*v4.ip()),
        SocketAddr::V6(_) => None,
    })
}
