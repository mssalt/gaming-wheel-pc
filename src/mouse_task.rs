use tokio::sync::mpsc::{Receiver, Sender};

use crate::i18n;
use crate::message::{Message, MouseMessage};

use crate::controller::MouseController;

pub async fn run_task(app_tx: Sender<Message>, mut mouse_rx: Receiver<MouseMessage>) {
    #[cfg(target_os = "linux")]
    let mut controller = loop {
        match MouseController::new() {
            Ok(ctl) => break ctl,
            Err(err) => {
                let msg = Message::UpdateMouseStatus(crate::error_message_chain(&Box::new(&err)));
                let _ = app_tx.try_send(msg);
                fltk::app::awake();
                match tokio::time::timeout(std::time::Duration::from_secs(2), mouse_rx.recv())
                    .await {
                    Ok(Some(msg)) => match msg {
                        MouseMessage::Exit => return,
                        _ => (),
                    },
                    _ => (),
                }
                continue;
            }
        }
    };
    #[cfg(target_os = "windows")]
    let mut controller = MouseController::new();
    let msg = Message::UpdateMouseStatus(i18n::working().to_owned());
    let _ = app_tx.try_send(msg);
    fltk::app::awake();
    loop {
        if let Some(message) = mouse_rx.recv().await {
            match message {
                MouseMessage::UpdateMouse(data) => {
                    controller.update(data);
                }
                MouseMessage::Exit => return,
            }
        }
    }
}
