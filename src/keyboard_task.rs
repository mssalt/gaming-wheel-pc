use tokio::sync::mpsc::{Receiver, Sender};

use crate::i18n;
use crate::message::{KeyboardMessage, Message};

use crate::controller::KeyboardController;

pub async fn run_task(app_tx: Sender<Message>, mut kb_rx: Receiver<KeyboardMessage>) {
    #[cfg(target_os = "linux")]
    let mut controller = loop {
        match KeyboardController::new() {
            Ok(ctl) => break ctl,
            Err(err) => {
                let msg =
                    Message::UpdateKeyboardStatus(crate::error_message_chain(&Box::new(&err)));
                let _ = app_tx.try_send(msg);
                fltk::app::awake();
                match tokio::time::timeout(std::time::Duration::from_secs(2), kb_rx.recv()).await {
                    Ok(Some(msg)) => match msg {
                        KeyboardMessage::Exit => return,
                        _ => (),
                    },
                    _ => (),
                }
                continue;
            }
        }
    };
    #[cfg(target_os = "windows")]
    let mut controller = KeyboardController::new();
    let msg = Message::UpdateKeyboardStatus(i18n::working().to_owned());
    let _ = app_tx.try_send(msg);
    fltk::app::awake();
    loop {
        if let Some(message) = kb_rx.recv().await {
            match message {
                KeyboardMessage::UpdateKeyboard(data) => {
                    controller.update(data);
                }
                KeyboardMessage::Exit => return,
            }
        }
    }
}
