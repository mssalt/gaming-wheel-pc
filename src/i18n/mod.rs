mod translations;

use std::{fmt::Display, sync::OnceLock};

pub(crate) use translations::*;

use crate::proto_capnp;

static CURRENT_LANG: OnceLock<Language> = OnceLock::new();

#[derive(Debug)]
pub enum Language {
    En,
    Tr,
    Ru,
    Pt,
}

impl Display for Language {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            Language::En => "en",
            Language::Tr => "tr",
            Language::Ru => "ru",
            Language::Pt => "pt",
        })
    }
}

/// This function needs to be called once at the app start
pub fn load_i18n() {
    let lang = match sys_locale::get_locale() {
        Some(lang) => {
            if lang.starts_with("en") {
                Language::En
            } else if lang.starts_with("tr") {
                Language::Tr
            } else if lang.starts_with("ru") {
                Language::Ru
            } else if lang.starts_with("pt") {
                Language::Pt
            } else {
                Language::En
            }
        }
        None => Language::En,
    };
    CURRENT_LANG.set(lang).unwrap();
}

pub fn get_current_lang() -> &'static Language {
    CURRENT_LANG.get().unwrap()
}

impl Display for proto_capnp::Gear {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            proto_capnp::Gear::R => "R",
            proto_capnp::Gear::N => "N",
            proto_capnp::Gear::D1 => "1",
            proto_capnp::Gear::D2 => "2",
            proto_capnp::Gear::D3 => "3",
            proto_capnp::Gear::D4 => "4",
            proto_capnp::Gear::D5 => "5",
            proto_capnp::Gear::D6 => "6",
            proto_capnp::Gear::D7 => "7",
        })
    }
}
