use super::Language;
use crate::i18n::get_current_lang;
use std::fmt::Display;

macro_rules! translation {
    {
        name: $name:ident,
        en: $en:literal,
        tr: $tr:literal,
        ru: $ru:literal,
        pt: $pt:literal$(,)?
    } => {
        pub fn $name() -> &'static str {
            match get_current_lang() {
                Language::En => $en,
                Language::Tr => $tr,
                Language::Ru => $ru,
                Language::Pt => $pt,
            }
        }
    };
    {
        name: $name:ident,
        params: ($($param:ident),*),
        en: $en:expr,
        tr: $tr:expr,
        ru: $ru:expr,
        pt: $pt:expr$(,)?
    } => {
        pub fn $name($($param: impl Display),*) -> String {
            match get_current_lang() {
                Language::En => format!($en),
                Language::Tr => format!($tr),
                Language::Ru => format!($ru),
                Language::Pt => format!($pt),
            }
        }
    };
}

translation! {
    name: app_name,
    en: "Gaming Wheel PC",
    tr: "Oyun Direksiyonu PC",
    ru: "Игровое Руль ПК",
    pt: "Roda de Jogos PC",
}

translation! {
    name: receiver_status,
    en: "Receiver status",
    tr: "Alıcı durumu",
    ru: "Статус ресивера",
    pt: "Estado do receptor",
}

translation! {
    name: joystick_status,
    en: "Joystick status",
    tr: "Oyun kolu durumu",
    ru: "Статус джойстика",
    pt: "Estado do joystick",
}

translation! {
    name: mouse_status,
    en: "Mouse status",
    tr: "Fare durumu",
    ru: "Статус мыши",
    pt: "Estado do mouse",
}

translation! {
    name: keyboard_status,
    en: "Keyboard status",
    tr: "Klavye durumu",
    ru: "Статус клавиатуры",
    pt: "Estado do teclado",
}

translation! {
    name: working,
    en: "Working",
    tr: "Çalışıyor",
    ru: "Работает",
    pt: "Trabalhando",
}

translation! {
    name: updating,
    en: "Updating",
    tr: "Güncelleniyor",
    ru: "Обновление",
    pt: "Atualizando",
}

translation! {
    name: port,
    en: "Port",
    tr: "Bağlantı noktası",
    ru: "Порт",
    pt: "Porto",
}

translation! {
    name: network_interfaces,
    en: "Network Interfaces",
    tr: "Ağ Arayüzleri",
    ru: "Сетевые интерфейсы",
    pt: "Interfaces de rede",
}

translation! {
    name: bind_address,
    en: "Bind address",
    tr: "Bağlama adresi",
    ru: "Привязать адрес",
    pt: "Endereço vinculado",
}

translation! {
    name: list_of_available_net_interfaces,
    en: "List of available network interfaces",
    tr: "Mevcut ağ arayüzlerinin listesi",
    ru: "Список доступных сетевых интерфейсов",
    pt: "Lista de interfaces de rede disponíveis",
}

translation! {
    name: no_supported_net_interface_found,
    en: "No supported network interface found",
    tr: "Desteklenen ağ arayüzü bulunamadı",
    ru: "Поддерживаемый сетевой интерфейс не найден",
    pt: "Nenhuma interface de rede suportada encontrada",
}

translation! {
    name: preferred_interface,
    en: "Preferred interface",
    tr: "Tercih edilen arayüz",
    ru: "Предпочитаемый интерфейс",
    pt: "Interface preferida",
}

translation! {
    name: reset_settings,
    en: "Reset the settings",
    tr: "Ayarları sıfırla",
    ru: "Сбросить настройки",
    pt: "Redefinir as configurações",
}

translation! {
    name: want_to_reset_settings,
    en: "Do you want to reset all settings?",
    tr: "Tüm ayarları sıfırlamak istiyor musunuz?",
    ru: "Вы хотите сбросить все настройки?",
    pt: "Deseja redefinir todas as configurações?",
}

translation! {
    name: failed_to_get_net_interfaces,
    en: "Failed to get network interfaces",
    tr: "Ağ arayüzleri alınamadı",
    ru: "Не удалось получить сетевые интерфейсы",
    pt: "Falha ao obter interfaces de rede",
}

translation! {
    name: listening_multicast_at_address,
    params: (address, interface_name),
    en: "Listening at multicast address '{address}' on interface '{interface_name}' \
        but not receiving data (check your mobile device)",
    tr: "'{interface_name}' arayüzünde '{address}' çok noktalı yayın adresinde \
        dinleme yapılıyor fakat veri alınmıyor (mobil cihazınızı kontrol edin)",
    ru: "Прослушивание многоадресного адреса '{address}' на интерфейсе '{interface_name}', \
        но не получение данных (проверьте свое мобильное устройство)",
    pt: "Ouvindo no endereço multicast '{address}' na interface '{interface_name}', \
        mas não recebendo dados (verifique seu dispositivo móvel)",
}

translation! {
    name: listening_at_unicast_address,
    params: (address),
    en: "Listening at '{address}' unicast address but not receiving data \
        (check your mobile device)",
    tr: "'{address}' tek noktalı yayın adresinde dinleme yapılıyor fakat veri alınmıyor \
        (mobil cihazınızı kontrol edin)",
    ru: "Прослушивание индивидуального адреса '{address}', но не получение данных \
        (проверьте свое мобильное устройство)",
    pt: "Ouvindo no endereço unicast '{address}', mas não recebendo dados \
        (verifique seu dispositivo móvel)",
}

translation! {
    name: receiving_data_from_address,
    params: (address),
    en: "Receiving data from address '{address}'",
    tr: "'{address}' adresinden veri alınıyor",
    ru: "Получение данных с адреса '{address}'",
    pt: "Recebendo dados do endereço '{address}'",
}

translation! {
    name: information,
    en: "Info",
    tr: "Bilgi",
    ru: "Информация",
    pt: "Informações",
}

translation! {
    name: ok,
    en: "Ok",
    tr: "Tamam",
    ru: "Хорошо",
    pt: "Ok",
}

translation! {
    name: cancel,
    en: "Cancel",
    tr: "İptal et",
    ru: "Отмена",
    pt: "Cancelar",
}

translation! {
    name: application,
    en: "Application",
    tr: "Uygulama",
    ru: "Приложение",
    pt: "Aplicativo",
}

translation! {
    name: exit,
    en: "Exit",
    tr: "Çıkış",
    ru: "Выход",
    pt: "Sair",
}

translation! {
    name: about,
    en: "About",
    tr: "Hakkında",
    ru: "О приложении",
    pt: "Sobre",
}

translation! {
    name: help,
    en: "Help",
    tr: "Yardım",
    ru: "Помощь",
    pt: "Ajuda",
}

translation! {
    name: settings,
    en: "Settings",
    tr: "Ayarlar",
    ru: "Настройки",
    pt: "Configurações",
}

translation! {
    name: wheel,
    en: "Wheel",
    tr: "Direksiyon",
    ru: "Руль",
    pt: "Roda",
}

translation! {
    name: gas,
    en: "Gas",
    tr: "Gaz",
    ru: "Газ",
    pt: "Gás",
}

translation! {
    name: brake,
    en: "Brake",
    tr: "Fren",
    ru: "Тормоз",
    pt: "Freio",
}

translation! {
    name: thumb_x,
    en: "Thumbstick X",
    tr: "Kontrol Çubuğu X",
    ru: "Аналоговый Стик X",
    pt: "Polegar X",
}

translation! {
    name: thumb_y,
    en: "Thumbstick Y",
    tr: "Kontrol Çubuğu Y",
    ru: "Аналоговый Стик Y",
    pt: "Polegar Y",
}

translation! {
    name: gear_label,
    params: (gear),
    en: "Gear: {gear}",
    tr: "Vites: {gear}",
    ru: "Передача: {gear}",
    pt: "Engrenagem: {gear}",
}

translation! {
    name: screen_buttons,
    en: "Screen Buttons",
    tr: "Ekran Düğmeleri",
    ru: "Экранные кнопки",
    pt: "Botões da tela",
}

translation! {
    name: volume_buttons,
    en: "Volume Keys",
    tr: "Ses Tuşları",
    ru: "Кнопки громкости",
    pt: "Teclas de volume",
}

translation! {
    name: refresh,
    en: "Refresh",
    tr: "Tazele",
    ru: "Обновить",
    pt: "Atualizar",
}

translation! {
    name: close,
    en: "Close",
    tr: "Kapat",
    ru: "Закрывать",
    pt: "Fechar",
}


translation! {
    name: qr_code_title,
    params: (ip),
    en: "QR Code ({ip})",
    tr: "QR Kodu ({ip})",
    ru: "QR код ({ip})",
    pt: "Código QR ({ip})",
}

translation! {
    name: ip_button_tooltip,
    en: "Click to show the QR code and copy the address",
    tr: "QR kodunu göstermek ve adresi kopyalamak için tıklayın",
    ru: "нажмите, чтобы показать QR-код и скопировать адрес",
    pt: "Clique para mostrar o código QR e copiar o endereço",
}

translation! {
    name: interface_name_button_tooltip,
    en: "Click to copy the interface name",
    tr: "Arayüz adını kopyalamak için tıklayın",
    ru: "Нажмите, чтобы скопировать имя интерфейса",
    pt: "Clique para copiar o nome da interface",
}

translation! {
    name: general,
    en: "General",
    tr: "Genel",
    ru: "Общий",
    pt: "Geral",
}

translation! {
    name: network,
    en: "Network",
    tr: "Ağ",
    ru: "Сеть",
    pt: "Rede",
}

translation! {
    name: axis,
    en: "Axis",
    tr: "Eksen",
    ru: "Ось",
    pt: "Eixo",
}

translation! {
    name: start,
    en: "Start",
    tr: "Başlangıç",
    ru: "Начало",
    pt: "Iniciar",
}

translation! {
    name: end,
    en: "End",
    tr: "Bitiş",
    ru: "конец",
    pt: "Fim",
}

translation! {
    name: linearity,
    en: "Linearity",
    tr: "Doğrusallık",
    ru: "Линейность",
    pt: "Linearidade",
}

translation! {
    name: unexpected_error,
    en: "An unexpected error has occurred",
    tr: "Beklenmeyen bir hata meydana geldi",
    ru: "Произошла непредвиденная ошибка",
    pt: "Ocorreu um erro inesperado",
}

translation! {
    name: unexpected_error_in_thread,
    params: (thread_name),
    en: "An unexpected error has occurred in thread '{thread_name}'",
    tr: "'{thread_name}' isimli iş parçacığında beklenmeyen bir hata meydana geldi",
    ru: "Произошла непредвиденная ошибка в потоке '{thread_name}'",
    pt: "Ocorreu um erro inesperado no tópico '{thread_name}'",
}

#[cfg(target_os = "windows")]
translation! {
    name: vjoy_already_owned_status,
    en: "vJoy device is already owned by this app",
    tr: "vJoy aygıtı zaten bu uygulama tarafından sahiplenilmiş",
    ru: "Устройство vJoy уже принадлежит этому приложению",
    pt: "o dispositivo vJoy já pertence a este aplicativo",
}

#[cfg(target_os = "windows")]
translation! {
    name: vjoy_busy_status,
    en: "vJoy device is already owned by another app",
    tr: "vJoy aygıtı zaten başka bir uygulama tarafından sahiplenilmiş",
    ru: "Устройство vJoy уже принадлежит другому приложению",
    pt: "dispositivo vJoy já pertence a outro aplicativo",
}

#[cfg(target_os = "windows")]
translation! {
    name: vjoy_missing_status,
    en: "vJoy device is either not installed or disabled",
    tr: "vJoy aygıtı ya kurulu değil ya da devre dışı",
    ru: "Устройство vJoy либо не установлено, либо отключено",
    pt: "dispositivo vJoy não está instalado ou desativado",
}

#[cfg(target_os = "windows")]
translation! {
    name: vjoy_other_status,
    en: "vJoy device general error",
    tr: "Genel vJoy aygıt hatası",
    ru: "Общая ошибка устройства vJoy",
    pt: "erro geral do dispositivo vJoy",
}

translation! {
    name: authors,
    en: "Authors",
    tr: "Yazanlar",
    ru: "Авторы",
    pt: "Autores",
}

translation! {
    name: app_version,
    en: "App version",
    tr: "Uygulama sürümü",
    ru: "Версия приложения",
    pt: "Versão do aplicativo",
}

translation! {
    name: network_protocol_version,
    en: "Network protocol version",
    tr: "Ağ protokolü sürümü",
    ru: "версия сетевого протокола",
    pt: "Versão do protocolo de rede",
}

translation! {
    name: joystick_script,
    en: "Joystick Script",
    tr: "Oyun Kolu Betiği",
    ru: "Скрипт джойстика",
    pt: "Script de joystick",
}

translation! {
    name: script_path,
    en: "Script path",
    tr: "Betik yolu",
    ru: "Путь к скрипту",
    pt: "Caminho do script",
}

translation! {
    name: js_script_desc,
    en: "Custom lua script for full control of the joystick driver (see help for details).",
    tr: "Oyun kolu sürücüsünün tam kontrolü için özel lua betiği (detaylar için yardıma bakın).",
    ru: "Пользовательский lua-скрипт для полного управления драйвером джойстика \
        (подробности см. в справке).",
    pt: "Script lua personalizado para controle total do driver do joystick \
        (consulte a ajuda para obter detalhes).",
}

translation! {
    name: select_lua_script,
    en: "Select lua script",
    tr: "Lua betiği seçin",
    ru: "Выберите луа-скрипт",
    pt: "Selecionar script lua",
}

translation! {
    name: enabled,
    en: "Enabled",
    tr: "Etkin",
    ru: "Эффективный",
    pt: "Habilitado",
}

translation! {
    name: check_update_at_startup,
    en: "Check for update at startup",
    tr: "Açılışta güncellemeyi kontrol et",
    ru: "Проверять обновлений при запуске",
    pt: "Verificar atualização na inicialização",
}

translation! {
    name: check_for_update,
    en: "Check for update",
    tr: "Güncellemeyi kontrol et",
    ru: "Проверять обновлений",
    pt: "Verificar atualização",
}

translation! {
    name: title_update_check,
    en: "Update Check",
    tr: "Güncelleme Kontrolü",
    ru: "Проверка обновления",
    pt: "Verificação de atualização",
}

translation! {
    name: checking_update_dots,
    en: "Checking update...",
    tr: "Güncelleme kontrol ediliyor...",
    ru: "Проверка обновления...",
    pt: "Verificando atualização...",
}

translation! {
    name: new_version_available,
    params: (full_version),
    en: "New version is available ({full_version})",
    tr: "Yeni sürüm kullanılabilir ({full_version})",
    ru: "Доступна новая версия ({full_version})",
    pt: "Nova versão está disponível ({full_version})",
}

translation! {
    name: changes,
    en: "Changes",
    tr: "Değişiklikler",
    ru: "Изменения",
    pt: "Mudanças",
}

translation! {
    name: app_up_to_date,
    en: "The app is up-to-date",
    tr: "Uygulama güncel",
    ru: "Приложение обновлено",
    pt: "O aplicativo está atualizado",
}

translation! {
    name: invalid_version_info_try_again,
    en: "Invalid version info received. Please try again later.",
    tr: "Geçersiz sürüm bilgisi alındı. Lütfen daha sonra tekrar deneyin.",
    ru: "Получена неверная информация о версии. Пожалуйста, повторите попытку позже.",
    pt: "Informação de versão inválida recebida. Tente novamente mais tarde.",
}

translation! {
    name: download,
    en: "Download",
    tr: "İndir",
    ru: "Скачать",
    pt: "Baixar",
}

translation! {
    name: string_download_page_url,
    en: "https://gwheel.pages.dev/",
    tr: "https://gwheel.pages.dev/",
    ru: "https://gwheel.pages.dev/",
    pt: "https://gwheel.pages.dev/",
}

translation! {
    name: ignore,
    en: "Ignore",
    tr: "Önemseme",
    ru: "Игнорировать",
    pt: "Ignorar",
}

translation! {
    name: latest_version_changes_not_found,
    en: "Error: Latest version changes not found",
    tr: "Hata: Yeni sürüm değişiklikleri bulunamadı",
    ru: "Ошибка: изменения последней версии не найдены",
    pt: "Erro: alterações da versão mais recente não encontradas",
}

translation! {
    name: theme,
    en: "Theme",
    tr: "Tema",
    ru: "Тема",
    pt: "Tema",
}

translation! {
    name: classic,
    en: "Classic",
    tr: "Klasik",
    ru: "Классик",
    pt: "Clássica",
}

translation! {
    name: aero,
    en: "Aero",
    tr: "Aero",
    ru: "Аэро",
    pt: "Aero",
}

translation! {
    name: metro,
    en: "Metro",
    tr: "Metro",
    ru: "Метро",
    pt: "Metrô",
}

translation! {
    name: aqua_classic,
    en: "Aqua Classic",
    tr: "Su Klasik",
    ru: "Аква Классик",
    pt: "Aqua Clássico",
}

translation! {
    name: grey_bird,
    en: "Grey Bird",
    tr: "Gri Kuş",
    ru: "Серая Птица",
    pt: "Pássaro Cinzento",
}

translation! {
    name: blue,
    en: "Blue",
    tr: "Mavi",
    ru: "Синий",
    pt: "Azul",
}

translation! {
    name: dark,
    en: "Dark",
    tr: "Karanlık",
    ru: "Темный",
    pt: "Escura",
}

translation! {
    name: high_contrast,
    en: "High Contrast",
    tr: "Yüksek Karşıtlık",
    ru: "Высокий контраст",
    pt: "Alto Contraste",
}
