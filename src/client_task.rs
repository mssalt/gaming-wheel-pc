use std::time::Duration;

use tokio::{
    select,
    sync::mpsc::{Receiver, Sender},
    time::{sleep, MissedTickBehavior},
};

const NETWORK_CHECK_PERIOD: u64 = 1500;
const RECEIVING_CHECK_PERIOD: u64 = 1000;

use crate::{
    client::{get_candidate_interfaces, Client},
    config::{NetworkConfig, NetworkUpdate, Update},
    error_message_chain, i18n,
    input_state::InputState,
    message::{ClientMessage, JoystickMessage, KeyboardMessage, Message, MouseMessage},
    net_interfaces,
};

enum State {
    Initializing(InitializingState),
    Receiving(ReceivingState),
    Exit,
}

struct InitializingState {
    app_tx: Sender<Message>,
    js_tx: Sender<JoystickMessage>,
    kb_tx: Sender<KeyboardMessage>,
    ms_tx: Sender<MouseMessage>,
    client_rx: Receiver<ClientMessage>,
    net_config: NetworkConfig,
    next_network_check: tokio::time::Interval,
    next_receiving_check: tokio::time::Interval,
}

struct ReceivingState {
    init: InitializingState,
    client: Box<Client>,
}

pub async fn run_task(
    net_config: NetworkConfig,
    app_tx: Sender<Message>,
    js_tx: Sender<JoystickMessage>,
    kb_tx: Sender<KeyboardMessage>,
    ms_tx: Sender<MouseMessage>,
    client_rx: Receiver<ClientMessage>,
) {
    let mut next_network_check = tokio::time::interval(Duration::from_millis(NETWORK_CHECK_PERIOD));
    next_network_check.set_missed_tick_behavior(MissedTickBehavior::Skip);
    let mut next_receiving_check =
        tokio::time::interval(Duration::from_millis(RECEIVING_CHECK_PERIOD));
    next_receiving_check.set_missed_tick_behavior(MissedTickBehavior::Skip);
    let mut state = State::Initializing(InitializingState {
        app_tx,
        js_tx,
        ms_tx,
        kb_tx,
        client_rx,
        net_config,
        next_network_check,
        next_receiving_check,
    });
    loop {
        state = match state {
            State::Initializing(state) => initialize(state).await,
            State::Receiving(state) => receive(state).await,
            State::Exit => break,
        }
    }
}

async fn initialize(mut state: InitializingState) -> State {
    match Client::new(&state.net_config).await {
        Ok(client) => {
            let message = if client.is_multicast() {
                i18n::listening_multicast_at_address(
                    &client.address().to_string(),
                    &client.interface().name,
                )
            } else {
                i18n::listening_at_unicast_address(&client.address().to_string())
            };
            send_info_message(&state.app_tx, message);
            return State::Receiving(ReceivingState {
                init: state,
                client: Box::new(client),
            });
        }
        Err(err) => {
            if let Ok(message) = state.client_rx.try_recv() {
                match message {
                    ClientMessage::UpdateConfig(update) => {
                        handle_config_update(update, &mut state.net_config);
                    }
                    ClientMessage::Exit => return State::Exit,
                }
            }
            send_info_message(&state.app_tx, error_message_chain(&Box::new(err)));
            match tokio::time::timeout(Duration::from_secs(2), state.client_rx.recv()).await {
                Ok(Some(msg)) => match msg {
                    ClientMessage::UpdateConfig(update) => {
                        handle_config_update(update, &mut state.net_config);
                    }
                    ClientMessage::Exit => return State::Exit,
                },
                _ => (),
            }
            return State::Initializing(state);
        }
    }
}

async fn receive(mut state: ReceivingState) -> State {
    let client_rx_wait = state.init.client_rx.recv();
    let network_check_tick = state.init.next_network_check.tick();
    let receive_check_tick = state.init.next_receiving_check.tick();
    let data_wait = state.client.receive_data();
    select! {
        Some(message) = client_rx_wait => {
            match message {
                ClientMessage::UpdateConfig(update) => {
                    handle_config_update(update, &mut state.init.net_config);
                    send_info_message(&state.init.app_tx, i18n::updating().to_owned());
                    State::Initializing(state.init)
                }
                ClientMessage::Exit => State::Exit,
            }
        }
        _instant = network_check_tick => {
            if let Ok(interfaces) = net_interfaces::get_all() {
                let interface_valid =
                    get_candidate_interfaces(&interfaces).contains(&state.client.interface());
                if !interface_valid {
                    send_info_message(&state.init.app_tx, i18n::updating().to_owned());
                    return State::Initializing(state.init);
                }
            }
            State::Receiving(state)
        }
        _instant = receive_check_tick => {
            let origin = state.client.last_effective_packet_info().map_or(None, |info| {
                if info.1.elapsed().as_millis() as u64 > RECEIVING_CHECK_PERIOD {
                    None
                } else {
                    Some(&info.0)
                }
            });
            let message = if let Some(origin) = origin {
                if state.client.is_multicast() {
                    i18n::receiving_data_from_address(
                        &origin.to_string(),
                    )
                } else {
                    i18n::receiving_data_from_address(&origin.to_string())
                }
            } else {
                if state.client.is_multicast() {
                    i18n::listening_multicast_at_address(
                        &state.client.address().to_string(),
                        &state.client.interface().name,
                    )
                } else {
                    i18n::listening_at_unicast_address(&state.client.address().to_string())
                }
            };
            send_info_message(&state.init.app_tx, message);
            State::Receiving(state)
        }
        data = data_wait => {
            match data {
                Ok(Some(data)) => {
                    send_data_message(&state.init, data);
                    State::Receiving(state)
                }
                Ok(None) => State::Receiving(state),
                Err(err) => {
                    sleep(Duration::from_secs(2)).await;
                    send_info_message(&state.init.app_tx, error_message_chain(&Box::new(err)));
                    State::Receiving(state)
                }
            }
        }
    }
}

fn send_info_message(app_tx: &Sender<Message>, msg: String) {
    let _ = app_tx.try_send(Message::UpdateClientStatus(msg));
    fltk::app::awake();
}

fn send_data_message(init_state: &InitializingState, data: InputState) {
    let _ = init_state
        .app_tx
        .try_send(Message::UpdateData(data.clone()));
    let _ = init_state
        .js_tx
        .try_send(JoystickMessage::UpdateJoystick(data.clone()));
    let _ = init_state
        .kb_tx
        .try_send(KeyboardMessage::UpdateKeyboard(data.clone()));
    let _ = init_state.ms_tx.try_send(MouseMessage::UpdateMouse(data));
    fltk::app::awake();
}

fn handle_config_update(update: Update, initializer: &mut NetworkConfig) {
    match update {
        Update::Full(config) => {
            initializer.address = config.network.address;
            initializer.port = config.network.port;
            initializer.preferred_interface = config.network.preferred_interface;
        }
        Update::Network(NetworkUpdate::Address(addr)) => initializer.address = addr,
        Update::Network(NetworkUpdate::Port(port)) => initializer.port = port,
        Update::Network(NetworkUpdate::PreferredInterface(interface)) => {
            initializer.preferred_interface = interface
        }
        _ => (),
    }
}
