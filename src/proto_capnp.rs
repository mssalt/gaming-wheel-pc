#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(unused)]

#[cfg(target_os = "windows")]
include!(concat!(env!("OUT_DIR"), "\\proto_capnp.rs"));

#[cfg(target_os = "linux")]
include!(concat!(env!("OUT_DIR"), "/proto_capnp.rs"));
