use std::{time::Duration, fmt::Display, collections::BTreeMap};

use tokio::{sync::mpsc::Sender, time::timeout};

use crate::{
    error,
    config::{AppConfig,
    Update,
    HiddenUpdate},
    message::Message,
    NETWORK_PROTOCOL_VERSION
};

const CUR_VER_INFO_URL: &str = "https://gwheel.pages.dev/latest-version-info.yml";
const ONE_DAY_IN_SECS: u64 = 24 * 60 * 60;

#[derive(Debug, Clone, serde::Deserialize)]
pub struct LatestVersionInfo {
    pub version: Version,
    pub changes: BTreeMap<String, Vec<String>>,
}

#[derive(Debug, Copy, Clone, serde::Deserialize, serde::Serialize)]
pub struct Version {
    pub major: u64,
    pub minor: u64,
    pub patch: u64,
    #[serde(alias = "messaging")]
    pub network_protocol: u64,
}

impl Display for Version {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "v{}.{}.{}-n{}", self.major, self.minor, self.patch, self.network_protocol)
    }
}

impl Eq for Version {}

impl PartialEq for Version {
    fn eq(&self, other: &Self) -> bool {
        self.major == other.major &&
        self.minor == other.minor &&
        self.patch == other.patch &&
        self.network_protocol == other.network_protocol
    }
}

impl Ord for Version {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.major.cmp(&other.major) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        match self.minor.cmp(&other.minor) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        match self.patch.cmp(&other.patch) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        self.network_protocol.cmp(&other.network_protocol)
    }
}

impl PartialOrd for Version {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match self.major.partial_cmp(&other.major) {
            Some(core::cmp::Ordering::Equal) => {}
            ord => return ord,
        }
        match self.minor.partial_cmp(&other.minor) {
            Some(core::cmp::Ordering::Equal) => {}
            ord => return ord,
        }
        match self.patch.partial_cmp(&other.patch) {
            Some(core::cmp::Ordering::Equal) => {}
            ord => return ord,
        }
        self.network_protocol.partial_cmp(&other.network_protocol)
    }
}

impl Version {
    pub fn get_current() -> Result<Self, error::GetCurrentVersion> {
        let version_str = env!("CARGO_PKG_VERSION");
        let mut splits = version_str.split('.');
        let major: u64 = splits.next().ok_or(
            error::GetCurrentVersion::MajorNotFound { version_str }
        )?.parse()?;
        let minor: u64 = splits.next().ok_or(
            error::GetCurrentVersion::MinorNotFound { version_str }
        )?.parse()?;
        let patch: u64 = splits.next().ok_or(
            error::GetCurrentVersion::PatchNotFound { version_str }
        )?.parse()?;
        Ok(Self {
            major,
            minor,
            patch,
            network_protocol: NETWORK_PROTOCOL_VERSION as u64
        })
    }
}

pub enum TaskReason {
    AppStartup {
        config: AppConfig
    },
    UserRequest
}

pub async fn get_latest_version() -> Result<LatestVersionInfo, error::CheckUpdate> {
    let text = reqwest::get(CUR_VER_INFO_URL).await?.text().await?;
    serde_yaml::from_str::<LatestVersionInfo>(&text)
        .map_err(|_| error::CheckUpdate::ParseVersionInfo { text })
}

pub async fn run_task(app_tx: Sender<Message>, reason: TaskReason) {
    let (mut show_update_window, timeout_sec, ignored_version) = match reason {
        TaskReason::AppStartup { config } => {
            let now = std::time::SystemTime::now()
                .duration_since(std::time::UNIX_EPOCH)
                .unwrap()
                .as_secs();
            let last_update_elapsed = now - config.hidden.last_update_check_ts;
            if !config.general.check_update_start || last_update_elapsed < ONE_DAY_IN_SECS {
                return;
            }
            (true, 5, config.hidden.ignored_version)
        },
        TaskReason::UserRequest => (false, 15, None),
    };
    match timeout(Duration::from_secs(timeout_sec), get_latest_version()).await {
        Ok(Ok(info)) => {
            if let Some(version) = &ignored_version {
                show_update_window = info.version.ne(version);
            }
            let _ = app_tx.send(Message::UpdateLatestVersionInfo{
                info_result: Ok(info),
                show_update_window
            }).await;
            let now = std::time::SystemTime::now()
                .duration_since(std::time::UNIX_EPOCH)
                .unwrap()
                .as_secs();
            let _ = app_tx.send(
                Message::UpdateConfig(Update::Hidden(HiddenUpdate::LastUpdateCheckTs(now)))
            ).await;
            fltk::app::awake();
        },
        Ok(Err(err)) => {
            let _ = app_tx.send(Message::UpdateLatestVersionInfo {
                info_result: Err(err),
                show_update_window
            }).await;
            fltk::app::awake();
        }
        _ => (),
    }
}

#[cfg(test)]
mod test {
    #[tokio::test]
    async fn get_latest_version() {
        let info = super::get_latest_version().await.unwrap();
        println!("info: {:?}", info);
    }
}
