use mlua::UserData;

use crate::{error::ReadInputData, proto_capnp, NETWORK_PROTOCOL_VERSION};

#[derive(Debug, Clone)]
pub struct InputState {
    pub version: u16,
    pub packet_number: u8,
    pub wheel: i16,
    pub gas: i16,
    pub brake: i16,
    pub thumb: (i16, i16),
    pub gear: proto_capnp::Gear,
    pub screen_buttons: [bool; 8],
    pub volume_buttons: [bool; 2],
    pub is_secondary_mode_active: bool,
    pub pressed_pc_keys: Vec<proto_capnp::Key>,
    pub delta_mouse_x: i16,
    pub delta_mouse_y: i16,
    pub delta_mouse_scroll: i16,
    pub left_mouse_button: bool,
    pub right_mouse_button: bool,
    pub middle_mouse_button: bool,
}

impl Default for InputState {
    fn default() -> Self {
        Self {
            version: Default::default(),
            packet_number: Default::default(),
            wheel: Default::default(),
            gas: i16::MAX,
            brake: i16::MAX,
            thumb: Default::default(),
            gear: proto_capnp::Gear::N,
            screen_buttons: Default::default(),
            volume_buttons: Default::default(),
            is_secondary_mode_active: Default::default(),
            pressed_pc_keys: Default::default(),
            delta_mouse_x: Default::default(),
            delta_mouse_y: Default::default(),
            delta_mouse_scroll: Default::default(),
            left_mouse_button: Default::default(),
            right_mouse_button: Default::default(),
            middle_mouse_button: Default::default(),
        }
    }
}

impl TryFrom<proto_capnp::input_state::Reader<'_>> for InputState {
    type Error = ReadInputData;

    fn try_from(value: proto_capnp::input_state::Reader<'_>) -> Result<Self, Self::Error> {
        let version = value.get_version();
        if NETWORK_PROTOCOL_VERSION != version {
            return Err(ReadInputData::NetworkProtocolVersionMismatch {
                mobile_net_version: version,
                pc_net_version: version,
            });
        }
        let mut keys = Vec::new();
        for key in value.get_pressed_pc_keys()? {
            keys.push(key?);
        }
        Ok(InputState {
            version,
            packet_number: value.get_packet_number(),
            wheel: value.get_wheel(),
            gas: value.get_gas(),
            brake: value.get_brake(),
            thumb: (value.get_thumb_x(), value.get_thumb_y()),
            gear: value.get_gear()?,
            screen_buttons: [
                value.get_screen_button1(),
                value.get_screen_button2(),
                value.get_screen_button3(),
                value.get_screen_button4(),
                value.get_screen_button5(),
                value.get_screen_button6(),
                value.get_screen_button7(),
                value.get_screen_button8(),
            ],
            volume_buttons: [value.get_volume_down(), value.get_volume_up()],
            is_secondary_mode_active: value.get_is_secondary_mode_active(),
            pressed_pc_keys: keys,
            delta_mouse_x: value.get_delta_mouse_x(),
            delta_mouse_y: value.get_delta_mouse_y(),
            delta_mouse_scroll: value.get_delta_mouse_scroll(),
            left_mouse_button: value.get_left_mouse_button(),
            right_mouse_button: value.get_right_mouse_button(),
            middle_mouse_button: value.get_middle_mouse_button(),
        })
    }
}

impl UserData for InputState {
    fn add_fields<'lua, F: mlua::UserDataFields<'lua, Self>>(fields: &mut F) {
        fields.add_field_method_get("wheel", |_, input| Ok(input.wheel));
        fields.add_field_method_get("gas", |_, input| Ok(input.gas));
        fields.add_field_method_get("brake", |_, input| Ok(input.brake));
        fields.add_field_method_get("thumbX", |_, input| Ok(input.thumb.0));
        fields.add_field_method_get("thumbY", |_, input| Ok(input.thumb.1));
        fields.add_field_method_get("gearR", |_, input| Ok(input.gear == proto_capnp::Gear::R));
        fields.add_field_method_get("gear1", |_, input| Ok(input.gear == proto_capnp::Gear::D1));
        fields.add_field_method_get("gear2", |_, input| Ok(input.gear == proto_capnp::Gear::D2));
        fields.add_field_method_get("gear3", |_, input| Ok(input.gear == proto_capnp::Gear::D3));
        fields.add_field_method_get("gear4", |_, input| Ok(input.gear == proto_capnp::Gear::D4));
        fields.add_field_method_get("gear5", |_, input| Ok(input.gear == proto_capnp::Gear::D5));
        fields.add_field_method_get("gear6", |_, input| Ok(input.gear == proto_capnp::Gear::D6));
        fields.add_field_method_get("gear7", |_, input| Ok(input.gear == proto_capnp::Gear::D7));
        fields.add_field_method_get("button1", |_, input| Ok(input.screen_buttons[0]));
        fields.add_field_method_get("button2", |_, input| Ok(input.screen_buttons[1]));
        fields.add_field_method_get("button3", |_, input| Ok(input.screen_buttons[2]));
        fields.add_field_method_get("button4", |_, input| Ok(input.screen_buttons[3]));
        fields.add_field_method_get("button5", |_, input| Ok(input.screen_buttons[4]));
        fields.add_field_method_get("button6", |_, input| Ok(input.screen_buttons[5]));
        fields.add_field_method_get("button7", |_, input| Ok(input.screen_buttons[6]));
        fields.add_field_method_get("button8", |_, input| Ok(input.screen_buttons[7]));
        fields.add_field_method_get("volumeDown", |_, input| Ok(input.volume_buttons[0]));
        fields.add_field_method_get("volumeUp", |_, input| Ok(input.volume_buttons[1]));
        fields.add_field_method_get("secondaryActive", |_, input| {
            Ok(input.is_secondary_mode_active)
        });
    }
}
