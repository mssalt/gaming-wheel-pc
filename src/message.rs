use crate::{config::Update, input_state::InputState, update_check::LatestVersionInfo, error};

/// Messages which can be sent/received on any channel
#[derive(Debug)]
pub enum Message {
    UpdateClientStatus(String),
    UpdateJoystickStatus(String),
    UpdateMouseStatus(String),
    UpdateKeyboardStatus(String),
    UpdateData(InputState),
    UpdateConfig(Update),
    UpdateLatestVersionInfo {
        info_result: Result<LatestVersionInfo, error::CheckUpdate>,
        show_update_window: bool
    },
    ShowInterfaces,
    ShowSettings,
    ShowAbout,
    ShowHelp,
    ShowCheckUpdate,
    ResetSettings,
    Exit,
}

pub enum JoystickMessage {
    UpdateConfig(Update),
    UpdateJoystick(InputState),
    Exit,
}

pub enum MouseMessage {
    UpdateMouse(InputState),
    Exit,
}

pub enum KeyboardMessage {
    UpdateKeyboard(InputState),
    Exit,
}

pub enum ClientMessage {
    UpdateConfig(Update),
    Exit,
}
