use std::{cell::RefCell, net::IpAddr, path::PathBuf, rc::Rc, str::FromStr};

use fltk::{
    button::{Button, CheckButton},
    dialog::{choice2, FileChooser, FileChooserType},
    enums::Align,
    frame::Frame,
    group::{Flex, FlexType, Scroll, ScrollType, Tabs},
    input::{Input, IntInput},
    prelude::{GroupExt, InputExt, ValuatorExt, WidgetBase, WidgetExt, WindowExt, DisplayExt},
    valuator::{SliderType, ValueSlider},
    window::Window, text::{TextDisplay, WrapMode, TextBuffer}, misc::InputChoice
};
use fltk_theme::widget_schemes::fluent;
use tokio::sync::mpsc::Sender;

use crate::{config::{
        AppConfig,
        AxisUpdate,
        JoystickScriptUpdate,
        NetworkUpdate,
        Update,
        GeneralUpdate,
        theme::Theme
    },
    dialog_center_window, i18n,
    message::Message,
};

use super::send_message_sync;

pub struct GeneralWidgets {
    check_update_start: CheckButton,
    theme: InputChoice
}

type AxisWidget = ValueSlider;
pub struct NetworkWidgets {
    bind_address_input: Input,
    port_number_input: IntInput,
    preferred_interface_input: Input,
}

pub struct AxisWidgets {
    wheel_start: AxisWidget,
    wheel_end: AxisWidget,
    wheel_linearity: AxisWidget,

    gas_start: AxisWidget,
    gas_end: AxisWidget,
    gas_linearity: AxisWidget,

    brake_start: AxisWidget,
    brake_end: AxisWidget,
    brake_linearity: AxisWidget,

    thumb_x_start: AxisWidget,
    thumb_x_end: AxisWidget,
    thumb_x_linearity: AxisWidget,

    thumb_y_start: AxisWidget,
    thumb_y_end: AxisWidget,
    thumb_y_linearity: AxisWidget,
}
pub struct JoystickScriptWidgets {
    enabled: CheckButton,
    choose: Rc<RefCell<Button>>,
    path: Rc<RefCell<Frame>>,
}

pub struct SettingsWindow {
    pub window: Rc<RefCell<Window>>,
    pub general_widgets: GeneralWidgets,
    pub network_widgets: NetworkWidgets,
    pub axis_widgets: AxisWidgets,
    pub js_script_widgets: JoystickScriptWidgets,
}

impl SettingsWindow {
    pub fn new(app_tx: Sender<Message>, config: &AppConfig) -> Self {
        let width = 500;
        let height = 480;
        let window = Rc::new(RefCell::new(
            Window::default()
                .with_size(width, height)
                .with_label(i18n::settings())
                .center_screen(),
        ));
        let mut window_ref = window.borrow_mut();
        window_ref.make_modal(true);
        window_ref.size_range(350, 350, 0, 0);
        window_ref.end();

        let margin = 8;
        let tab_height = 32;
        let button_gap = 40;

        let mut tabs = Tabs::new(
            margin,
            margin,
            width - margin * 2,
            height - margin * 2 - button_gap,
            "",
        );
        tabs.set_frame(fltk::enums::FrameType::EngravedFrame);
        tabs.end();

        let mut general_column = Flex::new(
            margin,
            margin + tab_height,
            width - margin * 2,
            height - margin * 2 - tab_height - button_gap,
            "",
        )
        .with_label(i18n::general())
        .with_type(FlexType::Column);
        general_column.set_margin(margin);
        general_column.end();
        let general_widgets = populate_general_col(&mut general_column, app_tx.clone(), config);

        let mut network_column = Flex::new(
            margin,
            margin + tab_height,
            width - margin * 2,
            height - margin * 2 - tab_height - button_gap,
            "",
        )
        .with_label(i18n::network())
        .with_type(FlexType::Column);
        network_column.set_margin(margin);
        network_column.set_pad(margin);
        network_column.end();
        let network_widgets = populate_network_col(&mut network_column, app_tx.clone(), config);

        let mut axis_scroll = Scroll::new(
            margin,
            margin + tab_height,
            width - margin * 2,
            height - margin * 2 - tab_height - button_gap,
            "",
        )
        .with_label(i18n::axis())
        .with_type(ScrollType::Vertical);
        axis_scroll.end();

        let mut axis_col = Flex::new(margin, margin + tab_height, width - margin * 2 - 32, 50, "")
            .with_type(FlexType::Column);
        axis_col.set_margin(margin);
        axis_col.end();

        let axis_widgets = populate_axis_col(&mut axis_col, app_tx.clone(), config);
        axis_scroll.add(&axis_col);
        let axis_col = Rc::new(RefCell::new(axis_col));
        axis_scroll.resize_callback({
            let axis_col_ref = Rc::clone(&axis_col);
            move |_, _, _, w, _| {
                let (x, y, height) = if let Ok(borrowed) = axis_col_ref.try_borrow() {
                    let x = borrowed.x();
                    let y = borrowed.y();
                    (x, y, borrowed.height())
                } else {
                    return;
                };
                if let Ok(mut borrowed) = axis_col_ref.try_borrow_mut() {
                    borrowed.resize(x, y, w - 32, height);
                }
            }
        });

        let mut joystick_script_col = Flex::new(
            margin,
            margin + tab_height,
            width - margin * 2,
            height - margin * 2 - tab_height - button_gap,
            "",
        )
        .with_label(i18n::joystick_script())
        .with_type(FlexType::Column);
        joystick_script_col.set_margin(margin);
        joystick_script_col.end();
        let js_script_widgets =
            populate_js_script_col(&mut joystick_script_col, app_tx.clone(), config);

        tabs.resizable(&network_column);
        tabs.add(&general_column);
        tabs.add(&network_column);
        tabs.add(&axis_scroll);
        tabs.add(&joystick_script_col);

        let mut button_row = Flex::new(
            margin,
            height - button_gap,
            width - margin * 2,
            button_gap - margin,
            "",
        )
        .row();
        button_row.set_pad(margin);
        let center = dialog_center_window(&*window_ref);
        Button::default()
            .with_label(i18n::reset_settings())
            .set_callback(move |_| {
                if let Some(0) = choice2(
                    center.0,
                    center.1,
                    i18n::want_to_reset_settings(),
                    i18n::ok(),
                    i18n::cancel(),
                    "",
                ) {
                    send_message_sync(app_tx.clone(), Message::ResetSettings);
                }
            });
        Button::default().with_label(i18n::close()).set_callback({
            let window_ref = Rc::clone(&window);
            move |_| {
                window_ref.borrow_mut().hide();
            }
        });

        button_row.end();

        window_ref.resizable(&tabs);
        window_ref.add(&tabs);
        window_ref.add(&button_row);
        drop(window_ref);
        Self {
            window,
            general_widgets,
            network_widgets,
            axis_widgets,
            js_script_widgets,
        }
    }

    pub fn show(&mut self) {
        self.window.borrow_mut().show();
    }

    pub fn reset_widgets(&mut self, config: &AppConfig) {
        self.general_widgets.check_update_start
            .set_checked(config.general.check_update_start);
        self.general_widgets.theme
            .set_value_index(config.general.theme as i32);
            
        self.network_widgets
            .bind_address_input
            .set_value(&config.network.address);
        self.network_widgets
            .port_number_input
            .set_value(&config.network.port.to_string());
        self.network_widgets
            .preferred_interface_input
            .set_value(&config.network.preferred_interface);

        self.axis_widgets
            .wheel_start
            .set_value(config.axis.wheel.start as f64);
        self.axis_widgets
            .wheel_end
            .set_value(config.axis.wheel.end as f64);
        self.axis_widgets
            .wheel_linearity
            .set_value(config.axis.wheel.linearity as f64);

        self.axis_widgets
            .gas_start
            .set_value(config.axis.gas.start as f64);
        self.axis_widgets
            .gas_end
            .set_value(config.axis.gas.end as f64);
        self.axis_widgets
            .gas_linearity
            .set_value(config.axis.gas.linearity as f64);

        self.axis_widgets
            .brake_start
            .set_value(config.axis.brake.start as f64);
        self.axis_widgets
            .brake_end
            .set_value(config.axis.brake.end as f64);
        self.axis_widgets
            .brake_linearity
            .set_value(config.axis.brake.linearity as f64);

        self.axis_widgets
            .thumb_x_start
            .set_value(config.axis.thumb_x.start as f64);
        self.axis_widgets
            .thumb_x_end
            .set_value(config.axis.thumb_x.end as f64);
        self.axis_widgets
            .thumb_x_linearity
            .set_value(config.axis.thumb_x.linearity as f64);

        self.axis_widgets
            .thumb_y_start
            .set_value(config.axis.thumb_y.start as f64);
        self.axis_widgets
            .thumb_y_end
            .set_value(config.axis.thumb_y.end as f64);
        self.axis_widgets
            .thumb_y_linearity
            .set_value(config.axis.thumb_y.linearity as f64);

        self.js_script_widgets
            .enabled
            .set_checked(config.js_script.enabled);
        if self.js_script_widgets.enabled.is_checked() {
            self.js_script_widgets.choose.borrow_mut().activate();
        } else {
            self.js_script_widgets.choose.borrow_mut().deactivate();
        }
        {
            let label = if let Some(path) = &config.js_script.path {
                path.to_str().unwrap()
            } else {
                " "
            };
            self.js_script_widgets.path.borrow_mut().set_label(label);
        }
    }
}

fn populate_general_col(
    content: &mut Flex,
    app_tx: Sender<Message>,
    config: &AppConfig
) -> GeneralWidgets {
    content.begin();

    let mut check_update = CheckButton::default().with_label(i18n::check_update_at_startup());
    content.set_size(&check_update, check_update.measure_label().1 + 8);
    let mut theme_row = Flex::default().with_type(FlexType::Row);
    let theme_label = Frame::default().with_label(i18n::theme())
        .with_align(Align::Right | Align::Inside);
    let mut theme = InputChoice::default();
    for item in Theme::all() {
        theme.add(item);
    }
    theme_row.set_size(&theme_label, theme_label.measure_label().0 + 8);
    theme_row.end();
    content.set_size(&theme_row, theme_label.measure_label().1 + 8);
    content.end();
    if config.general.check_update_start {
        check_update.set_checked(true);
    }
    check_update.set_callback({
        let app_tx = app_tx.clone();
        move |check| {
        send_message_sync(
            app_tx.clone(),
            Message::UpdateConfig(Update::General(GeneralUpdate::CheckUpdateStart(
                check.is_checked(),
            ))),
        );
    }});
    theme.set_value_index(config.general.theme as i32);
    theme.set_callback(move |choice| {
        if let Some(val) = choice.value() {
            if let Ok(theme) = Theme::from_str(&val) {
                send_message_sync(
                    app_tx.clone(),
                    Message::UpdateConfig(Update::General(GeneralUpdate::Theme(
                        theme
                    ))),
                );
            }
        }
    });
    GeneralWidgets {
        check_update_start: check_update,
        theme
    }
}

fn populate_network_col(
    content: &mut Flex,
    app_tx: Sender<Message>,
    config: &AppConfig,
) -> NetworkWidgets {
    let labels = [
        Frame::default().with_label(i18n::bind_address())
            .with_align(Align::Right | Align::Inside),
        Frame::default().with_label(i18n::port())
            .with_align(Align::Right | Align::Inside),
        Frame::default().with_label(i18n::preferred_interface())
            .with_align(Align::Right | Align::Inside),
    ];
    let max_label_width: i32 = labels
        .iter()
        .map(|label| label.measure_label().0)
        .reduce(|acc, item| acc.max(item))
        .unwrap() as _;
    content.begin();

    let mut bind_address_row = Flex::default().with_type(FlexType::Row);

    bind_address_row.add(&labels[0]);
    let mut bind_address_input = Input::default();
    bind_address_input.set_value(&config.network.address);
    bind_address_input.set_callback({
        let app_tx = app_tx.clone();
        move |input| {
            if IpAddr::from_str(&input.value()).is_ok() {
                send_message_sync(
                    app_tx.clone(),
                    Message::UpdateConfig(Update::Network(NetworkUpdate::Address(input.value()))),
                );
            } else {
                let default_address = AppConfig::default().network.address;
                input.set_value(&default_address);
                send_message_sync(
                    app_tx.clone(),
                    Message::UpdateConfig(Update::Network(NetworkUpdate::Address(default_address))),
                );
            }
        }
    });
    bind_address_row.set_size(&labels[0], max_label_width);

    bind_address_row.end();
    content.set_size(&bind_address_row, 32);

    let mut port_number_row = Flex::default().with_type(FlexType::Row);

    port_number_row.add(&labels[1]);
    let mut port_number_input = IntInput::default();
    port_number_input.set_value(&config.network.port.to_string());
    port_number_input.set_callback({
        let app_tx = app_tx.clone();
        move |input| {
            match input.value().parse::<u16>() {
                Ok(value) => {
                    let clamped_value = 1.max(u16::MAX.min(value));
                    input.set_value(&clamped_value.to_string());
                    send_message_sync(
                        app_tx.clone(),
                        Message::UpdateConfig(Update::Network(NetworkUpdate::Port(clamped_value))),
                    );
                }
                Err(err) => match err.kind() {
                    std::num::IntErrorKind::PosOverflow => {
                        input.set_value(&u16::MAX.to_string());
                        send_message_sync(
                            app_tx.clone(),
                            Message::UpdateConfig(Update::Network(NetworkUpdate::Port(u16::MAX))),
                        );
                    }
                    std::num::IntErrorKind::NegOverflow | std::num::IntErrorKind::Zero => {
                        input.set_value(&u16::MIN.to_string());
                        send_message_sync(
                            app_tx.clone(),
                            Message::UpdateConfig(Update::Network(NetworkUpdate::Port(u16::MIN))),
                        );
                    }
                    _ => {
                        let default_port = AppConfig::default().network.port;
                        input.set_value(&default_port.to_string());
                        send_message_sync(
                            app_tx.clone(),
                            Message::UpdateConfig(Update::Network(NetworkUpdate::Port(
                                default_port,
                            ))),
                        );
                    }
                },
            };
        }
    });
    port_number_row.set_size(&labels[1], max_label_width);

    port_number_row.end();
    content.set_size(&port_number_row, 32);

    let mut preferred_interface_row = Flex::default().with_type(FlexType::Row);

    preferred_interface_row.add(&labels[2]);
    let mut preferred_interface_input = Input::default();
    preferred_interface_input.set_value(&config.network.preferred_interface);
    preferred_interface_input.set_callback(move |input| {
        send_message_sync(
            app_tx.clone(),
            Message::UpdateConfig(Update::Network(NetworkUpdate::PreferredInterface(
                input.value(),
            ))),
        );
    });
    preferred_interface_row.set_size(&labels[2], max_label_width);

    preferred_interface_row.end();
    content.set_size(&preferred_interface_row, 32);

    content.end();
    NetworkWidgets {
        bind_address_input,
        port_number_input,
        preferred_interface_input,
    }
}

macro_rules! add_axis {
    ($wheel:ident, $start:ident, $end:ident, $linearity:ident,
        $axis_height:expr, $axis_group_margin:expr, $title_height:expr, $max_label_width:expr,
    $app_tx:expr, $config:expr) => {{
        let mut col = Flex::default()
            .with_type(FlexType::Column)
            .with_align(Align::Top | Align::Inside);
        col.set_margin($axis_group_margin);
        col.set_frame(fluent::frames::OS_HOVERED_UP_FRAME);

        let title = Frame::default().with_label(i18n::$wheel());
        col.set_size(&title, $title_height);

        let mut start_row = Flex::default().with_type(FlexType::Row);
        let start_label = Frame::default().with_label(i18n::start())
            .with_align(Align::Right | Align::Inside);
        let mut start = create_axis_slider($config.axis.$wheel.start as u16, false);
        start.set_callback({
            let app_tx = $app_tx.clone();
            move |slider| {
                send_message_sync(
                    app_tx.clone(),
                    Message::UpdateConfig(Update::Axis(AxisUpdate::$start(slider.value() as u8))),
                );
            }
        });
        start_row.set_size(&start_label, $max_label_width);
        start_row.end();
        col.set_size(&start_row, $axis_height);

        let mut end_row = Flex::default().with_type(FlexType::Row);
        let end_label = Frame::default().with_label(i18n::end())
            .with_align(Align::Right | Align::Inside);
        let mut end = create_axis_slider($config.axis.$wheel.end, true);
        end.set_callback({
            let app_tx = $app_tx.clone();
            move |slider| {
                send_message_sync(
                    app_tx.clone(),
                    Message::UpdateConfig(Update::Axis(AxisUpdate::$end(slider.value() as u16))),
                );
            }
        });
        end_row.set_size(&end_label, $max_label_width);
        end_row.end();
        col.set_size(&end_row, $axis_height);

        let mut linearity_row = Flex::default().with_type(FlexType::Row);
        let linearity_label = Frame::default().with_label(i18n::linearity())
            .with_align(Align::Right | Align::Inside);
        let mut linearity = create_axis_slider($config.axis.$wheel.linearity as u16, false);
        linearity.set_callback({
            let app_tx = $app_tx.clone();
            move |slider| {
                send_message_sync(
                    app_tx.clone(),
                    Message::UpdateConfig(Update::Axis(AxisUpdate::$linearity(
                        slider.value() as u8
                    ))),
                );
            }
        });
        linearity_row.set_size(&linearity_label, $max_label_width);
        linearity_row.end();
        col.set_size(&linearity_row, $axis_height);

        col.end();
        (start, end, linearity)
    }};
}

fn populate_axis_col(
    content: &mut Flex,
    app_tx: Sender<Message>,
    config: &AppConfig,
) -> AxisWidgets {
    let axis_height = 32;
    let axis_group_margin = 8;
    let title_height = 24;
    let labels = [
        Frame::default().with_label(i18n::start()),
        Frame::default().with_label(i18n::end()),
        Frame::default().with_label(i18n::linearity()),
    ];
    let max_label_width: i32 = labels
        .iter()
        .map(|label| label.measure_label().0)
        .reduce(|acc, item| acc.max(item))
        .unwrap() as _;
    content.begin();

    let (wheel_start, wheel_end, wheel_linearity) = add_axis!(
        wheel,
        WheelStart,
        WheelEnd,
        WheelLinearity,
        axis_height,
        axis_group_margin,
        title_height,
        max_label_width,
        app_tx,
        config
    );

    let (gas_start, gas_end, gas_linearity) = add_axis!(
        gas,
        GasStart,
        GasEnd,
        GasLinearity,
        axis_height,
        axis_group_margin,
        title_height,
        max_label_width,
        app_tx,
        config
    );

    let (brake_start, brake_end, brake_linearity) = add_axis!(
        brake,
        BrakeStart,
        BrakeEnd,
        BrakeLinearity,
        axis_height,
        axis_group_margin,
        title_height,
        max_label_width,
        app_tx,
        config
    );

    let (thumb_x_start, thumb_x_end, thumb_x_linearity) = add_axis!(
        thumb_x,
        ThumbXStart,
        ThumbXEnd,
        ThumbXLinearity,
        axis_height,
        axis_group_margin,
        title_height,
        max_label_width,
        app_tx,
        config
    );

    let (thumb_y_start, thumb_y_end, thumb_y_linearity) = add_axis!(
        thumb_y,
        ThumbYStart,
        ThumbYEnd,
        ThumbYLinearity,
        axis_height,
        axis_group_margin,
        title_height,
        max_label_width,
        app_tx,
        config
    );

    WidgetExt::set_size(
        content,
        content.width(),
        axis_height * 3 * 5 + title_height * 5 + axis_group_margin * 5 * 5,
    );
    content.end();
    AxisWidgets {
        wheel_start,
        wheel_end,
        wheel_linearity,
        gas_start,
        gas_end,
        gas_linearity,
        brake_start,
        brake_end,
        brake_linearity,
        thumb_x_start,
        thumb_x_end,
        thumb_x_linearity,
        thumb_y_start,
        thumb_y_end,
        thumb_y_linearity,
    }
}

fn create_axis_slider(value: u16, end: bool) -> AxisWidget {
    let mut slider = AxisWidget::default().with_type(SliderType::Horizontal);
    slider.set_range(0.0, if end { 500.0 } else { 100.0 });
    slider.set_precision(0);
    slider.set_value(value as f64);
    slider
}

fn populate_js_script_col(
    content: &mut Flex,
    app_tx: Sender<Message>,
    config: &AppConfig,
) -> JoystickScriptWidgets {
    content.begin();

    let mut desc_buffer = TextBuffer::default();
    desc_buffer.set_text(i18n::js_script_desc());
    let mut description = TextDisplay::default().with_align(Align::TopLeft);
    description.wrap_mode(WrapMode::AtBounds, 16);
    description.set_frame(fltk::enums::FrameType::NoBox);
    description.set_buffer(desc_buffer);
    description.set_scrollbar_align(Align::Clip);
    description.set_color(fltk::enums::Color::BackGround);
    content.set_size(&description, 54);

    let mut enabled = CheckButton::default().with_label(i18n::enabled());
    content.set_size(&enabled, enabled.measure_label().1 + 8);

    let mut choose_button = Button::default().with_label(i18n::select_lua_script());
    if !config.js_script.enabled {
        choose_button.deactivate();
    } else {
        enabled.set_checked(true);
    }
    content.set_size(&choose_button, choose_button.measure_label().1 + 16);

    let mut current_path_frame = Frame::default()
        .with_label(" ")
        .with_align(Align::TopRight | Align::Inside | Align::Wrap);
    content.set_size(
        &current_path_frame,
        current_path_frame.measure_label().1 * 2,
    );
    if let Some(ref path) = config.js_script.path {
        current_path_frame.set_label(&format_script_path(path.to_str().unwrap()));
    }
    let current_path_frame = Rc::new(RefCell::new(current_path_frame));

    choose_button.set_callback({
        let mut current_path = config.js_script.path.clone();
        let current_path_frame = Rc::clone(&current_path_frame);
        let app_tx = app_tx.clone();
        move |_| {
            let start_path = match current_path.as_ref() {
                Some(path) => path.clone(),
                None => match std::env::current_exe() {
                    Ok(mut exe_path) => {
                        exe_path.pop();
                        exe_path
                    }
                    Err(_) => PathBuf::default(),
                },
            };
            let mut file_chooser = FileChooser::new(
                start_path,
                "*.lua",
                FileChooserType::Single,
                i18n::select_lua_script(),
            );
            file_chooser.set_preview(false);
            file_chooser.show();
            while file_chooser.shown() {
                fltk::app::wait();
            }
            if let Some(path) = file_chooser.value(1) {
                current_path = Some(PathBuf::from_str(&path).unwrap());
                current_path_frame
                    .borrow_mut()
                    .set_label(&format_script_path(&path));
                send_message_sync(
                    app_tx.clone(),
                    Message::UpdateConfig(Update::JoystickScript(JoystickScriptUpdate::Path(
                        current_path.clone(),
                    ))),
                );
            }
        }
    });
    let choose_button = Rc::new(RefCell::new(choose_button));
    enabled.set_callback({
        let choose_button = Rc::clone(&choose_button);
        move |check| {
            if check.is_checked() {
                choose_button.borrow_mut().activate();
            } else {
                choose_button.borrow_mut().deactivate();
            }
            send_message_sync(
                app_tx.clone(),
                Message::UpdateConfig(Update::JoystickScript(JoystickScriptUpdate::Enabled(
                    check.is_checked(),
                ))),
            );
        }
    });

    content.end();

    JoystickScriptWidgets {
        enabled,
        path: current_path_frame,
        choose: choose_button,
    }
}

fn format_script_path(path: &str) -> String {
    format!("{}: {}", i18n::script_path(), path)
}
