mod about_window;
mod interface_window;
pub mod main_window;
mod qr_window;
mod settings_window;
pub mod check_update_window;

pub use about_window::AboutWindow;
pub use interface_window::InterfaceWindow;
pub use main_window::MainWindow;
pub use settings_window::SettingsWindow;

use crate::message::Message;
use tokio::sync::mpsc::Sender;

pub(self) fn send_message_sync(app_tx: Sender<Message>, message: Message) {
    let handle = tokio::runtime::Handle::current();
    let _guard = handle.enter();
    handle.spawn(async move {
        let _ = app_tx.send(message).await;
        fltk::app::awake();
    });
}
