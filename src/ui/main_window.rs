use std::{cell::RefCell, rc::Rc};

use fltk::{
    button::Button,
    dialog::alert,
    draw::Offscreen,
    enums::{Align, Color, Shortcut},
    frame::Frame,
    group::{Flex, FlexType},
    image::PngImage,
    menu::{MenuFlag, SysMenuBar},
    prelude::{DisplayExt, GroupExt, MenuExt, WidgetBase, WidgetExt, WindowExt},
    text::{TextBuffer, TextDisplay, WrapMode},
    window::Window,
};
use tokio::sync::{mpsc::Sender, broadcast};

use crate::{
    dialog_center_screen, error::LoadAppIcon, error_message_chain, i18n, input_state,
    message::Message, proto_capnp, DATA_DIR, config::theme::Theme,
};

use super::send_message_sync;

pub enum Status {
    Client,
    Joystick,
    Keyboard,
    Mouse,
}

pub struct MainWindow {
    pub window: Window,
    pub status: TextDisplay,
    pub client_status: String,
    pub joystick_status: String,
    pub keyboard_status: String,
    pub mouse_status: String,
    pub wheel: Frame,
    pub gas: Frame,
    pub brake: Frame,
    pub thumb_x: Frame,
    pub thumb_y: Frame,
    pub gear: Frame,
    pub screen_buttons: [Button; 8],
    pub volume_buttons: [Button; 2],
    input_data: Rc<RefCell<input_state::InputState>>,
}

impl MainWindow {
    pub fn new(app_tx: Sender<Message>, theme_tx: &broadcast::Sender<Theme>) -> Self {
        let input_data = Rc::new(RefCell::new(input_state::InputState::default()));
        let width = 800;
        let height = 500;
        let mut window = Window::default().with_size(width, height).center_screen();
        window.set_label(&format!(
            "{} - v{}",
            i18n::app_name(),
            env!("CARGO_PKG_VERSION")
        ));
        match PngImage::load(DATA_DIR.get().unwrap().join("image/icon.png")) {
            Ok(image) => {
                window.set_icon(Some(image));
            }
            Err(err) => {
                let center = dialog_center_screen();
                alert(
                    center.0,
                    center.1,
                    &error_message_chain(&LoadAppIcon::from(err)),
                );
            }
        };
        window.set_callback({
            let app_tx = app_tx.clone();
            move |_| send_message_sync(app_tx.clone(), Message::Exit)
        });
        window.make_resizable(true);
        let mut main_column = Flex::default()
            .with_type(FlexType::Column)
            .with_size(width, height);

        let menu = create_menu(app_tx);
        Flex::set_size(&mut main_column, &menu, 32);

        let mut content_column = Flex::default().with_type(FlexType::Column);
        content_column.set_margin(8);

        content_column.end();
        let labels = [
            Frame::default().with_label(i18n::wheel()),
            Frame::default().with_label(i18n::gas()),
            Frame::default().with_label(i18n::brake()),
            Frame::default().with_label(i18n::thumb_x()),
            Frame::default().with_label(i18n::thumb_y()),
            Frame::default().with_label(i18n::screen_buttons()),
            Frame::default().with_label(i18n::volume_buttons()),
        ];
        let max_label_width: i32 = labels
            .iter()
            .map(|label| label.measure_label().0)
            .reduce(|acc, item| acc.max(item))
            .unwrap() as _;
        content_column.begin();

        let mut indicator_column = Flex::default().with_type(FlexType::Column);
        indicator_column.set_pad(10);

        let mut wheel_row = Flex::default().with_type(FlexType::Row);
        Flex::add(&mut wheel_row, &labels[0]);
        let mut wheel = Frame::default();
        let data_ref = Rc::clone(&input_data);
        WidgetBase::draw(&mut wheel, {
            let mut theme_rx = theme_tx.subscribe();
            let mut cur_theme = None::<Theme>;
            move |frame| {
                if let Ok(theme) = theme_rx.try_recv() {
                    cur_theme.replace(theme);
                }
                if let Some(offscreen) = Offscreen::new(frame.width(), frame.height()) {
                    let data_borrow = RefCell::borrow(&data_ref);
                    draw_axis(
                        data_borrow.wheel,
                        true,
                        frame,
                        offscreen,
                        cur_theme.as_ref().unwrap_or(&Theme::Aero)
                    );
                }
            }
        });
        Flex::set_size(&mut wheel_row, &labels[0], max_label_width);
        wheel_row.end();

        let mut gas_row = Flex::default().with_type(FlexType::Row);
        Flex::add(&mut gas_row, &labels[1]);
        let mut gas = Frame::default();
        let data_ref = Rc::clone(&input_data);
        WidgetBase::draw(&mut gas, {
            let mut theme_rx = theme_tx.subscribe();
            let mut cur_theme = None::<Theme>;
            move |frame| {
                if let Ok(theme) = theme_rx.try_recv() {
                    cur_theme.replace(theme);
                }
                if let Some(offscreen) = Offscreen::new(frame.width(), frame.height()) {
                    let data_borrow = RefCell::borrow(&data_ref);
                    draw_axis(
                        data_borrow.gas,
                        false,
                        frame,
                        offscreen,
                        cur_theme.as_ref().unwrap_or(&Theme::Aero)
                    );
                }
            }
        });
        Flex::set_size(&mut gas_row, &labels[1], max_label_width);
        gas_row.end();

        let mut brake_row = Flex::default().with_type(FlexType::Row);
        Flex::add(&mut brake_row, &labels[2]);
        let mut brake = Frame::default();
        let data_ref = Rc::clone(&input_data);
        WidgetBase::draw(&mut brake, {
            let mut theme_rx = theme_tx.subscribe();
            let mut cur_theme = None::<Theme>;
            move |frame| {
                if let Ok(theme) = theme_rx.try_recv() {
                    cur_theme.replace(theme);
                }
                if let Some(offscreen) = Offscreen::new(frame.width(), frame.height()) {
                    let data_borrow = RefCell::borrow(&data_ref);
                    draw_axis(
                        data_borrow.brake,
                        false,
                        frame,
                        offscreen,
                        cur_theme.as_ref().unwrap_or(&Theme::Aero)
                    );
                }
            }
        });
        Flex::set_size(&mut brake_row, &labels[2], max_label_width);
        brake_row.end();

        let mut thumb_x_row = Flex::default().with_type(FlexType::Row);
        Flex::add(&mut thumb_x_row, &labels[3]);
        let mut thumb_x = Frame::default();
        let data_ref = Rc::clone(&input_data);
        WidgetBase::draw(&mut thumb_x, {
            let mut theme_rx = theme_tx.subscribe();
            let mut cur_theme = None::<Theme>;
            move |frame| {
                if let Ok(theme) = theme_rx.try_recv() {
                    cur_theme.replace(theme);
                }
                if let Some(offscreen) = Offscreen::new(frame.width(), frame.height()) {
                    let data_borrow = RefCell::borrow(&data_ref);
                    draw_axis(
                        data_borrow.thumb.0,
                        true,
                        frame,
                        offscreen,
                        cur_theme.as_ref().unwrap_or(&Theme::Aero)
                    );
                }
            }
        });
        Flex::set_size(&mut thumb_x_row, &labels[3], max_label_width);
        thumb_x_row.end();

        let mut thumb_y_row = Flex::default().with_type(FlexType::Row);
        Flex::add(&mut thumb_y_row, &labels[4]);
        let mut thumb_y = Frame::default();
        let data_ref = Rc::clone(&input_data);
        WidgetBase::draw(&mut thumb_y, {
            let mut theme_rx = theme_tx.subscribe();
            let mut cur_theme = None::<Theme>;
            move |frame| {
                if let Ok(theme) = theme_rx.try_recv() {
                    cur_theme.replace(theme);
                }
                if let Some(offscreen) = Offscreen::new(frame.width(), frame.height()) {
                    let data_borrow = RefCell::borrow(&data_ref);
                    draw_axis(
                        data_borrow.thumb.1,
                        true,
                        frame,
                        offscreen,
                        cur_theme.as_ref().unwrap_or(&Theme::Aero)
                    );
                }
            }
        });
        Flex::set_size(&mut thumb_y_row, &labels[4], max_label_width);
        thumb_y_row.end();

        let gear = Frame::default().with_label(&i18n::gear_label(proto_capnp::Gear::N));

        let mut screen_button_row = Flex::default().with_type(FlexType::Row);
        Flex::add(&mut screen_button_row, &labels[5]);
        Flex::set_size(&mut screen_button_row, &labels[5], max_label_width);
        let screen_buttons: [Button; 8] = (0..8)
            .map(|i| {
                let mut button = Button::default().with_label(&(i + 1).to_string());
                button.deactivate();
                button
            })
            .collect::<Vec<Button>>()
            .try_into()
            .unwrap();
        screen_button_row.end();

        let mut volume_button_row = Flex::default().with_type(FlexType::Row);
        Flex::add(&mut volume_button_row, &labels[6]);
        Flex::set_size(&mut volume_button_row, &labels[6], max_label_width);
        let mut vol_up_button = Button::default().with_label("+");
        vol_up_button.deactivate();
        let mut vol_down_button = Button::default().with_label("-");
        vol_down_button.deactivate();
        let volume_buttons = [vol_down_button, vol_up_button];
        volume_button_row.end();

        indicator_column.end();

        let mut status = TextDisplay::default().with_align(Align::TopLeft);
        status.wrap_mode(WrapMode::AtBounds, 16);
        status.set_frame(fltk_theme::widget_schemes::aqua::frames::OS_DEFAULT_BUTTON_UP_BOX);
        status.set_buffer(TextBuffer::default());
        content_column.set_size(&indicator_column, 340);
        content_column.end();
        main_column.end();
        window.end();
        Self {
            window,
            status,
            client_status: i18n::receiver_status().to_owned(),
            joystick_status: i18n::joystick_status().to_owned(),
            keyboard_status: i18n::keyboard_status().to_owned(),
            mouse_status: i18n::mouse_status().to_owned(),
            wheel,
            gas,
            brake,
            thumb_x,
            thumb_y,
            gear,
            screen_buttons,
            volume_buttons,
            input_data,
        }
    }

    pub fn update_status(&mut self, text: &str, status: Status) {
        let text_to_change = match status {
            Status::Client => &mut self.client_status,
            Status::Joystick => &mut self.joystick_status,
            Status::Keyboard => &mut self.keyboard_status,
            Status::Mouse => &mut self.mouse_status,
        };
        if String::eq(text_to_change, text) {
            return;
        }
        *text_to_change = text.to_owned();
        let mut buffer = self.status.buffer();
        let buffer_ref = buffer.as_mut().unwrap();
        buffer_ref.remove(0, buffer_ref.length());
        buffer_ref.append(&format!(
            "{}\n{}\n{}\n{}",
            self.client_status, self.joystick_status, self.keyboard_status, self.mouse_status
        ));
    }

    pub fn update_data(&mut self, state: input_state::InputState) {
        let old_input = self.input_data.replace(state);
        let input_ref = self.input_data.borrow();
        if old_input.wheel != input_ref.wheel {
            self.wheel.redraw();
        }
        if old_input.gas != input_ref.gas {
            self.gas.redraw();
        }
        if old_input.brake != input_ref.brake {
            self.brake.redraw();
        }
        if old_input.thumb.0 != input_ref.thumb.0 {
            self.thumb_x.redraw();
        }
        if old_input.thumb.1 != input_ref.thumb.1 {
            self.thumb_y.redraw();
        }
        if old_input.gear != input_ref.gear {
            self.gear.set_label(&i18n::gear_label(input_ref.gear))
        }
        for i in 0..8 {
            if !old_input.is_secondary_mode_active && input_ref.is_secondary_mode_active {
                self.screen_buttons[i].set_label(&(i + 1 + 8).to_string());
            } else if old_input.is_secondary_mode_active && !input_ref.is_secondary_mode_active {
                self.screen_buttons[i].set_label(&(i + 1).to_string());
            }
            if old_input.screen_buttons[i] && !input_ref.screen_buttons[i] {
                self.screen_buttons[i].deactivate();
            } else if !old_input.screen_buttons[i] && input_ref.screen_buttons[i] {
                self.screen_buttons[i].activate();
            }
            if i < 2 {
                if old_input.volume_buttons[i] && !input_ref.volume_buttons[i] {
                    self.volume_buttons[i].deactivate();
                } else if !old_input.volume_buttons[i] && input_ref.volume_buttons[i] {
                    self.volume_buttons[i].activate();
                }
            }
        }
    }
}

fn draw_axis(
        value: i16,
        centered: bool,
        frame: &mut Frame,
        offscreen: Offscreen,
        theme: &Theme
    ) {
    offscreen.begin();
    use fltk::draw::*;
    let arc_width = 25;
    draw_rect_fill(0, 0, frame.width(), frame.height(), Color::BackGround);

    let (value_x, origin_x) = if centered {
        (
            ((value as f32 / 32767.0 + 1.0) / 2.0) * frame.width() as f32,
            frame.width() as f32 / 2.0,
        )
    } else {
        (
            (-(value as f32) + 32767.0) / 65534.0 * frame.width() as f32,
            0.0,
        )
    };
    let clip_x = value_x.min(origin_x) as i32;
    let clip_width = (value_x - origin_x).abs() as i32;
    push_clip(clip_x, 0, clip_width, frame.height());
    let liq_col = if !centered || value > 0 {
        theme.positive_bar_color()
    } else {
        theme.negative_bar_color()
    };
    set_color_rgb(liq_col.0, liq_col.1, liq_col.2);
    draw_pie(0, 0, arc_width, frame.height(), 90.0, 270.0);
    draw_pie(
        frame.width() - arc_width,
        0,
        arc_width,
        frame.height(),
        -90.0,
        90.0,
    );
    draw_rect_fill(
        arc_width / 2,
        0,
        frame.width() - arc_width + 1,
        frame.height(),
        Color::from_rgb(liq_col.0, liq_col.1, liq_col.2),
    );
    pop_clip();
    let border_color = theme.bar_border_color();
    set_color_rgb(border_color.0, border_color.1, border_color.2);
    draw_arc(0, 0, arc_width, frame.height(), 90.0, 270.0);
    draw_arc(
        frame.width() - arc_width,
        0,
        arc_width,
        frame.height(),
        -90.0,
        90.0,
    );
    draw_line(arc_width / 2, 0, frame.width() - arc_width / 2, 0);
    draw_line(
        arc_width / 2,
        frame.height() - 1,
        frame.width() - arc_width / 2,
        frame.height() - 1,
    );
    offscreen.end();
    offscreen.copy(frame.x(), frame.y(), frame.w(), frame.h(), 0, 0);
}

fn create_menu(app_tx: Sender<Message>) -> SysMenuBar {
    let mut menu = SysMenuBar::default();
    let app_tx_clone = app_tx.clone();
    menu.add(
        &format!("{}/{}", i18n::application(), i18n::settings()),
        Shortcut::empty(),
        MenuFlag::Normal,
        move |_| send_message_sync(app_tx_clone.clone(), Message::ShowSettings),
    );
    let app_tx_clone = app_tx.clone();
    menu.add(
        &format!("{}/{}", i18n::application(), i18n::exit()),
        Shortcut::empty(),
        MenuFlag::Normal,
        move |_| send_message_sync(app_tx_clone.clone(), Message::Exit),
    );
    let app_tx_clone = app_tx.clone();
    menu.add(
        &format!("{}/{}", i18n::information(), i18n::network_interfaces()),
        Shortcut::empty(),
        MenuFlag::Normal,
        move |_| send_message_sync(app_tx_clone.clone(), Message::ShowInterfaces),
    );
    let app_tx_clone = app_tx.clone();
    menu.add(
        &format!("{}/{}", i18n::information(), i18n::help()),
        Shortcut::empty(),
        MenuFlag::Normal,
        move |_| send_message_sync(app_tx_clone.clone(), Message::ShowHelp),
    );
    let app_tx_clone = app_tx.clone();
    menu.add(
        &format!("{}/{}", i18n::information(), i18n::about()),
        Shortcut::empty(),
        MenuFlag::Normal,
        move |_| send_message_sync(app_tx_clone.clone(), Message::ShowAbout),
    );
    let app_tx_clone = app_tx.clone();
    menu.add(
        &format!("{}/{}", i18n::information(), i18n::check_for_update()),
        Shortcut::empty(),
        MenuFlag::Normal,
        move |_| send_message_sync(app_tx_clone.clone(), Message::ShowCheckUpdate),
    );
    menu
}
