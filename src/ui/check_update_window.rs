use std::{cell::RefCell, rc::Rc};

use fltk::{
    button::Button,
    group::{Flex, FlexType},
    prelude::{DisplayExt, GroupExt, WidgetBase, WidgetExt, WindowExt},
    text::{TextBuffer, TextDisplay, WrapMode},
    window::Window,
};
use tokio::sync::mpsc::Sender;

use crate::{
    error::{self, CheckUpdate}, error_message_chain, i18n,
    update_check::{LatestVersionInfo, Version}, message::Message, config::{Update, HiddenUpdate},
};

use super::send_message_sync;

pub struct CheckUpdateWindow {
    window: Rc<RefCell<Window>>,
    info_display: TextDisplay,
    latest_info_result: Rc<RefCell<Option<Result<LatestVersionInfo, error::CheckUpdate>>>>
}

impl CheckUpdateWindow {
    pub fn new(app_tx: Sender<Message>) -> Self {
        let width = 600;
        let height = 300;
        let window = Rc::new(RefCell::new(
            Window::default()
                .with_size(width, height)
                .with_label(i18n::title_update_check())
                .center_screen(),
        ));
        let mut window_ref = window.borrow_mut();
        window_ref.make_resizable(true);
        window_ref.make_modal(true);

        let mut content_col = Flex::new(0, 0, width, height, "").with_type(FlexType::Column);
        content_col.set_margin(8);
        content_col.set_pad(8);

        let mut info_display = TextDisplay::default();
        info_display.wrap_mode(WrapMode::AtBounds, 8);
        info_display.set_frame(fltk_theme::widget_schemes::aqua::frames::OS_DEFAULT_BUTTON_UP_BOX);
        let mut info_buffer = TextBuffer::default();
        info_buffer.set_text(i18n::checking_update_dots());
        info_display.set_buffer(info_buffer);

        let mut button_row = Flex::new(0, 0, width, height, "").with_type(FlexType::Row);
        button_row.set_pad(8);

        let mut ignore_button = Button::default().with_label(i18n::ignore());
        let latest_info_result = Rc::new(
            RefCell::new(None::<Result<LatestVersionInfo, CheckUpdate>>)
        );
        ignore_button.set_callback({
            let latest_version_info_ref = Rc::clone(&latest_info_result);
            let window_ref = Rc::clone(&window);
            move |_| {
                let opt_info_result = latest_version_info_ref.borrow();
                if let Some(Ok(info)) = opt_info_result.as_ref() {
                    let message = Message::UpdateConfig(
                        Update::Hidden(HiddenUpdate::IgnoredVersion(Some(info.version.clone())))
                    );
                    let app_tx = app_tx.clone();
                    send_message_sync(app_tx, message);
                }
                window_ref.borrow_mut().hide();
            }
        });

        let mut download_button = Button::default().with_label(i18n::download());
        download_button.set_callback(move |_| {
            let _ = open::that(i18n::string_download_page_url());
        });

        let mut close_button = Button::default().with_label(i18n::close());
        close_button.set_callback({
            let window_ref = Rc::clone(&window);
            move |_| {
                window_ref.borrow_mut().hide();
            }
        });

        button_row.end();
        
        content_col.set_size(&button_row, close_button.measure_label().1 + 16);
        content_col.end();
        window_ref.end();
        drop(window_ref);
        Self {
            window,
            info_display,
            latest_info_result
        }
    }

    pub fn show(&mut self) {
        self.info_display
            .buffer()
            .unwrap()
            .set_text(i18n::checking_update_dots());
        self.window.borrow_mut().show();
    }

    pub fn process_info_result(
        &mut self,
        info_result: Result<LatestVersionInfo, error::CheckUpdate>,
        show_update_window: bool,
    ) {
        match &info_result {
            Ok(info) => self.display_info(info, show_update_window),
            Err(err) => {
                let content = error_message_chain(&err);
                self.info_display.buffer().unwrap().set_text(&content);
            }
        }
        self.latest_info_result.borrow_mut().replace(info_result);
    }

    fn display_info(&mut self, info: &LatestVersionInfo, show_update_window: bool) {
        match Version::get_current() {
            Ok(version) => {
                if info.version == version {
                    self.info_display
                        .buffer()
                        .unwrap()
                        .set_text(i18n::app_up_to_date());
                } else if info.version < version {
                    self.info_display
                        .buffer()
                        .unwrap()
                        .set_text(i18n::invalid_version_info_try_again());
                } else {
                    let mut content = format!(
                        "{}\n\n{}:\n",
                        i18n::new_version_available(info.version),
                        i18n::changes()
                    );
                    let mut changes_not_found = false;
                    let empty_changes = vec![];
                    let changes = info.changes.get(&i18n::get_current_lang().to_string())
                        .unwrap_or_else(|| {
                        info.changes.get("en").unwrap_or_else(|| {
                            changes_not_found = true;
                            content.push_str(i18n::latest_version_changes_not_found());
                            &empty_changes
                        })
                    });
                    if !changes_not_found {
                        for change in changes {
                            content.push_str(&format!("- {}\n", change));
                        }
                    }
                    self.info_display.buffer().unwrap().set_text(&content);
                    if show_update_window {
                        self.window.borrow_mut().show();
                    }
                }
            }
            Err(err) => {
                let content = error_message_chain(&err);
                self.info_display.buffer().unwrap().set_text(&content);
            }
        }
    }
}
