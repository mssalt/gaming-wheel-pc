use fltk::{
    enums::Event,
    frame::Frame,
    image::BmpImage,
    prelude::{GroupExt, WidgetBase, WidgetExt, WindowExt},
    window::Window,
};

use crate::i18n;

pub struct QrWindow {
    pub window: Window,
}

impl QrWindow {
    pub fn new() -> Self {
        let width = 300;
        let height = 300;
        let mut window = Window::default()
            .with_size(width, height)
            .center_screen();
        window.make_modal(true);
        window.end();
        window.handle(|window, event| {
            if event == Event::Push {
                window.hide();
            }
            true
        });
        Self { window }
    }

    pub fn show(&mut self, value: String) {
        self.window.clear();
        self.window.set_label(&i18n::qr_code_title(&value));
        let qr_code = qr_code::QrCode::new(value.as_bytes()).unwrap();
        let bmp = qr_code.to_bmp();
        let mut bmp_bytes = Vec::new();
        bmp.write(&mut bmp_bytes).unwrap();
        let qr_image = BmpImage::from_data(bmp_bytes.as_slice()).unwrap();
        self.window.begin();
        let margin = 16;
        let mut qr_frame = Frame::default().with_pos(margin, margin).with_size(
            self.window.width() - margin * 2,
            self.window.height() - margin * 2,
        );
        qr_frame.set_image_scaled(Some(qr_image));
        self.window.end();
        self.window.show();
    }
}
