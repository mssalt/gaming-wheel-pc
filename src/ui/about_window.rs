use std::{cell::RefCell, rc::Rc};

use fltk::{
    button::Button,
    frame::Frame,
    group::{Flex, FlexType},
    prelude::{GroupExt, WidgetBase, WidgetExt, WindowExt},
    window::Window,
};

use crate::{i18n, NETWORK_PROTOCOL_VERSION};

pub struct AboutWindow {
    pub window: Rc<RefCell<Window>>,
}

impl AboutWindow {
    pub fn new() -> Self {
        let width = 400;
        let height = 200;
        let window = Rc::new(RefCell::new(
            Window::default()
                .with_size(width, height)
                .with_label(i18n::about())
                .center_screen(),
        ));
        let mut window_ref = window.borrow_mut();
        window_ref.make_resizable(true);
        window_ref.make_modal(true);

        let mut content_col = Flex::new(0, 0, width, height, "").with_type(FlexType::Column);
        content_col.set_margin(8);

        let mut app_name = Frame::default().with_label(i18n::app_name());
        app_name.set_label_size(24);
        Frame::default().with_label(&format!(
            "{}: {}",
            i18n::authors(),
            env!("CARGO_PKG_AUTHORS")
        ));
        Frame::default().with_label(&format!(
            "{}: {}",
            i18n::app_version(),
            env!("CARGO_PKG_VERSION")
        ));
        Frame::default().with_label(&format!(
            "{}: {}",
            i18n::network_protocol_version(),
            NETWORK_PROTOCOL_VERSION
        ));
        let mut close_button = Button::default().with_label(i18n::close());
        close_button.set_callback({
            let window_ref = Rc::clone(&window);
            move |_| {
                window_ref.borrow_mut().hide();
            }
        });
        content_col.set_size(&close_button, close_button.measure_label().1 + 16);

        content_col.end();

        window_ref.end();

        drop(window_ref);

        Self { window }
    }

    pub fn show(&mut self) {
        self.window.borrow_mut().show();
    }
}
