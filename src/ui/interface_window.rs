use std::{cell::RefCell, rc::Rc};

use fltk::{
    app::copy,
    button::Button,
    frame::Frame,
    group::{Flex, FlexType, Scroll, ScrollType},
    prelude::{GroupExt, WidgetBase, WidgetExt, WindowExt},
    window::Window, enums::Align,
};

use crate::{client, error_message_chain, i18n, net_interfaces};

use super::qr_window::QrWindow;

pub struct InterfaceWindow {
    pub window: Rc<RefCell<Window>>,
    pub description: Rc<RefCell<Frame>>,
    pub content: Rc<RefCell<Flex>>,
}

impl InterfaceWindow {
    pub fn new() -> Self {
        let width = 600;
        let height = 300;
        let window = Rc::new(RefCell::new(
            Window::default()
                .with_size(width, height)
                .with_label(i18n::network_interfaces())
                .center_screen(),
        ));
        let mut window_ref = window.borrow_mut();
        window_ref.make_resizable(true);
        window_ref.make_modal(true);
        let mut main_column = Flex::default()
            .with_type(FlexType::Column)
            .with_size(width, height);
        main_column.set_margin(8);

        let description = Rc::new(RefCell::new(
            Frame::default().with_label(i18n::list_of_available_net_interfaces()),
        ));
        let desc_ref = description.borrow();
        Flex::set_size(&mut main_column, &*desc_ref, desc_ref.measure_label().1);

        let mut scroll = Scroll::default().with_type(ScrollType::Vertical);
        let content_column = Rc::new(RefCell::new(Flex::default().with_type(FlexType::Column)));
        let mut content_ref = content_column.borrow_mut();
        content_ref.set_margin(8);
        content_ref.set_frame(fltk::enums::FrameType::EmbossedBox);
        content_ref.end();
        scroll.end();

        let button_row = Flex::default().with_type(FlexType::Row);
        let mut refresh_button = Button::default().with_label(i18n::refresh());
        refresh_button.set_callback({
            let content_ref = Rc::clone(&content_column);
            let desc_ref = Rc::clone(&description);
            let window_ref = Rc::clone(&window);
            move |_| {
                window_ref.borrow_mut().hide();
                refresh(&mut content_ref.borrow_mut(), &mut desc_ref.borrow_mut());
                window_ref.borrow_mut().show();
            }
        });
        let mut close_button = Button::default().with_label(i18n::close());
        close_button.set_callback({
            let window_ref = Rc::clone(&window);
            move |_| {
                window_ref.borrow_mut().hide();
            }
        });
        button_row.end();
        let max_height = close_button
            .measure_label()
            .1
            .max(refresh_button.measure_label().1);
        main_column.set_size(&button_row, max_height + 16);
        main_column.end();
        window_ref.end();

        drop(desc_ref);
        drop(content_ref);
        drop(window_ref);

        scroll.resize_callback({
            let content_ref = Rc::clone(&content_column);
            move |_, _, _, w, _| {
                let (x, y, height) = if let Ok(borrowed) = content_ref.try_borrow() {
                    let x = borrowed.x();
                    let y = borrowed.y();
                    (x, y, borrowed.height())
                } else {
                    return;
                };
                if let Ok(mut borrowed) = content_ref.try_borrow_mut() {
                    borrowed.resize(x, y, w - 16, height);
                }
            }
        });

        Self {
            window,
            content: content_column,
            description,
        }
    }

    pub fn show(&mut self) {
        refresh(
            &mut self.content.borrow_mut(),
            &mut self.description.borrow_mut(),
        );
        self.window.borrow_mut().show();
    }
}

fn refresh(content: &mut Flex, description: &mut Frame) {
    content.clear();

    let all_interfaces = match net_interfaces::get_all() {
        Ok(interfaces) => interfaces,
        Err(err) => {
            description.set_label(&format!(
                "{}: {}",
                i18n::failed_to_get_net_interfaces(),
                error_message_chain(&Box::new(err))
            ));
            content.redraw();
            description.redraw();
            return;
        }
    };
    let candidates = client::get_candidate_interfaces(&all_interfaces);
    if candidates.is_empty() {
        description.set_label(i18n::no_supported_net_interface_found());
        content.redraw();
        description.redraw();
        return;
    }
    let mut rows: Vec<Flex> = Vec::new();
    let mut name_buttons: Vec<Button> = Vec::new();
    let mut max_label_width = 0;
    content.begin();
    for candidate in candidates {
        let interface_row = Flex::default().with_type(FlexType::Row);
        let mut name_button = Button::default().with_label(&candidate.name);
        name_button.set_align(Align::Clip | Align::Left | Align::Inside);
        name_button.set_tooltip(i18n::interface_name_button_tooltip());
        name_button.set_callback(|button| {
            copy(&button.label());
        });
        max_label_width = max_label_width.max(name_button.measure_label().0 + 16);
        name_buttons.push(name_button);
        for address in &candidate.addresses {
            let mut button = Button::default().with_label(&address.ip().to_string());
            button.set_align(Align::Clip | Align::Left | Align::Inside);
            button.set_callback(|button| {
                copy(&button.label());
                let mut qr_window = QrWindow::new();
                qr_window.show(button.label());
            });
            button.set_tooltip(i18n::ip_button_tooltip());
        }
        interface_row.end();
        rows.push(interface_row);
    }
    for i in 0..rows.len() {
        rows[i].set_size(&name_buttons[i], max_label_width);
        rows[i].recalc();
    }
    WidgetExt::set_size(
        content,
        0.max(content.parent().unwrap().width() - 16),
        name_buttons.len() as i32 * 38,
    );
    content.end();
    content.redraw();
    description.redraw();
}
