use fltk::{dialog::alert, enums::Mode, prelude::*, app::redraw};
use fltk_theme::{ThemeType, WidgetTheme};
use tokio::sync::{mpsc::{channel, Receiver, Sender}, broadcast};

use crate::{
    client_task,
    config::{Update, theme::Theme},
    dialog_center_window, error_message_chain, joystick_task, keyboard_task,
    message::{ClientMessage, JoystickMessage, KeyboardMessage, MouseMessage},
    mouse_task,
    ui::{main_window, AboutWindow, InterfaceWindow, MainWindow, SettingsWindow,
        check_update_window::CheckUpdateWindow}, update_check, CHANNEL_SIZE,
};

use crate::{config::AppConfig, i18n, message::Message};

pub struct MyApp {
    fltk_app: fltk::app::App,
    main_window: MainWindow,
    settings_window: SettingsWindow,
    interface_window: InterfaceWindow,
    about_window: AboutWindow,
    check_update_window: CheckUpdateWindow,
    app_rx: Receiver<Message>,
    app_tx: Sender<Message>,
    theme_tx: broadcast::Sender<Theme>,
    client_tx: Sender<ClientMessage>,
    joystick_tx: Sender<JoystickMessage>,
    keyboard_tx: Sender<KeyboardMessage>,
    mouse_tx: Sender<MouseMessage>,
    config: AppConfig,
}

impl MyApp {
    pub async fn run() -> Result<(), FltkError> {
        let config = AppConfig::load();
        let fltk_app = fltk::app::App::default();
        let widget_theme = WidgetTheme::new(ThemeType::from(&config.general.theme));
        widget_theme.apply();
        let _ = fltk_app.set_visual(Mode::MultiSample);
        let (app_tx, app_rx) = channel::<Message>(CHANNEL_SIZE);
        let (client_tx, client_rx) = channel::<ClientMessage>(CHANNEL_SIZE);
        let (mouse_tx, mouse_rx) = channel::<MouseMessage>(CHANNEL_SIZE);
        let (keyboard_tx, keyboard_rx) = channel::<KeyboardMessage>(CHANNEL_SIZE);
        let (joystick_tx, joystick_rx) = channel::<JoystickMessage>(CHANNEL_SIZE);
        let (theme_tx, _) = broadcast::channel(1);
        tokio::task::spawn(update_check::run_task(
            app_tx.clone(),
            update_check::TaskReason::AppStartup {
                config: config.clone()
            }
        ));
        tokio::task::spawn(mouse_task::run_task(app_tx.clone(), mouse_rx));
        tokio::task::spawn(keyboard_task::run_task(app_tx.clone(), keyboard_rx));
        tokio::task::spawn(joystick_task::run_task(
            config.axis,
            config.js_script.clone(),
            app_tx.clone(),
            joystick_rx,
        ));
        tokio::task::spawn(client_task::run_task(
            config.network.clone(),
            app_tx.clone(),
            joystick_tx.clone(),
            keyboard_tx.clone(),
            mouse_tx.clone(),
            client_rx,
        ));
        let mut app = Self {
            main_window: MainWindow::new(app_tx.clone(), &theme_tx),
            settings_window: SettingsWindow::new(app_tx.clone(), &config),
            interface_window: InterfaceWindow::new(),
            about_window: AboutWindow::new(),
            check_update_window: CheckUpdateWindow::new(app_tx.clone()),
            app_rx,
            app_tx,
            theme_tx,
            fltk_app,
            joystick_tx,
            client_tx,
            keyboard_tx,
            mouse_tx,
            config,
        };
        let _ = app.theme_tx.send(app.config.general.theme);
        app.main_window.window.show();
        while fltk_app.wait() {
            if !app.process_all_messages().await {
                break;
            }
        }
        Ok(())
    }

    pub async fn process_all_messages(&mut self) -> bool {
        while let Ok(message) = self.app_rx.try_recv() {
            self.process_message(message).await;
        }
        true
    }

    async fn process_message(&mut self, message: Message) {
        match message {
            Message::UpdateClientStatus(text) => {
                self.main_window.update_status(
                    &format_status(i18n::receiver_status(), text),
                    main_window::Status::Client,
                );
            }
            Message::UpdateJoystickStatus(text) => {
                self.main_window.update_status(
                    &format_status(i18n::joystick_status(), text),
                    main_window::Status::Joystick,
                );
            }
            Message::UpdateMouseStatus(text) => {
                self.main_window.update_status(
                    &format_status(i18n::mouse_status(), text),
                    main_window::Status::Mouse,
                );
            }
            Message::UpdateKeyboardStatus(text) => {
                self.main_window.update_status(
                    &format_status(i18n::keyboard_status(), text),
                    main_window::Status::Keyboard,
                );
            }
            Message::UpdateData(data) => {
                self.main_window.update_data(data);
            }
            Message::UpdateConfig(update) => {
                self.config.update_with(update.clone());
                self.update_workers_config(update).await;
                WidgetTheme::new((&self.config.general.theme).into()).apply();
                let _ = self.theme_tx.send(self.config.general.theme);
                redraw();
            }
            Message::UpdateLatestVersionInfo {
                info_result,
                show_update_window
            } => self.check_update_window.process_info_result(
                info_result,
                show_update_window
            ),
            Message::ShowInterfaces => {
                self.interface_window.show();
            }
            Message::ShowSettings => {
                self.settings_window.show();
            }
            Message::ShowAbout => {
                self.about_window.show();
            }
            Message::ShowHelp => self.show_help(),
            Message::ShowCheckUpdate => {
                tokio::task::spawn(update_check::run_task(
                    self.app_tx.clone(),
                    update_check::TaskReason::UserRequest
                ));
                self.check_update_window.show();
            },
            Message::ResetSettings => {
                self.reset_settings().await;
                WidgetTheme::new((&self.config.general.theme).into()).apply();
                let _ = self.theme_tx.send(self.config.general.theme);
                redraw();
            },
            Message::Exit => {
                let _ = self.joystick_tx.try_send(JoystickMessage::Exit);
                let _ = self.client_tx.try_send(ClientMessage::Exit);
                let _ = self.keyboard_tx.try_send(KeyboardMessage::Exit);
                let _ = self.mouse_tx.try_send(MouseMessage::Exit);
                self.config.save();
                self.fltk_app.quit();
            }
        }
    }

    async fn reset_settings(&mut self) {
        self.config = AppConfig {
            hidden: self.config.hidden.clone(),
            ..Default::default()
        };
        self.settings_window.reset_widgets(&self.config);
        let _ = self
            .client_tx
            .send(ClientMessage::UpdateConfig(Update::Full(
                self.config.clone(),
            )))
            .await;
        let _ = self
            .joystick_tx
            .send(JoystickMessage::UpdateConfig(Update::Full(
                self.config.clone(),
            )))
            .await;
    }

    fn show_help(&self) {
        let path = get_help_url();
        if let Err(err) = open::that(path) {
            let center = dialog_center_window(&self.main_window.window);
            alert(center.0, center.1, &error_message_chain(&err));
        }
    }

    async fn update_workers_config(&self, update: Update) {
        match update {
            Update::Full(_) => {
                let _ = self
                    .client_tx
                    .send(ClientMessage::UpdateConfig(update.clone()))
                    .await;
                let _ = self
                    .joystick_tx
                    .send(JoystickMessage::UpdateConfig(update))
                    .await;
            }
            Update::Network(_) => {
                let _ = self
                    .client_tx
                    .send(ClientMessage::UpdateConfig(update))
                    .await;
            }
            Update::Axis(_) | Update::JoystickScript(_) => {
                let _ = self
                    .joystick_tx
                    .send(JoystickMessage::UpdateConfig(update))
                    .await;
            }
            _ => ()
        }
    }
}

fn get_help_url() -> &'static str {
    "https://gwheel.pages.dev/help"
}

fn format_status(status_name: &str, detail: String) -> String {
    format!("{}: {}", status_name, detail)
}
