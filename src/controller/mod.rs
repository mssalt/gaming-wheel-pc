#[cfg(target_os = "linux")]
mod linux;
#[cfg(target_os = "windows")]
mod windows;

use std::path::PathBuf;

#[cfg(target_os = "linux")]
pub use linux::JoystickController;
#[cfg(target_os = "linux")]
pub use linux::KeyboardController;
#[cfg(target_os = "linux")]
pub use linux::MouseController;
use mlua::prelude::*;

use crate::error::CreateController;

#[cfg(target_os = "windows")]
pub use self::windows::vjoy_interface;
#[cfg(target_os = "windows")]
pub use self::windows::JoystickController;
#[cfg(target_os = "windows")]
pub use self::windows::KeyboardController;
#[cfg(target_os = "windows")]
pub use self::windows::MouseController;

/// Adds a function to the 'package.searchers' global Lua variable,
/// which finds and loads scripts from the script folder of the app.
pub fn add_searcher_fn(vm: &Lua, script_data_path: &PathBuf) -> Result<(), CreateController> {
    // The joystick scripts need to include the 'gaming_wheel' script to work. We would normally
    // add the script path to the 'package.path' global variable but a Lua string may not be
    // encoded in UTF-8. Instead of adding a path, we add our own searcher function to
    // 'package.searchers' table, which joins the script directory with the input term.
    let searcher_function = vm.create_function({
        let script_data_path = script_data_path.clone();
        move |vm, input_path: String| {
            let lua_appended = script_data_path
                .join(format!("{}.lua", &input_path));
            let script_path = lua_appended.exists()
                .then_some(lua_appended)
                .or_else(|| {
                    let without_lua = script_data_path.join(&input_path);
                    without_lua.exists().then_some(without_lua)
                });
            if let Some(script_path) = script_path {
                let mut multi_value = LuaMultiValue::new();
                let loader = vm.create_function::<(String, String), _, _>(move |vm, (_, data)| {
                    let header = std::fs::read_to_string(&data)?;
                    let header_table: LuaValue = vm.load(&header).eval()?;
                    return Ok(header_table);
                })?;
                let script_path_str = script_path.into_os_string().into_string().unwrap();
                let loader_data = vm.create_string(&script_path_str)?;
                multi_value.push_front(LuaValue::String(loader_data));
                multi_value.push_front(LuaValue::Function(loader));
                Ok(multi_value)
            } else {
                let mut multi_value = LuaMultiValue::new();
                let not_found_msg = format!(
                    "Gaming wheel PC couldn't find file '{}' in the scripts folder.",
                    input_path
                );
                let msg = vm.create_string(&not_found_msg)?;
                multi_value.push_front(LuaValue::String(msg));
                Ok(multi_value)
            }
    }})?;  
    vm.globals().get::<_, LuaTable>("package")?
        .get::<_, LuaTable>("searchers")?
        .raw_insert(1, searcher_function)?;
    Ok(())
}
