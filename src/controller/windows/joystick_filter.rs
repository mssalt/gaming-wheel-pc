use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, Copy)]
#[serde(rename_all = "camelCase")]
pub struct JoystickFilter {
    pub interface: u8,
    pub button_count: u8,
    pub hat_count: u8,
    pub aileron: bool,
    pub axis_vbrx: bool,
    pub axis_vbry: bool,
    pub axis_vbrz: bool,
    pub axis_vx: bool,
    pub axis_vy: bool,
    pub axis_vz: bool,
    pub axis_x: bool,
    pub axis_xrot: bool,
    pub axis_y: bool,
    pub axis_yrot: bool,
    pub axis_z: bool,
    pub axis_zrot: bool,
    pub dial: bool,
    pub rudder: bool,
    pub slider: bool,
    pub throttle: bool,
    pub wheel: bool,
}
