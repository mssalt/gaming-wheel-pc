use windows_sys::Win32::UI::{
    Input::KeyboardAndMouse::{
        MapVirtualKeyA, SendInput, INPUT, INPUT_KEYBOARD, KEYBDINPUT, KEYEVENTF_KEYUP,
        KEYEVENTF_SCANCODE, VK_0, VK_1, VK_2, VK_3, VK_4, VK_5, VK_6, VK_7, VK_8, VK_9, VK_A,
        VK_ADD, VK_APPS, VK_B, VK_BACK, VK_C, VK_CAPITAL, VK_D, VK_DECIMAL, VK_DELETE, VK_DIVIDE,
        VK_DOWN, VK_E, VK_END, VK_ESCAPE, VK_F, VK_F1, VK_F10, VK_F11, VK_F12, VK_F2, VK_F3, VK_F4,
        VK_F5, VK_F6, VK_F7, VK_F8, VK_F9, VK_G, VK_H, VK_HOME, VK_I, VK_INSERT, VK_J, VK_K, VK_L,
        VK_LCONTROL, VK_LEFT, VK_LMENU, VK_LSHIFT, VK_LWIN, VK_M, VK_MEDIA_NEXT_TRACK,
        VK_MEDIA_PLAY_PAUSE, VK_MEDIA_PREV_TRACK, VK_MEDIA_STOP, VK_MULTIPLY, VK_N, VK_NEXT,
        VK_NUMLOCK, VK_NUMPAD0, VK_NUMPAD1, VK_NUMPAD2, VK_NUMPAD3, VK_NUMPAD4, VK_NUMPAD5,
        VK_NUMPAD6, VK_NUMPAD7, VK_NUMPAD8, VK_NUMPAD9, VK_O, VK_OEM_1, VK_OEM_2, VK_OEM_3,
        VK_OEM_4, VK_OEM_5, VK_OEM_6, VK_OEM_7, VK_OEM_COMMA, VK_OEM_MINUS, VK_OEM_PERIOD,
        VK_OEM_PLUS, VK_P, VK_PAUSE, VK_PRIOR, VK_Q, VK_R, VK_RCONTROL, VK_RETURN, VK_RIGHT,
        VK_RMENU, VK_RSHIFT, VK_RWIN, VK_S, VK_SCROLL, VK_SNAPSHOT, VK_SPACE, VK_SUBTRACT, VK_T,
        VK_TAB, VK_U, VK_UP, VK_V, VK_VOLUME_DOWN, VK_VOLUME_MUTE, VK_VOLUME_UP, VK_W, VK_X, VK_Y,
        VK_Z, MAPVK_VK_TO_VSC
    },
    WindowsAndMessaging::GetMessageExtraInfo,
};

use crate::{input_state::InputState, proto_capnp::Key};

pub struct Controller {
    thread_extra_info: usize,
    current_keys: Vec<Key>,
}

impl Controller {
    pub fn new() -> Self {
        Controller {
            thread_extra_info: unsafe { GetMessageExtraInfo() } as usize,
            current_keys: Vec::default(),
        }
    }

    pub fn update(&mut self, data: InputState) {
        let mut inputs = Vec::<INPUT>::new();
        self.current_keys = self
            .current_keys
            .clone()
            .into_iter()
            .filter(|key| {
                if !data.pressed_pc_keys.contains(key) {
                    inputs.push(self.make_input(proto_to_vk(*key), false));
                    false
                } else {
                    true
                }
            })
            .collect();
        for key in data.pressed_pc_keys {
            if !self.current_keys.contains(&key) {
                self.current_keys.push(key);
                inputs.push(self.make_input(proto_to_vk(key), true));
            }
        }

        unsafe {
            SendInput(
                inputs.len() as u32,
                inputs.as_ptr(),
                std::mem::size_of::<INPUT>() as i32,
            );
        }
    }

    fn make_input(&self, keycode: u16, pressing: bool) -> INPUT {
        INPUT {
            r#type: INPUT_KEYBOARD,
            Anonymous: windows_sys::Win32::UI::Input::KeyboardAndMouse::INPUT_0 {
                ki: KEYBDINPUT {
                    wVk: 0,
                    wScan: keycode,
                    time: 0,
                    dwFlags: KEYEVENTF_SCANCODE | if pressing { 0 } else { KEYEVENTF_KEYUP },
                    dwExtraInfo: self.thread_extra_info,
                },
            },
        }
    }
}

fn proto_to_vk(key: Key) -> u16 {
    let vk_key = match key {
        Key::Esc => VK_ESCAPE,
        Key::Grave => VK_OEM_3,
        Key::Tab => VK_TAB,
        Key::CapsLock => VK_CAPITAL,
        Key::LeftShift => VK_LSHIFT,
        Key::LeftCtrl => VK_LCONTROL,
        Key::LeftMeta => VK_LWIN,
        Key::RightMeta => VK_RWIN,
        Key::Menu => VK_APPS,
        Key::RightCtrl => VK_RCONTROL,
        Key::LeftAlt => VK_LMENU,
        Key::AltGr => VK_RMENU,
        Key::Space => VK_SPACE,
        Key::Z => VK_Z,
        Key::X => VK_X,
        Key::C => VK_C,
        Key::V => VK_V,
        Key::B => VK_B,
        Key::N => VK_N,
        Key::M => VK_M,
        Key::Comma => VK_OEM_COMMA,
        Key::Dot => VK_OEM_PERIOD,
        Key::Slash => VK_OEM_2,
        Key::RightShift => VK_RSHIFT,
        Key::A => VK_A,
        Key::S => VK_S,
        Key::D => VK_D,
        Key::F => VK_F,
        Key::G => VK_G,
        Key::H => VK_H,
        Key::J => VK_J,
        Key::K => VK_K,
        Key::L => VK_L,
        Key::Semicolon => VK_OEM_1,
        Key::Apostrophe => VK_OEM_7,
        Key::Q => VK_Q,
        Key::W => VK_W,
        Key::E => VK_E,
        Key::R => VK_R,
        Key::T => VK_T,
        Key::Y => VK_Y,
        Key::U => VK_U,
        Key::I => VK_I,
        Key::O => VK_O,
        Key::P => VK_P,
        Key::LeftBracket => VK_OEM_4,
        Key::RightBracket => VK_OEM_6,
        Key::Enter => VK_RETURN,
        Key::F1 => VK_F1,
        Key::F2 => VK_F2,
        Key::F3 => VK_F3,
        Key::F4 => VK_F4,
        Key::F5 => VK_F5,
        Key::F6 => VK_F6,
        Key::F7 => VK_F7,
        Key::F8 => VK_F8,
        Key::F9 => VK_F9,
        Key::F10 => VK_F10,
        Key::F11 => VK_F11,
        Key::F12 => VK_F12,
        Key::PrintScreen => VK_SNAPSHOT,
        Key::ScrollLock => VK_SCROLL,
        Key::PauseBreak => VK_PAUSE,
        Key::Play => VK_MEDIA_PLAY_PAUSE,
        Key::Previous => VK_MEDIA_PREV_TRACK,
        Key::Next => VK_MEDIA_NEXT_TRACK,
        Key::VolumeUp => VK_VOLUME_UP,
        Key::VolumeDown => VK_VOLUME_DOWN,
        Key::Mute => VK_VOLUME_MUTE,
        Key::Stop => VK_MEDIA_STOP,
        Key::D1 => VK_1,
        Key::D2 => VK_2,
        Key::D3 => VK_3,
        Key::D4 => VK_4,
        Key::D5 => VK_5,
        Key::D6 => VK_6,
        Key::D7 => VK_7,
        Key::D8 => VK_8,
        Key::D9 => VK_9,
        Key::D0 => VK_0,
        Key::Hyphen => VK_OEM_MINUS,
        Key::Equals => VK_OEM_PLUS,
        Key::Backslash => VK_OEM_5,
        Key::Back => VK_BACK,
        Key::Insert => VK_INSERT,
        Key::Delete => VK_DELETE,
        Key::End => VK_END,
        Key::Up => VK_UP,
        Key::Down => VK_DOWN,
        Key::Right => VK_RIGHT,
        Key::Left => VK_LEFT,
        Key::PageDown => VK_NEXT,
        Key::Home => VK_HOME,
        Key::PageUp => VK_PRIOR,
        Key::NumLock => VK_NUMLOCK,
        Key::Num7 => VK_NUMPAD7,
        Key::Num4 => VK_NUMPAD4,
        Key::Num1 => VK_NUMPAD1,
        Key::Num0 => VK_NUMPAD0,
        Key::Num2 => VK_NUMPAD2,
        Key::Num3 => VK_NUMPAD3,
        Key::NumDel => VK_DECIMAL,
        Key::Num5 => VK_NUMPAD5,
        Key::Num6 => VK_NUMPAD6,
        Key::Num8 => VK_NUMPAD8,
        Key::Num9 => VK_NUMPAD9,
        Key::Divide => VK_DIVIDE,
        Key::Multiply => VK_MULTIPLY,
        Key::Subtract => VK_SUBTRACT,
        Key::Add => VK_ADD,
        Key::NumEnter => VK_RETURN,
    };
    unsafe { MapVirtualKeyA(vk_key as u32, MAPVK_VK_TO_VSC) as u16 }
}
