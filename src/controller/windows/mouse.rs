use windows_sys::Win32::UI::{
    Input::KeyboardAndMouse::{
        SendInput, INPUT, INPUT_MOUSE, MOUSEEVENTF_LEFTDOWN, MOUSEEVENTF_LEFTUP,
        MOUSEEVENTF_MIDDLEDOWN, MOUSEEVENTF_MIDDLEUP, MOUSEEVENTF_MOVE, MOUSEEVENTF_RIGHTDOWN,
        MOUSEEVENTF_RIGHTUP, MOUSEEVENTF_WHEEL, MOUSEINPUT,
    },
    WindowsAndMessaging::{GetMessageExtraInfo, WHEEL_DELTA},
};

use crate::input_state::InputState;

pub struct Controller {
    thread_extra_info: usize,
    left_button: bool,
    right_button: bool,
    middle_button: bool,
    dx: i16,
    dy: i16,
    scroll: i16,
}

impl Controller {
    pub fn new() -> Self {
        Controller {
            thread_extra_info: unsafe { GetMessageExtraInfo() } as usize,
            left_button: false,
            right_button: false,
            middle_button: false,
            dx: 0,
            dy: 0,
            scroll: 0,
        }
    }

    pub fn update(&mut self, data: InputState) {
        let left = if data.left_mouse_button != self.left_button {
            self.left_button = data.left_mouse_button;
            Some(data.left_mouse_button)
        } else {
            None
        };

        let right = if data.right_mouse_button != self.right_button {
            self.right_button = data.right_mouse_button;
            Some(data.right_mouse_button)
        } else {
            None
        };

        let middle = if data.middle_mouse_button != self.middle_button {
            self.middle_button = data.middle_mouse_button;
            Some(data.middle_mouse_button)
        } else {
            None
        };

        let scroll = if data.delta_mouse_scroll != self.scroll {
            self.scroll = data.delta_mouse_scroll;
            Some(data.delta_mouse_scroll)
        } else {
            None
        };

        let rel_move = if self.dx != data.delta_mouse_x || self.dy != data.delta_mouse_y {
            self.dx = data.delta_mouse_x;
            self.dy = data.delta_mouse_y;
            Some((data.delta_mouse_x, data.delta_mouse_y))
        } else {
            None
        };

        let input = self.make_input(left, right, middle, scroll, rel_move);
        unsafe {
            SendInput(1, [input].as_ptr(), std::mem::size_of::<INPUT>() as i32);
        }
    }

    fn make_input(
        &self,
        left: Option<bool>,
        right: Option<bool>,
        middle: Option<bool>,
        scroll: Option<i16>,
        rel_move: Option<(i16, i16)>,
    ) -> INPUT {
        let mut flags = 0;
        if let Some(left) = left {
            if left {
                flags |= MOUSEEVENTF_LEFTDOWN
            } else {
                flags |= MOUSEEVENTF_LEFTUP
            }
        }

        if let Some(right) = right {
            if right {
                flags |= MOUSEEVENTF_RIGHTDOWN
            } else {
                flags |= MOUSEEVENTF_RIGHTUP
            }
        }

        if let Some(middle) = middle {
            if middle {
                flags |= MOUSEEVENTF_MIDDLEDOWN
            } else {
                flags |= MOUSEEVENTF_MIDDLEUP
            }
        }

        let data = if let Some(scroll) = scroll {
            flags |= MOUSEEVENTF_WHEEL;
            scroll as i32 * WHEEL_DELTA as i32
        } else {
            0
        };

        let (dx, dy) = if let Some((dx, dy)) = rel_move {
            flags |= MOUSEEVENTF_MOVE;
            (dx, dy)
        } else {
            (0, 0)
        };

        INPUT {
            r#type: INPUT_MOUSE,
            Anonymous: windows_sys::Win32::UI::Input::KeyboardAndMouse::INPUT_0 {
                mi: MOUSEINPUT {
                    dx: dx as i32,
                    dy: dy as i32,
                    time: 0,
                    dwFlags: flags,
                    mouseData: data,
                    dwExtraInfo: self.thread_extra_info,
                },
            },
        }
    }
}
