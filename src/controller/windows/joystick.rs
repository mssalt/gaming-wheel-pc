use std::fs;

use libc::c_void;
use mlua::{prelude::*, Lua};

use crate::{
    config::js_script::JoystickScriptConfig,
    controller::{
        windows::vjoy_interface::{vJoyEnabled, DriverMatch, GetVJDStatus},
        add_searcher_fn
    },
    error::{CreateController, SelectJoystick, UpdateJoystick},
    input_state::InputState,
    DATA_DIR,
};

use super::{
    joystick_filter::JoystickFilter,
    vjoy_interface::{
        isVJDExists, AcquireVJD, GetVJDAxisExist, GetVJDButtonNumber, GetVJDContPovNumber,
        RelinquishVJD, UpdateVJD, VjdStat_VJD_STAT_FREE, HID_USAGE_RX, HID_USAGE_RY, HID_USAGE_RZ,
        HID_USAGE_SL0, HID_USAGE_SL1, HID_USAGE_WHL, HID_USAGE_X, HID_USAGE_Y, HID_USAGE_Z,
        JOYSTICK_POSITION_V2,
    },
};

struct ScriptStorage {
    vm: Lua,
}

pub struct Controller {
    interface_number: u32,
    current_state: InputState,
    current_report: JOYSTICK_POSITION_V2,
    script_storage: ScriptStorage,
}

impl Controller {
    pub fn new(script_config: &JoystickScriptConfig) -> Result<Self, CreateController> {
        if unsafe { !vJoyEnabled() } {
            return Err(CreateController::VjoyDisabled);
        }

        let mut dll_version = 0;
        let mut driver_version = 0;
        if unsafe { !DriverMatch(&mut dll_version, &mut driver_version) } {
            return Err(CreateController::VersionMismatch {
                dll_version,
                driver_version,
            });
        }

        let vm = Lua::new();
        let script_data_path = DATA_DIR.get().unwrap().join("script").join("windows");
        add_searcher_fn(&vm, &script_data_path)?;
        let path = if script_config.enabled && script_config.path.is_some() {
            script_config.path.as_ref().unwrap().clone()
        } else {
            script_data_path.join("default.lua")
        };
        let script_code = fs::read_to_string(path)?;
        let user_script = vm.load(&script_code);
        user_script.exec()?;
        let mut filter: JoystickFilter = {
            let setup_func: mlua::Function = vm.globals().get("Setup")?;
            vm.from_value(LuaValue::Table(setup_func.call::<_, LuaTable>(())?))?
        };
        let script_storage = ScriptStorage { vm };

        if filter.interface == 0 {
            let mut temp_filter = filter;
            let mut first_result = None::<Result<(), SelectJoystick>>;
            for i in 1..=15_u8 {
                temp_filter.interface = i;
                if first_result.is_none() {
                    first_result = Some(select_joystick(&temp_filter));
                }
                if let Some(Ok(())) = first_result {
                    filter.interface = i;
                    break;
                }
            }
            if filter.interface == 0 {
                if let Some(Err(err)) = first_result {
                    return Err(CreateController::SelectJoystick { source: err });
                }
            }
        } else {
            select_joystick(&filter)?;
        }

        if unsafe { !AcquireVJD(filter.interface as _) } {
            return Err(CreateController::AcquireVjoy {
                interface_number: filter.interface as _,
            });
        }

        Ok(Controller {
            current_state: InputState::default(),
            interface_number: filter.interface as _,
            current_report: JOYSTICK_POSITION_V2 {
                bDevice: filter.interface as i8,
                bHats: 0,
                bHatsEx1: 0,
                bHatsEx2: 0,
                bHatsEx3: 0,
                lButtons: 0,
                lButtonsEx1: 0,
                lButtonsEx2: 0,
                lButtonsEx3: 0,
                wAileron: 0,
                wAxisVBRX: 0,
                wAxisVBRY: 0,
                wAxisVBRZ: 0,
                wAxisVX: 0,
                wAxisVY: 0,
                wAxisVZ: 0,
                wAxisX: 0,
                wAxisXRot: 0,
                wAxisY: 0,
                wAxisYRot: 0,
                wAxisZ: 0,
                wAxisZRot: 0,
                wDial: 0,
                wRudder: 0,
                wSlider: 0,
                wThrottle: 0,
                wWheel: 0,
            },
            script_storage,
        })
    }

    pub fn update(&mut self, data: InputState) -> Result<(), UpdateJoystick> {
        let vm = &self.script_storage.vm;
        let update_func: mlua::Function = vm.globals().get("Update")?;
        let output: LuaTable = update_func.call(data.clone())?;
        for pair in output.pairs::<String, LuaValue>() {
            let (k, v) = pair?;
            match (k.as_str(), v) {
                ("hat", LuaValue::Table(t)) => {
                    for pair in t.pairs::<u8, i32>() {
                        let (i, v) = pair?;
                        match i {
                            0 => self.current_report.bHats = v,
                            1 => self.current_report.bHatsEx1 = v,
                            2 => self.current_report.bHatsEx2 = v,
                            3 => self.current_report.bHatsEx3 = v,
                            x => {
                                return Err(UpdateJoystick::InvalidHatIndex { index: x });
                            }
                        }
                    }
                }
                ("button", LuaValue::Table(t)) => {
                    for pair in t.pairs::<u8, bool>() {
                        let (i, v) = pair?;
                        let button_set = match i {
                            0..=31 => &mut self.current_report.lButtons,
                            32..=63 => &mut self.current_report.lButtonsEx1,
                            64..=95 => &mut self.current_report.lButtonsEx2,
                            96..=127 => &mut self.current_report.lButtonsEx3,
                            x => {
                                return Err(UpdateJoystick::InvalidButtonIndex { index: x });
                            }
                        };
                        let bit = if v { 1 } else { 0 };
                        change_bit(button_set, i, bit);
                    }
                }
                ("aileron", LuaValue::Integer(i)) => {
                    self.current_report.wAileron = i.try_into()?;
                }
                ("axisVBRX", LuaValue::Integer(i)) => {
                    self.current_report.wAxisVBRX = i.try_into()?;
                }
                ("axisVBRY", LuaValue::Integer(i)) => {
                    self.current_report.wAxisVBRY = i.try_into()?;
                }
                ("axisVBRZ", LuaValue::Integer(i)) => {
                    self.current_report.wAxisVBRZ = i.try_into()?;
                }
                ("axisVX", LuaValue::Integer(i)) => {
                    self.current_report.wAxisVX = i.try_into()?;
                }
                ("axisVY", LuaValue::Integer(i)) => {
                    self.current_report.wAxisVY = i.try_into()?;
                }
                ("axisVZ", LuaValue::Integer(i)) => {
                    self.current_report.wAxisVZ = i.try_into()?;
                }
                ("axisX", LuaValue::Integer(i)) => {
                    self.current_report.wAxisX = i.try_into()?;
                }
                ("axisXRot", LuaValue::Integer(i)) => {
                    self.current_report.wAxisXRot = i.try_into()?;
                }
                ("axisY", LuaValue::Integer(i)) => {
                    self.current_report.wAxisY = i.try_into()?;
                }
                ("axisYRot", LuaValue::Integer(i)) => {
                    self.current_report.wAxisYRot = i.try_into()?;
                }
                ("axisZ", LuaValue::Integer(i)) => {
                    self.current_report.wAxisZ = i.try_into()?;
                }
                ("axisZRot", LuaValue::Integer(i)) => {
                    self.current_report.wAxisZRot = i.try_into()?;
                }
                ("dial", LuaValue::Integer(i)) => {
                    self.current_report.wDial = i.try_into()?;
                }
                ("rudder", LuaValue::Integer(i)) => {
                    self.current_report.wRudder = i.try_into()?;
                }
                ("slider", LuaValue::Integer(i)) => {
                    self.current_report.wSlider = i.try_into()?;
                }
                ("throttle", LuaValue::Integer(i)) => {
                    self.current_report.wThrottle = i.try_into()?;
                }
                ("wheel", LuaValue::Integer(i)) => {
                    self.current_report.wWheel = i.try_into()?;
                }
                (key, _) => {
                    return Err(UpdateJoystick::InvalidField {
                        field: key.to_owned(),
                    })
                }
            }
        }
        unsafe {
            UpdateVJD(
                self.interface_number,
                (&mut self.current_report) as *mut JOYSTICK_POSITION_V2 as *mut c_void,
            )
        };
        self.current_state = data;
        Ok(())
    }
}

fn change_bit(value: &mut i32, index: u8, new_bit: u8) {
    *value = (*value & !(1 << index)) | ((new_bit as i32) << index);
}

impl Drop for Controller {
    fn drop(&mut self) {
        unsafe {
            RelinquishVJD(self.interface_number);
        }
    }
}

fn select_joystick(filter: &JoystickFilter) -> Result<(), SelectJoystick> {
    let interface = filter.interface as u32;
    if unsafe { !isVJDExists(interface) } {
        return Err(SelectJoystick::NotExists {
            interface_number: interface,
        });
    }
    let status = unsafe { GetVJDStatus(interface) };
    if status != VjdStat_VJD_STAT_FREE {
        return Err(SelectJoystick::NotFree {
            interface_number: interface,
            status_code: status,
        });
    }
    let button_count = unsafe { GetVJDButtonNumber(interface) };
    if unsafe { GetVJDButtonNumber(interface) } < filter.button_count as i32 {
        return Err(SelectJoystick::NotEnoughButtons {
            interface_number: interface,
            enabled_count: button_count,
            required_count: filter.button_count as _,
        });
    }
    let hat_count = unsafe { GetVJDContPovNumber(interface) };
    if hat_count < filter.hat_count as i32 {
        return Err(SelectJoystick::NotEnoughHats {
            interface_number: interface,
            enabled_count: hat_count,
            required_count: filter.hat_count as _,
        });
    }
    if filter.axis_x && unsafe { !GetVJDAxisExist(interface, HID_USAGE_X) } {
        return Err(SelectJoystick::DisabledAxis {
            interface_number: interface,
            axis: "X",
        });
    }
    if filter.axis_y && unsafe { !GetVJDAxisExist(interface, HID_USAGE_Y) } {
        return Err(SelectJoystick::DisabledAxis {
            interface_number: interface,
            axis: "Y",
        });
    }
    if filter.axis_z && unsafe { !GetVJDAxisExist(interface, HID_USAGE_Z) } {
        return Err(SelectJoystick::DisabledAxis {
            interface_number: interface,
            axis: "Z",
        });
    }
    if filter.axis_xrot && unsafe { !GetVJDAxisExist(interface, HID_USAGE_RX) } {
        return Err(SelectJoystick::DisabledAxis {
            interface_number: interface,
            axis: "RX",
        });
    }
    if filter.axis_yrot && unsafe { !GetVJDAxisExist(interface, HID_USAGE_RY) } {
        return Err(SelectJoystick::DisabledAxis {
            interface_number: interface,
            axis: "RX",
        });
    }
    if filter.axis_zrot && unsafe { !GetVJDAxisExist(interface, HID_USAGE_RZ) } {
        return Err(SelectJoystick::DisabledAxis {
            interface_number: interface,
            axis: "RZ",
        });
    }
    if filter.slider && unsafe { !GetVJDAxisExist(interface, HID_USAGE_SL0) } {
        return Err(SelectJoystick::DisabledAxis {
            interface_number: interface,
            axis: "Slider",
        });
    }
    if filter.dial && unsafe { !GetVJDAxisExist(interface, HID_USAGE_SL1) } {
        return Err(SelectJoystick::DisabledAxis {
            interface_number: interface,
            axis: "Dial/Slider2",
        });
    }
    if filter.wheel && unsafe { !GetVJDAxisExist(interface, HID_USAGE_WHL) } {
        return Err(SelectJoystick::DisabledAxis {
            interface_number: interface,
            axis: "Wheel",
        });
    }
    return Ok(());
}
