mod joystick;
mod joystick_info;
mod keyboard;
mod mouse;

use std::time::{SystemTime, UNIX_EPOCH};

use input_linux_sys::{input_event, timeval};
pub use joystick::Controller as JoystickController;
pub use keyboard::Controller as KeyboardController;
pub use mouse::Controller as MouseController;

fn write_event<T: Into<i32>>(descriptor: libc::c_int, event_type: i32, code: i32, value: T) {
    let now = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
    let event = input_event {
        code: code as _,
        time: timeval {
            tv_sec: now.as_secs() as _,
            tv_usec: now.subsec_micros() as _,
        },
        type_: event_type as _,
        value: value.into(),
    };

    let _ = unsafe {
        libc::write(
            descriptor,
            &event as *const input_event as _,
            std::mem::size_of::<input_event>(),
        )
    };
}
