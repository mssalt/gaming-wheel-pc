use std::{collections::HashMap, ffi::CStr, fs};

use input_linux_sys::{
    input_id, ui_dev_create, ui_dev_destroy, ui_set_absbit, ui_set_evbit, ui_set_keybit,
    ui_set_relbit, uinput_user_dev, ABS_CNT, EV_ABS, EV_KEY, EV_REL, EV_SYN, SYN_REPORT,
    UINPUT_MAX_NAME_SIZE,
};
use libc::strerror;
use mlua::{prelude::*, Lua, LuaSerdeExt};

use crate::{
    config::js_script::JoystickScriptConfig, error::CreateController, input_state::InputState,
    DATA_DIR, controller::add_searcher_fn,
};

use super::{joystick_info::JoystickInfo, write_event};

struct ScriptStorage {
    vm: Lua,
}

pub struct Controller {
    descriptor: i32,
    current_state: InputState,
    script_storage: ScriptStorage,
}

impl Controller {
    pub fn new(script_config: &JoystickScriptConfig) -> Result<Self, CreateController> {
        let vm = Lua::new();
        let script_data_path = DATA_DIR.get().unwrap().join("script").join("linux");
        add_searcher_fn(&vm, &script_data_path)?;
        let path = if script_config.enabled && script_config.path.is_some() {
            script_config.path.as_ref().unwrap().clone()
        } else {
            script_data_path.join("default.lua")
        };
        let script_code = fs::read_to_string(path)?;
        let user_script = vm.load(&script_code);
        user_script.exec()?;
        let info: JoystickInfo = {
            let setup_func: mlua::Function = vm.globals().get("Setup")?;
            vm.from_value(LuaValue::Table(setup_func.call::<_, LuaTable>(())?))?
        };
        let script_storage = ScriptStorage { vm };
        let name_bytes = info.name.as_bytes();
        let name_array = [0i8; UINPUT_MAX_NAME_SIZE as usize];
        let length = (name_array.len() - 1).min(name_bytes.len());
        unsafe {
            libc::memcpy(name_array.as_ptr() as _, name_bytes.as_ptr() as _, length);
        }
        let mut device = uinput_user_dev {
            name: name_array,
            id: input_id {
                bustype: info.bus_type,
                vendor: info.vendor_id,
                product: info.product_id,
                version: info.version,
            },
            ff_effects_max: 0,
            absmin: [0i32; ABS_CNT as usize],
            absmax: [0i32; ABS_CNT as usize],
            absflat: [0i32; ABS_CNT as usize],
            absfuzz: [0i32; ABS_CNT as usize],
        };
        let descriptor = unsafe {
            libc::open(
                "/dev/uinput\0".as_ptr().cast(),
                libc::O_WRONLY | libc::O_NONBLOCK,
            )
        };
        if descriptor == -1 {
            let error_code = unsafe { *libc::__errno_location() };
            return Err(CreateController::OpenUinput {
                error_code,
                error_text: unsafe {
                    CStr::from_ptr(strerror(error_code))
                        .to_str()
                        .unwrap()
                        .to_string()
                },
            });
        }
        if let Err(err) = create_device(&mut device, &info, descriptor) {
            let _ = unsafe { libc::close(descriptor) };
            return Err(err);
        }
        Ok(Self {
            descriptor,
            current_state: InputState::default(),
            script_storage,
        })
    }

    pub fn update(&mut self, state: InputState) -> Result<(), LuaError> {
        let vm = &self.script_storage.vm;
        let update_func: mlua::Function = vm.globals().get("Update")?;
        let output: LuaTable = update_func.call(state.clone())?;
        if let Some(table) = output.get::<_, Option<HashMap<i16, i16>>>("abs")? {
            for (k, v) in table {
                write_event(self.descriptor, EV_ABS, k as _, v);
            }
        }
        if let Some(table) = output.get::<_, Option<HashMap<i16, i16>>>("rel")? {
            for (k, v) in table {
                write_event(self.descriptor, EV_REL, k as _, v);
            }
        }
        if let Some(table) = output.get::<_, Option<HashMap<i16, bool>>>("btn")? {
            for (k, v) in table {
                write_event(self.descriptor, EV_KEY, k as _, v);
            }
        }
        if let Some(table) = output.get::<_, Option<HashMap<i16, bool>>>("key")? {
            for (k, v) in table {
                write_event(self.descriptor, EV_KEY, k as _, v);
            }
        }
        write_event(self.descriptor, EV_SYN, SYN_REPORT, 1);
        self.current_state = state;
        Ok(())
    }
}

fn create_device(
    device: &mut uinput_user_dev,
    info: &JoystickInfo,
    descriptor: i32,
) -> Result<(), CreateController> {
    if let Some(ref abs_vector) = info.abs {
        unsafe {
            ui_set_evbit(descriptor, EV_ABS as _)?;
        }
        for abs in abs_vector {
            unsafe { ui_set_absbit(descriptor, *abs as _) }?;
            device.absmin[*abs as usize] = i16::MIN as i32;
            device.absmax[*abs as usize] = i16::MAX as i32;
        }
    }
    if let Some(ref rel_vector) = info.rel {
        unsafe {
            ui_set_evbit(descriptor, EV_REL as _)?;
        }
        for rel in rel_vector {
            unsafe { ui_set_relbit(descriptor, *rel as _) }?;
        }
    }
    if info.key.is_some() || info.btn.is_some() {
        unsafe {
            ui_set_evbit(descriptor, EV_KEY as _)?;
        }
    }
    if let Some(ref key_vector) = info.key {
        for key in key_vector {
            unsafe { ui_set_keybit(descriptor, *key as _) }?;
        }
    }
    if let Some(ref btn_vector) = info.btn {
        for btn in btn_vector {
            unsafe { ui_set_keybit(descriptor, *btn as _) }?;
        }
    }
    if unsafe {
        libc::write(
            descriptor,
            device as *const uinput_user_dev as _,
            std::mem::size_of::<uinput_user_dev>(),
        )
    } == -1
    {
        let error_code = unsafe { *libc::__errno_location() };
        return Err(CreateController::WriteUinput {
            error_code,
            error_text: unsafe {
                CStr::from_ptr(strerror(error_code))
                    .to_str()
                    .unwrap()
                    .to_string()
            },
        });
    }
    unsafe {
        ui_dev_create(descriptor)?;
    }
    Ok(())
}

impl Drop for Controller {
    fn drop(&mut self) {
        unsafe {
            let _ = ui_dev_destroy(self.descriptor);
            libc::close(self.descriptor);
        }
    }
}
