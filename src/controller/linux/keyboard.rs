use std::ffi::CStr;

use crate::error::CreateController;
use crate::input_state::InputState;
use crate::proto_capnp::Key;
use input_linux_sys::{
    input_id, ui_dev_create, ui_dev_destroy, ui_set_evbit, ui_set_keybit, uinput_user_dev, ABS_CNT,
    BUS_VIRTUAL, EV_KEY, EV_SYN, KEY_0, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8,
    KEY_9, KEY_A, KEY_APOSTROPHE, KEY_B, KEY_BACKSLASH, KEY_BACKSPACE, KEY_C, KEY_CAPSLOCK,
    KEY_COMMA, KEY_D, KEY_DELETE, KEY_DOT, KEY_DOWN, KEY_E, KEY_END, KEY_ENTER, KEY_EQUAL, KEY_ESC,
    KEY_F, KEY_F1, KEY_F10, KEY_F11, KEY_F12, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7,
    KEY_F8, KEY_F9, KEY_G, KEY_GRAVE, KEY_H, KEY_HOME, KEY_I, KEY_INSERT, KEY_J, KEY_K, KEY_KP0,
    KEY_KP1, KEY_KP2, KEY_KP3, KEY_KP4, KEY_KP5, KEY_KP6, KEY_KP7, KEY_KP8, KEY_KP9,
    KEY_KPASTERISK, KEY_KPDOT, KEY_KPENTER, KEY_KPMINUS, KEY_KPPLUS, KEY_KPSLASH, KEY_L, KEY_LEFT,
    KEY_LEFTALT, KEY_LEFTBRACE, KEY_LEFTCTRL, KEY_LEFTMETA, KEY_LEFTSHIFT, KEY_M, KEY_MENU,
    KEY_MINUS, KEY_MUTE, KEY_N, KEY_NEXTSONG, KEY_NUMLOCK, KEY_O, KEY_P, KEY_PAGEDOWN, KEY_PAGEUP,
    KEY_PAUSE, KEY_PLAYPAUSE, KEY_PREVIOUSSONG, KEY_PRINT, KEY_Q, KEY_R, KEY_RIGHT, KEY_RIGHTALT,
    KEY_RIGHTBRACE, KEY_RIGHTCTRL, KEY_RIGHTMETA, KEY_RIGHTSHIFT, KEY_S, KEY_SCROLLLOCK,
    KEY_SEMICOLON, KEY_SLASH, KEY_SPACE, KEY_STOP, KEY_T, KEY_TAB, KEY_U, KEY_UP, KEY_V,
    KEY_VOLUMEDOWN, KEY_VOLUMEUP, KEY_W, KEY_X, KEY_Y, KEY_Z, SYN_REPORT, UINPUT_MAX_NAME_SIZE,
};
use libc::strerror;

use super::write_event;

const KEYS: [libc::c_int; 111] = [
    KEY_ESC,
    KEY_GRAVE,
    KEY_TAB,
    KEY_CAPSLOCK,
    KEY_LEFTSHIFT,
    KEY_LEFTCTRL,
    KEY_LEFTMETA,
    KEY_RIGHTMETA,
    KEY_MENU,
    KEY_RIGHTCTRL,
    KEY_LEFTALT,
    KEY_RIGHTALT,
    KEY_SPACE,
    KEY_Z,
    KEY_X,
    KEY_C,
    KEY_V,
    KEY_B,
    KEY_N,
    KEY_M,
    KEY_COMMA,
    KEY_DOT,
    KEY_SLASH,
    KEY_RIGHTSHIFT,
    KEY_A,
    KEY_S,
    KEY_D,
    KEY_F,
    KEY_G,
    KEY_H,
    KEY_J,
    KEY_K,
    KEY_L,
    KEY_SEMICOLON,
    KEY_APOSTROPHE,
    KEY_Q,
    KEY_W,
    KEY_E,
    KEY_R,
    KEY_T,
    KEY_Y,
    KEY_U,
    KEY_I,
    KEY_O,
    KEY_P,
    KEY_LEFTBRACE,
    KEY_RIGHTBRACE,
    KEY_ENTER,
    KEY_F1,
    KEY_F2,
    KEY_F3,
    KEY_F4,
    KEY_F5,
    KEY_F6,
    KEY_F7,
    KEY_F8,
    KEY_F9,
    KEY_F10,
    KEY_F11,
    KEY_F12,
    KEY_PRINT,
    KEY_SCROLLLOCK,
    KEY_PAUSE,
    KEY_PLAYPAUSE,
    KEY_PREVIOUSSONG,
    KEY_NEXTSONG,
    KEY_VOLUMEUP,
    KEY_VOLUMEDOWN,
    KEY_MUTE,
    KEY_STOP,
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,
    KEY_0,
    KEY_MINUS,
    KEY_EQUAL,
    KEY_BACKSLASH,
    KEY_BACKSPACE,
    KEY_INSERT,
    KEY_DELETE,
    KEY_END,
    KEY_UP,
    KEY_DOWN,
    KEY_RIGHT,
    KEY_LEFT,
    KEY_PAGEDOWN,
    KEY_HOME,
    KEY_PAGEUP,
    KEY_NUMLOCK,
    KEY_KP7,
    KEY_KP4,
    KEY_KP1,
    KEY_KP0,
    KEY_KP2,
    KEY_KP3,
    KEY_KPDOT,
    KEY_KP5,
    KEY_KP6,
    KEY_KP8,
    KEY_KP9,
    KEY_KPSLASH,
    KEY_KPASTERISK,
    KEY_KPMINUS,
    KEY_KPPLUS,
    KEY_KPENTER,
];

pub struct Controller {
    descriptor: i32,
    current_state: InputState,
}

impl Controller {
    pub fn new() -> Result<Self, crate::error::CreateController>
    where
        Self: Sized,
    {
        let name = "GW Virtual Keyboard".as_bytes();
        let mut name_slice = [0i8; UINPUT_MAX_NAME_SIZE as usize];
        assert!(name.len() < UINPUT_MAX_NAME_SIZE as usize);
        unsafe {
            libc::memcpy(
                name_slice.as_mut_ptr().cast(),
                name.as_ptr().cast(),
                name.len(),
            );
        }
        let mut device = uinput_user_dev {
            name: name_slice,
            id: input_id {
                bustype: BUS_VIRTUAL,
                vendor: 0,
                product: 0,
                version: 1,
            },
            ff_effects_max: 0,
            absmin: [0i32; ABS_CNT as usize],
            absmax: [0i32; ABS_CNT as usize],
            absflat: [0i32; ABS_CNT as usize],
            absfuzz: [0i32; ABS_CNT as usize],
        };
        let descriptor = unsafe { libc::open("/dev/uinput\0".as_ptr().cast(), libc::O_WRONLY) };
        if descriptor == -1 {
            let error_code = unsafe { *libc::__errno_location() };
            return Err(CreateController::OpenUinput {
                error_code,
                error_text: unsafe {
                    CStr::from_ptr(strerror(error_code))
                        .to_str()
                        .unwrap()
                        .to_string()
                },
            });
        }
        if let Err(err) = create_device(&mut device, descriptor) {
            let _ = unsafe { libc::close(descriptor) };
            return Err(err);
        }
        Ok(Self {
            descriptor,
            current_state: InputState::default(),
        })
    }

    pub fn update(&mut self, state: InputState) {
        for key in KEYS {
            let proto_key = ev_key_to_proto(key);
            let state = if state.pressed_pc_keys.contains(&proto_key) {
                1
            } else {
                0
            };
            write_event(self.descriptor, EV_KEY, key, state);
        }
        write_event(self.descriptor, EV_SYN, SYN_REPORT, 1);
        self.current_state = state;
    }
}

fn ev_key_to_proto(key: libc::c_int) -> Key {
    match key {
        KEY_ESC => Key::Esc,
        KEY_GRAVE => Key::Grave,
        KEY_TAB => Key::Tab,
        KEY_CAPSLOCK => Key::CapsLock,
        KEY_LEFTSHIFT => Key::LeftShift,
        KEY_LEFTCTRL => Key::LeftCtrl,
        KEY_LEFTMETA => Key::LeftMeta,
        KEY_RIGHTMETA => Key::RightMeta,
        KEY_MENU => Key::Menu,
        KEY_RIGHTCTRL => Key::RightCtrl,
        KEY_LEFTALT => Key::LeftAlt,
        KEY_RIGHTALT => Key::AltGr,
        KEY_SPACE => Key::Space,
        KEY_Z => Key::Z,
        KEY_X => Key::X,
        KEY_C => Key::C,
        KEY_V => Key::V,
        KEY_B => Key::B,
        KEY_N => Key::N,
        KEY_M => Key::M,
        KEY_COMMA => Key::Comma,
        KEY_DOT => Key::Dot,
        KEY_SLASH => Key::Slash,
        KEY_RIGHTSHIFT => Key::RightShift,
        KEY_A => Key::A,
        KEY_S => Key::S,
        KEY_D => Key::D,
        KEY_F => Key::F,
        KEY_G => Key::G,
        KEY_H => Key::H,
        KEY_J => Key::J,
        KEY_K => Key::K,
        KEY_L => Key::L,
        KEY_SEMICOLON => Key::Semicolon,
        KEY_APOSTROPHE => Key::Apostrophe,
        KEY_Q => Key::Q,
        KEY_W => Key::W,
        KEY_E => Key::E,
        KEY_R => Key::R,
        KEY_T => Key::T,
        KEY_Y => Key::Y,
        KEY_U => Key::U,
        KEY_I => Key::I,
        KEY_O => Key::O,
        KEY_P => Key::P,
        KEY_LEFTBRACE => Key::LeftBracket,
        KEY_RIGHTBRACE => Key::RightBracket,
        KEY_ENTER => Key::Enter,
        KEY_F1 => Key::F1,
        KEY_F2 => Key::F2,
        KEY_F3 => Key::F3,
        KEY_F4 => Key::F4,
        KEY_F5 => Key::F5,
        KEY_F6 => Key::F6,
        KEY_F7 => Key::F7,
        KEY_F8 => Key::F8,
        KEY_F9 => Key::F9,
        KEY_F10 => Key::F10,
        KEY_F11 => Key::F11,
        KEY_F12 => Key::F12,
        KEY_PRINT => Key::PrintScreen,
        KEY_SCROLLLOCK => Key::ScrollLock,
        KEY_PAUSE => Key::PauseBreak,
        KEY_PLAYPAUSE => Key::Play,
        KEY_PREVIOUSSONG => Key::Previous,
        KEY_NEXTSONG => Key::Next,
        KEY_VOLUMEUP => Key::VolumeUp,
        KEY_VOLUMEDOWN => Key::VolumeDown,
        KEY_MUTE => Key::Mute,
        KEY_STOP => Key::Stop,
        KEY_1 => Key::D1,
        KEY_2 => Key::D2,
        KEY_3 => Key::D3,
        KEY_4 => Key::D4,
        KEY_5 => Key::D5,
        KEY_6 => Key::D6,
        KEY_7 => Key::D7,
        KEY_8 => Key::D8,
        KEY_9 => Key::D9,
        KEY_0 => Key::D0,
        KEY_MINUS => Key::Hyphen,
        KEY_EQUAL => Key::Equals,
        KEY_BACKSLASH => Key::Backslash,
        KEY_BACKSPACE => Key::Back,
        KEY_INSERT => Key::Insert,
        KEY_DELETE => Key::Delete,
        KEY_END => Key::End,
        KEY_UP => Key::Up,
        KEY_DOWN => Key::Down,
        KEY_RIGHT => Key::Right,
        KEY_LEFT => Key::Left,
        KEY_PAGEDOWN => Key::PageDown,
        KEY_HOME => Key::Home,
        KEY_PAGEUP => Key::PageUp,
        KEY_NUMLOCK => Key::NumLock,
        KEY_KP7 => Key::Num7,
        KEY_KP4 => Key::Num4,
        KEY_KP1 => Key::Num1,
        KEY_KP0 => Key::Num0,
        KEY_KP2 => Key::Num2,
        KEY_KP3 => Key::Num3,
        KEY_KPDOT => Key::NumDel,
        KEY_KP5 => Key::Num5,
        KEY_KP6 => Key::Num6,
        KEY_KP8 => Key::Num8,
        KEY_KP9 => Key::Num9,
        KEY_KPSLASH => Key::Divide,
        KEY_KPASTERISK => Key::Multiply,
        KEY_KPMINUS => Key::Subtract,
        KEY_KPPLUS => Key::Add,
        KEY_KPENTER => Key::NumEnter,
        _ => unreachable!(),
    }
}

fn create_device(device: &mut uinput_user_dev, descriptor: i32) -> Result<(), CreateController> {
    unsafe {
        ui_set_evbit(descriptor, EV_KEY as _)?;
    }
    for key in KEYS {
        unsafe { ui_set_keybit(descriptor, key as _) }?;
    }
    if unsafe {
        libc::write(
            descriptor,
            device as *const uinput_user_dev as _,
            std::mem::size_of::<uinput_user_dev>(),
        )
    } == -1
    {
        let error_code = unsafe { *libc::__errno_location() };
        return Err(CreateController::WriteUinput {
            error_code,
            error_text: unsafe {
                CStr::from_ptr(strerror(error_code))
                    .to_str()
                    .unwrap()
                    .to_string()
            },
        });
    }
    unsafe {
        ui_dev_create(descriptor)?;
    }
    Ok(())
}

impl Drop for Controller {
    fn drop(&mut self) {
        unsafe {
            let _ = ui_dev_destroy(self.descriptor);
            libc::close(self.descriptor);
        }
    }
}
