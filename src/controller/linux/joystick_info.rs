use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct JoystickInfo {
    pub name: String,
    pub vendor_id: u16,
    pub product_id: u16,
    pub bus_type: u16,
    pub version: u16,
    pub abs: Option<Vec<libc::c_int>>,
    pub rel: Option<Vec<libc::c_int>>,
    pub btn: Option<Vec<libc::c_int>>,
    pub key: Option<Vec<libc::c_int>>,
}
