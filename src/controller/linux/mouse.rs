use crate::{error::CreateController, input_state::InputState};
use input_linux_sys::{
    input_id, ui_dev_create, ui_dev_destroy, ui_set_evbit, ui_set_keybit, ui_set_relbit,
    uinput_user_dev, ABS_CNT, BTN_LEFT, BTN_MIDDLE, BTN_RIGHT, BUS_VIRTUAL, EV_KEY, EV_REL, EV_SYN,
    REL_WHEEL, REL_X, REL_Y, SYN_REPORT, UINPUT_MAX_NAME_SIZE,
};
use libc::strerror;
use std::ffi::CStr;

use super::write_event;

const REL_INPUTS: [libc::c_int; 3] = [REL_X, REL_Y, REL_WHEEL];

const BUTTON_INPUTS: [libc::c_int; 3] = [BTN_LEFT, BTN_RIGHT, BTN_MIDDLE];

pub struct Controller {
    descriptor: i32,
    current_data: InputState,
}

impl Controller {
    pub fn new() -> Result<Self, crate::error::CreateController>
    where
        Self: Sized,
    {
        let name = "GW Virtual Mouse".as_bytes();
        let mut name_slice = [0i8; UINPUT_MAX_NAME_SIZE as usize];
        assert!(name.len() < UINPUT_MAX_NAME_SIZE as usize);
        unsafe {
            libc::memcpy(
                name_slice.as_mut_ptr().cast(),
                name.as_ptr().cast(),
                name.len(),
            );
        }
        let mut device = uinput_user_dev {
            name: name_slice,
            id: input_id {
                bustype: BUS_VIRTUAL,
                vendor: 0,
                product: 0,
                version: 1,
            },
            ff_effects_max: 0,
            absmin: [0i32; ABS_CNT as usize],
            absmax: [0i32; ABS_CNT as usize],
            absflat: [0i32; ABS_CNT as usize],
            absfuzz: [0i32; ABS_CNT as usize],
        };
        let descriptor = unsafe { libc::open("/dev/uinput\0".as_ptr().cast(), libc::O_WRONLY) };
        if descriptor == -1 {
            let error_code = unsafe { *libc::__errno_location() };
            return Err(CreateController::OpenUinput {
                error_code,
                error_text: unsafe {
                    CStr::from_ptr(strerror(error_code))
                        .to_str()
                        .unwrap()
                        .to_string()
                },
            });
        }
        if let Err(err) = create_device(&mut device, descriptor) {
            let _ = unsafe { libc::close(descriptor) };
            return Err(err);
        }
        Ok(Self {
            descriptor,
            current_data: InputState::default(),
        })
    }

    pub fn update(&mut self, state: InputState) {
        write_event(self.descriptor, EV_REL, REL_X, state.delta_mouse_x);
        write_event(self.descriptor, EV_REL, REL_Y, state.delta_mouse_y);
        write_event(self.descriptor, EV_REL, REL_WHEEL, state.delta_mouse_scroll);
        write_event(
            self.descriptor,
            EV_KEY,
            BTN_LEFT,
            if state.left_mouse_button { 1 } else { 0 },
        );
        write_event(
            self.descriptor,
            EV_KEY,
            BTN_RIGHT,
            if state.right_mouse_button { 1 } else { 0 },
        );
        write_event(
            self.descriptor,
            EV_KEY,
            BTN_MIDDLE,
            if state.middle_mouse_button { 1 } else { 0 },
        );
        write_event(self.descriptor, EV_SYN, SYN_REPORT, 1);
        self.current_data = state;
    }
}

fn create_device(device: &mut uinput_user_dev, descriptor: i32) -> Result<(), CreateController> {
    unsafe {
        ui_set_evbit(descriptor, EV_REL as _)?;
    }
    for rel in REL_INPUTS {
        unsafe { ui_set_relbit(descriptor, rel as _) }?;
    }
    unsafe {
        ui_set_evbit(descriptor, EV_KEY as _)?;
    }
    for key in BUTTON_INPUTS {
        unsafe { ui_set_keybit(descriptor, key as _) }?;
    }
    if unsafe {
        libc::write(
            descriptor,
            device as *const uinput_user_dev as _,
            std::mem::size_of::<uinput_user_dev>(),
        )
    } == -1
    {
        let error_code = unsafe { *libc::__errno_location() };
        return Err(CreateController::WriteUinput {
            error_code,
            error_text: unsafe {
                CStr::from_ptr(strerror(error_code))
                    .to_str()
                    .unwrap()
                    .to_string()
            },
        });
    }
    unsafe {
        ui_dev_create(descriptor)?;
    }
    Ok(())
}

impl Drop for Controller {
    fn drop(&mut self) {
        unsafe {
            let _ = ui_dev_destroy(self.descriptor);
            libc::close(self.descriptor);
        }
    }
}
