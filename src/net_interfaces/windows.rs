use std::ffi::CStr;
use std::{
    alloc::Layout,
    collections::HashSet,
    net::{Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6},
    ptr::{null, null_mut},
};

use windows_sys::Win32::Foundation::ERROR_BUFFER_OVERFLOW;
use windows_sys::Win32::{
    Foundation::{GetLastError, ERROR_SUCCESS},
    NetworkManagement::{
        IpHelper::{
            GetAdaptersAddresses, IF_TYPE_SOFTWARE_LOOPBACK, IP_ADAPTER_ADDRESSES_LH,
            IP_ADAPTER_NO_MULTICAST, IP_ADAPTER_UNICAST_ADDRESS_LH,
        },
        Ndis::IfOperStatusUp,
    },
    Networking::WinSock::{AF_INET, AF_INET6, AF_UNSPEC, SOCKADDR_IN, SOCKADDR_IN6},
    System::{
        Diagnostics::Debug::{
            FormatMessageA, FORMAT_MESSAGE_ALLOCATE_BUFFER, FORMAT_MESSAGE_FROM_SYSTEM,
            FORMAT_MESSAGE_IGNORE_INSERTS,
        },
        Memory::LocalFree,
        SystemServices::{LANG_NEUTRAL, SUBLANG_DEFAULT},
    },
};

use super::Interface;
use crate::error::GetInterfaces;

pub fn get_all() -> Result<Vec<Interface>, GetInterfaces> {
    /*
    https://learn.microsoft.com/en-us/windows/win32/api/iphlpapi/nf-iphlpapi-getadaptersaddresses:

    The recommended method of calling the GetAdaptersAddresses function is to pre-allocate a 15KB
    working buffer pointed to by the AdapterAddresses parameter. On typical computers, this
    dramatically reduces the chances that the GetAdaptersAddresses function returns
    ERROR_BUFFER_OVERFLOW, which would require calling GetAdaptersAddresses function multiple times.
    */
    let mut info_size: u32 = 16384;
    let info_retry_inc_size: u32 = 65536;
    let mut first_adapter: *mut IP_ADAPTER_ADDRESSES_LH;
    let mut adapter_alloc_layout;
    let error_code = loop {
        adapter_alloc_layout = Layout::array::<u8>(info_size as _)?;
        first_adapter = unsafe { std::alloc::alloc(adapter_alloc_layout) }.cast();
        if first_adapter.is_null() {
            return Err(GetInterfaces::HeapAlloc);
        }
        let error_code = unsafe {
            GetAdaptersAddresses(AF_UNSPEC as u32, 0, null_mut(), first_adapter, &mut info_size)
        };
        if error_code != ERROR_BUFFER_OVERFLOW {
            break error_code;
        }
        unsafe {
            std::alloc::dealloc(first_adapter.cast(), adapter_alloc_layout);
        }
        info_size += info_retry_inc_size;
    };
    if error_code != ERROR_SUCCESS {
        unsafe {
            std::alloc::dealloc(first_adapter.cast(), adapter_alloc_layout);
        }
        return Err(GetInterfaces::GetAdaptersAddresses {
            error_text: get_last_error_string(Some(error_code)).unwrap_or_else(|| "".to_owned()),
            error_code,
        });
    }
    if first_adapter.is_null() {
        return Ok(vec![]);
    }
    let mut adapter_list = HashSet::<Interface>::new();
    let mut adapter = unsafe { *first_adapter };
    loop {
        let name_ptr = adapter.FriendlyName;
        let name_len = unsafe { libc::wcslen(name_ptr.cast()) };
        let name =
            String::from_utf16(unsafe { std::slice::from_raw_parts(name_ptr, name_len) }).unwrap();
        let index = unsafe { adapter.Anonymous1.Anonymous.IfIndex };
        let is_loopback = adapter.IfType & IF_TYPE_SOFTWARE_LOOPBACK != 0;
        let supports_multicast = unsafe { adapter.Anonymous2.Flags } & IP_ADAPTER_NO_MULTICAST == 0;
        let is_up = adapter.OperStatus == IfOperStatusUp;

        let first_address: *mut IP_ADAPTER_UNICAST_ADDRESS_LH = adapter.FirstUnicastAddress;
        let addresses = if first_address.is_null() {
            vec![]
        } else {
            let mut address_list: Vec<SocketAddr> = Vec::new();
            let mut address = unsafe { *first_address };
            loop {
                let sock_address = unsafe { *address.Address.lpSockaddr };
                let new_address = if sock_address.sa_family == AF_INET as u16 {
                    let addr_in: SOCKADDR_IN = unsafe { *address.Address.lpSockaddr.cast() };
                    #[cfg(target_endian = "little")]
                    let address_int = unsafe { addr_in.sin_addr.S_un.S_addr.swap_bytes() };
                    #[cfg(target_endian = "big")]
                    let address_int = unsafe { addr_in.sin_addr.S_un.S_addr };
                    Some(SocketAddr::V4(SocketAddrV4::new(
                        Ipv4Addr::from(address_int),
                        addr_in.sin_port,
                    )))
                } else if sock_address.sa_family == AF_INET6 as u16 {
                    let addr_in6: SOCKADDR_IN6 = unsafe { *address.Address.lpSockaddr.cast() };
                    let addr_bytes = unsafe { addr_in6.sin6_addr.u.Byte };

                    Some(SocketAddr::V6(SocketAddrV6::new(
                        Ipv6Addr::from(addr_bytes),
                        addr_in6.sin6_port,
                        addr_in6.sin6_flowinfo,
                        unsafe { addr_in6.Anonymous.sin6_scope_id },
                    )))
                } else {
                    None
                };
                if let Some(addr) = new_address {
                    address_list.push(addr);
                }
                if !address.Next.is_null() {
                    address = unsafe { *address.Next };
                } else {
                    break address_list;
                }
            }
        };
        let new_adapter = Interface {
            index,
            name,
            is_loopback,
            is_up,
            supports_multicast,
            addresses,
        };
        if let Some(mut adapter) = adapter_list.take(&new_adapter) {
            adapter.addresses.extend(new_adapter.addresses);
            adapter_list.insert(adapter);
        } else {
            adapter_list.insert(new_adapter);
        }
        if !adapter.Next.is_null() {
            adapter = unsafe { *adapter.Next };
        } else {
            break;
        }
    }
    unsafe {
        std::alloc::dealloc(first_adapter.cast(), adapter_alloc_layout);
    }

    Ok(adapter_list.into_iter().collect())
}

///Returns the last Win32 error, in string format. Returns an empty string if there is no error.
fn get_last_error_string(code: Option<u32>) -> Option<String> {
    //Get the error message ID, if any.
    let error_code = code.unwrap_or_else(|| unsafe { GetLastError() });
    if error_code == 0 {
        return None;
    }
    let lang_id: u32 = LANG_NEUTRAL | (SUBLANG_DEFAULT << 10);
    let message_buffer: *mut libc::c_char = null_mut();
    // Ask Win32 to give us the string version of that message ID.
    // The parameters we pass in, tell Win32 to create the buffer that holds the message for us
    // (because we don't yet know how long the message string will be).
    let size = unsafe {
        FormatMessageA(
            FORMAT_MESSAGE_ALLOCATE_BUFFER
                | FORMAT_MESSAGE_FROM_SYSTEM
                | FORMAT_MESSAGE_IGNORE_INSERTS,
            null(),
            error_code,
            lang_id,
            message_buffer as _,
            0,
            null(),
        )
    };

    if size == 0 {
        return Some("Can't show error message: FormatMessageA failed".to_string());
    }

    let message = unsafe { CStr::from_ptr(message_buffer) }
        .to_str()
        .unwrap()
        .to_string();

    unsafe {
        LocalFree(message_buffer as _);
    }

    Some(message)
}

#[cfg(test)]
mod test {
    use super::get_last_error_string;

    #[test]
    fn last_error() {
        let _x = get_last_error_string(Some(5));
    }
}
