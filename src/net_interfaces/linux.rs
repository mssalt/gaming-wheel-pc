use std::ffi::CStr;
use std::{
    collections::HashSet,
    net::{Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6},
    ptr::null_mut,
};

use libc::strerror;

use crate::error::GetInterfaces;

use super::Interface;

pub fn get_all() -> Result<Vec<Interface>, GetInterfaces> {
    let mut first_interface_address: *mut libc::ifaddrs = null_mut();
    if unsafe { libc::getifaddrs(&mut first_interface_address) } == -1 {
        let error_code = unsafe { *libc::__errno_location() };
        return Err(GetInterfaces::GetIfAddressesError {
            error_code,
            error_text: unsafe {
                CStr::from_ptr(strerror(error_code))
                    .to_str()
                    .unwrap()
                    .to_string()
            },
        });
    }
    if first_interface_address.is_null() {
        return Ok(vec![]);
    }
    let mut interface_list = HashSet::<Interface>::new();
    let mut interface = unsafe { *first_interface_address };
    loop {
        let name = unsafe { CStr::from_ptr(interface.ifa_name) }
            .to_str()
            .unwrap()
            .to_string();
        let index = match unsafe { libc::if_nametoindex(interface.ifa_name) } {
            0 => {
                unsafe {
                    libc::freeifaddrs(first_interface_address);
                }
                let error_code = unsafe { *libc::__errno_location() };
                return Err(GetInterfaces::GetIndexError {
                    error_code,
                    error_text: unsafe {
                        CStr::from_ptr(strerror(error_code))
                            .to_str()
                            .unwrap()
                            .to_string()
                    },
                });
            }
            index => index,
        };
        let is_loopback = interface.ifa_flags as libc::c_int & libc::IFF_LOOPBACK != 0;
        let supports_multicast = interface.ifa_flags as libc::c_int & libc::IFF_MULTICAST != 0;
        let is_up = interface.ifa_flags as libc::c_int & libc::IFF_UP != 0;
        let sock_address = unsafe { *interface.ifa_addr };
        let addresses = if sock_address.sa_family == libc::AF_INET as u16 {
            // IPv4
            let addr_in: libc::sockaddr_in = unsafe { *interface.ifa_addr.cast() };
            #[cfg(target_endian = "little")]
            let address = addr_in.sin_addr.s_addr.swap_bytes();
            #[cfg(target_endian = "big")]
            let address = addr_in.sin_addr.s_addr;
            vec![SocketAddr::V4(SocketAddrV4::new(
                Ipv4Addr::from(address),
                addr_in.sin_port,
            ))]
        } else if sock_address.sa_family == libc::AF_INET6 as u16 {
            let addr_in6: libc::sockaddr_in6 = unsafe { *interface.ifa_addr.cast() };
            let addr_bytes = addr_in6.sin6_addr.s6_addr;

            vec![SocketAddr::V6(SocketAddrV6::new(
                Ipv6Addr::from(addr_bytes),
                addr_in6.sin6_port,
                addr_in6.sin6_flowinfo,
                addr_in6.sin6_scope_id,
            ))]
        } else {
            vec![]
        };
        let new_interface = Interface {
            index,
            name,
            is_loopback,
            is_up,
            supports_multicast,
            addresses,
        };
        if let Some(mut interface) = interface_list.take(&new_interface) {
            interface.addresses.extend(new_interface.addresses);
            interface_list.insert(interface);
        } else {
            interface_list.insert(new_interface);
        }
        if !interface.ifa_next.is_null() {
            interface = unsafe { *interface.ifa_next };
        } else {
            break;
        }
    }
    unsafe {
        libc::freeifaddrs(first_interface_address);
    }

    Ok(interface_list.into_iter().collect())
}
