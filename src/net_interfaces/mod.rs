use std::{hash::Hash, net::SocketAddr};

#[cfg(target_os = "linux")]
mod linux;
#[cfg(target_os = "windows")]
mod windows;

#[cfg(target_os = "windows")]
pub use crate::net_interfaces::windows::get_all;
#[cfg(target_os = "linux")]
pub use linux::get_all;

#[derive(Debug, Clone)]
pub struct Interface {
    pub index: u32,
    pub name: String,
    pub addresses: Vec<SocketAddr>,
    pub is_loopback: bool,
    pub supports_multicast: bool,
    pub is_up: bool,
}

impl Eq for Interface {}

impl PartialEq for Interface {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Hash for Interface {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}
