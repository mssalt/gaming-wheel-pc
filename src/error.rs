use std::{fmt::Display, net::AddrParseError};

use capnp::NotInSchema;
use fltk::prelude::FltkError;
use thiserror::Error;

use crate::i18n::{get_current_lang, Language};

#[derive(Error, Debug)]
pub enum CreateClient {
    Io {
        #[from]
        source: std::io::Error,
    },
    AddrParse {
        #[from]
        source: AddrParseError,
    },
    FindBestInterface {
        #[from]
        source: FindBestInterface,
    },
    NoIpV4AddressFound {
        interface_name: String,
    },
    GetInterfaces {
        #[from]
        source: GetInterfaces,
    },
}

impl Display for CreateClient {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CreateClient::NoIpV4AddressFound { interface_name } => {
                match get_current_lang() {
                    Language::En => write!(f, "Could not find an IPv4 address in the chosen \
                                           network interface ({interface_name})"),
                    Language::Tr => write!(f, "Seçili ağ arayüzünde ({interface_name}) bir \
                                           IPv4 adresi bulunamadı"),
                    Language::Ru => write!(f, "Не удалось найти адрес IPv4 в выбранном \
                                           сетевом интерфейсе ({interface_name})"),
                    Language::Pt => write!(f, "Não foi possível encontrar um endereço IPv4 na \
                                           interface de rede escolhida ({interface_name})"),
                }
            },
            _ => {
                match get_current_lang() {
                    Language::En => write!(f, "Error starting receiver (check your \
                                  network connection)"),
                    Language::Tr => write!(f, "Alıcı başlatılırken hata \
                                           (ağ bağlantınızı kontrol edin)"),
                    Language::Ru => write!(f, "Ошибка при запуске приемника \
                                           (проверьте подключение к сети)"),
                    Language::Pt => write!(f, "Erro ao iniciar o receptor \
                                           (verifique sua conexão de rede)"),
                }
            },
        }
    }
}

#[derive(Error, Debug)]
pub enum RunFrame {
    ReadInputData {
        #[from]
        source: ReadInputData,
    },
    Io {
        #[from]
        source: std::io::Error,
    },
    Capnp {
        #[from]
        source: capnp::Error,
    },
}

impl Display for RunFrame {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match get_current_lang() {
            Language::En => write!(f, "Error occurred in receiver (check your network connection)"),
            Language::Tr => write!(f, "Alıcıda hata meydana geldi (ağ bağlantınızı kontrol edin)"),
            Language::Ru => write!(f, "Произошла ошибка в приемнике \
                                   (проверьте подключение к сети)"),
            Language::Pt => write!(f, "Ocorreu um erro no receptor \
                                   (verifique sua conexão de rede)"),
        }
    }
}

#[derive(Error, Debug)]
pub enum ReadInputData {
    NetworkProtocolVersionMismatch {
        pc_net_version: u16,
        mobile_net_version: u16,
    },
    NotInSchema {
        #[from]
        source: NotInSchema,
    },
    Capnp {
        #[from]
        source: capnp::Error,
    },
}

impl Display for ReadInputData {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ReadInputData::NetworkProtocolVersionMismatch {
                pc_net_version,
                mobile_net_version,
            } => {
                match get_current_lang() {
                    Language::En => write!(f, "PC app's network protocol version \
                                           ({pc_net_version}) and that of mobile app's \
                                           ({mobile_net_version}) didn't match"),
                    Language::Tr => write!(f, "Bilgisayar uygulamasının ağ protokol sürümü \
                                           ({pc_net_version}) mobil uygulamanınkiyle \
                                           ({mobile_net_version}) eşleşmiyor"),
                    Language::Ru => write!(f, "Версия сетевого протокола приложения для ПК \
                                           ({pc_net_version}) не совпадает с версией мобильного \
                                           приложения ({mobile_net_version})."),
                    Language::Pt => write!(f, "A versão do protocolo de rede do aplicativo para \
                                           PC ({pc_net_version}) e a do aplicativo móvel \
                                           ({mobile_net_version}) não corresponde"),
                }
            },
            _ => {
                match get_current_lang() {
                    Language::En => write!(f, "Error reading incoming input data (make sure \
                                  the protocol versions of the apps are the same)"),
                    Language::Tr => write!(f, "Gelen girdi verisini okurken hata (uygulamaların \
                                  protokol sürümlerinin aynı olduğundan emin olun)"),
                    Language::Ru => write!(f, "Ошибка чтения входящих входных данных (убедитесь, \
                                  что версии протоколов приложений совпадают)"),
                    Language::Pt => write!(f, "Erro ao ler os dados de entrada recebidos \
                                           (certifique-se de que as versões de protocolo dos \
                                            aplicativos são as mesmas)"),
                }
            },
        }
    }
}

#[derive(Error, Debug)]
pub enum FindBestInterface {
    NoSuitableInterface,
}

impl Display for FindBestInterface {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            FindBestInterface::NoSuitableInterface => match get_current_lang() {
                Language::En => write!(f, "Could not find a suitable network interface"),
                Language::Tr => write!(f, "Uygun bir ağ arayüzü bulunamadı"),
                Language::Ru => write!(f, "Не удалось найти подходящий сетевой интерфейс"),
                Language::Pt => write!(f, "Não foi possível encontrar uma interface de \
                                       rede adequada"),
            },
        }
    }
}

#[cfg(target_os = "linux")]
#[derive(Error, Debug)]
pub enum GetInterfaces {
    GetIfAddressesError { error_code: i32, error_text: String },
    GetIndexError { error_code: i32, error_text: String },
}

#[cfg(target_os = "linux")]
impl Display for GetInterfaces {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            GetInterfaces::GetIfAddressesError { error_code, error_text } => {
                match get_current_lang() {
                    Language::En => write!(f, "Call to `getifaddrs` failed with error code \
                                           '{error_code} ({error_text})'"),
                    Language::Tr => write!(f, "`getifaddrs` fonksiyonuna olan çağrı \
                                           '{error_code} ({error_text})' hata koduyla sonuçlandı"),
                    Language::Ru => write!(f, "Вызов `getifaddrs` завершился неудачно с кодом \
                                           ошибки '{error_code} ({error_text})'"),
                    Language::Pt => write!(f, "Chamada para `getifaddrs` falhou com código de \
                                           erro '{error_code} ({error_text})'"),
                }
            },
            GetInterfaces::GetIndexError { error_code, error_text } => {
                match get_current_lang() {
                    Language::En => write!(f, "Call to `if_nametoindex` failed with error code \
                                           '{error_code} ({error_text})'"),
                    Language::Tr => write!(f, "`if_nametoindex` fonksiyonuna olan çağrı \
                                           '{error_code} ({error_text})' hata koduyla sonuçlandı"),
                    Language::Ru => write!(f, "Вызов `if_nametoindex` завершился неудачно с \
                                           кодом ошибки '{error_code} ({error_text})'"),
                    Language::Pt => write!(f, "Chamada para `if_nametoindex` falhou com código \
                                           de erro '{error_code} ({error_text})'"),
                }
            },
        }
    }
}

#[cfg(target_os = "windows")]
#[derive(Error, Debug)]
pub enum GetInterfaces {
    GetAdaptersAddresses {
        error_code: u32,
        error_text: String,
    },
    HeapAlloc,
    Layout {
        #[from]
        source: core::alloc::LayoutError,
    },
}

#[cfg(target_os = "windows")]
impl Display for GetInterfaces {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            GetInterfaces::GetAdaptersAddresses { error_code, error_text } => {
                match get_current_lang() {
                    Language::En => write!(f, "Call to `GetAdaptersAddresses` failed with error \
                                           code '{error_code} ({error_text})'"),
                    Language::Tr => write!(f, "`GetAdaptersAddresses` fonksiyonuna olan \
                                           çağrı '{error_code} ({error_text})' hata koduyla \
                                           sonuçlandı"),
                    Language::Ru => write!(f, "Вызов `GetAdaptersAddresses` завершился неудачно \
                                           с кодом ошибки '{error_code} ({error_text})'"),
                    Language::Pt => write!(f, "A chamada para `GetAdaptersAddresses` falhou com \
                                           o código de erro '{error_code} ({error_text})'"),
                }
            },
            GetInterfaces::HeapAlloc => {
                match get_current_lang() {
                    Language::En => write!(f, "Call to `HeapAlloc` returned null"),
                    Language::Tr => write!(f, "`HeapAlloc` çağrısı null döndürdü"),
                    Language::Ru => write!(f, "Вызов `HeapAlloc` вернул null"),
                    Language::Pt => write!(f, "Chamada para `HeapAlloc` retornou nulo"),
                }
            },
            _ => {
                match get_current_lang() {
                    Language::En => write!(f, "Error getting interfaces"),
                    Language::Tr => write!(f, "Arayüzleri alırken hata"),
                    Language::Ru => write!(f, "Ошибка получения интерфейсов"),
                    Language::Pt => write!(f, "Erro ao obter interfaces"),
                }
            },
        }
    }
}

#[cfg(target_os = "linux")]
#[derive(Error, Debug)]
pub enum CreateController {
    OpenUinput {
        error_code: i32,
        error_text: String,
    },
    WriteUinput {
        error_code: i32,
        error_text: String,
    },
    Ioctl {
        #[from]
        source: input_linux_sys::Error,
    },
    Io {
        #[from]
        source: std::io::Error,
    },
    Lua {
        #[from]
        source: mlua::Error,
    },
}

#[cfg(target_os = "linux")]
impl Display for CreateController {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CreateController::OpenUinput { error_code, error_text } => {
                match get_current_lang() {
                    Language::En => write!(f, "Call to `open(\"/dev/uinput\")` failed with error \
                                           code '{error_code} ({error_text})'"),
                    Language::Tr => write!(f, "`open(\"/dev/uinput\")` fonksiyonuna olan çağrı \
                                           '{error_code} ({error_text})' hata koduyla sonuçlandı"),
                    Language::Ru => write!(f, "Вызов `open(\"/dev/uinput\")` не удался с кодом \
                                           ошибки '{error_code} ({error_text})'"),
                    Language::Pt => write!(f, "A chamada para `open(\"/dev/uinput\")` falhou com \
                                           o código de erro '{error_code} ({error_text})'"),
                }
            },
            CreateController::WriteUinput { error_code, error_text } => {
                match get_current_lang() {
                    Language::En => write!(f, "Call to `write()` for uinput file descriptor \
                                           failed with error code '{error_code} ({error_text})'"),
                    Language::Tr => write!(f, "Uinput dosya tanımlayıcısı için `write()` \
                                           fonksiyonuna olan çağrı '{error_code} ({error_text})' \
                                           hata koduyla sonuçlandı"),
                    Language::Ru => write!(f, "Вызов `write()` для дескриптора файла uinput \
                                           завершился неудачно с кодом ошибки '{error_code} \
                                           ({error_text})'"),
                    Language::Pt => write!(f, "Chamada para `write()` para descritor de arquivo \
                                           uinput falhou com código de erro '{error_code} \
                                           ({error_text})'"),
                }
            },
            _ => {
                match get_current_lang() {
                    Language::En => write!(f, "Error creating virtual joystick feeder"),
                    Language::Tr => write!(f, "Sanal joystick besleyicisi oluşturulurken hata"),
                    Language::Ru => write!(f, "Ошибка при создании виртуального контроллера \
                                           джойстика"),
                    Language::Pt => write!(f, "Erro ao criar alimentador de joystick virtual"),
                }
            },
        }
    }
}

#[cfg(target_os = "windows")]
#[derive(Error, Debug)]
pub enum CreateController {
    VjoyDisabled,
    VersionMismatch {
        dll_version: i16,
        driver_version: i16,
    },
    AcquireVjoy {
        interface_number: u32,
    },
    Io {
        #[from]
        source: std::io::Error,
    },
    Lua {
        #[from]
        source: mlua::Error,
    },
    SelectJoystick {
        #[from]
        source: SelectJoystick,
    },
}

#[cfg(target_os = "windows")]
impl Display for CreateController {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CreateController::VjoyDisabled => {
                match get_current_lang() {
                    Language::En => write!(f, "vJoy driver is disabled or not installed"),
                    Language::Tr => write!(f, "vJoy sürücüsü devre dışı veya kurulu değil"),
                    Language::Ru => write!(f, "Драйвер vJoy отключен или не установлен"),
                    Language::Pt => write!(f, "o driver vJoy está desabilitado ou não instalado"),
                }
            }
            CreateController::VersionMismatch {
                dll_version,
                driver_version,
            } => {
                match get_current_lang() {
                    Language::En => write!(f, "vJoy DLL version '{dll_version}' and the vJoy \
                                           driver version '{driver_version}' don't match"),
                    Language::Tr => write!(f, "vJoy DLL sürümü '{dll_version}' ve vJoy sürücü \
                                           sürümü '{driver_version}' eşleşmiyor"),
                    Language::Ru => write!(f, "Версия vJoy DLL '{dll_version}' и версия драйвера \
                                           vJoy '{driver_version}' не совпадают"),
                    Language::Pt => write!(f, "vJoy DLL versão '{dll_version}' e a versão do \
                                           driver vJoy '{driver_version}' não correspondem"),
                }
            }
            CreateController::AcquireVjoy { interface_number } => {
                match get_current_lang() {
                    Language::En => write!(f, "Failed to acquire vJoy device with interface \
                                           number '{interface_number}'"),
                    Language::Tr => write!(f, "'{interface_number}' arayüz numaralı vJoy \
                                           aygıtına erişim başarısız"),
                    Language::Ru => write!(f, "Не удалось получить устройство vJoy с номером \
                                           интерфейса '{interface_number}'"),
                    Language::Pt => write!(f, "Falha ao adquirir o dispositivo vJoy com o número \
                                           da interface '{interface_number}'"),
                }
            },
            _ => {
                match get_current_lang() {
                    Language::En => write!(f, "Error creating virtual joystick feeder"),
                    Language::Tr => write!(f, "Sanal oyun kolu besleyicisi oluşturulurken hata"),
                    Language::Ru => write!(f, "Ошибка при создании виртуального контроллера \
                                           джойстика"),
                    Language::Pt => write!(f, "Erro ao criar alimentador de joystick virtual"),
                }
            },
        }
    }
}

#[cfg(target_os = "windows")]
#[derive(Error, Debug)]
pub enum SelectJoystick {
    NotExists {
        interface_number: u32,
    },
    NotFree {
        interface_number: u32,
        status_code: i32,
    },
    DisabledAxis {
        interface_number: u32,
        axis: &'static str,
    },
    NotEnoughHats {
        interface_number: u32,
        enabled_count: i32,
        required_count: i32,
    },
    NotEnoughButtons {
        interface_number: u32,
        enabled_count: i32,
        required_count: i32,
    },
}

#[cfg(target_os = "windows")]
impl Display for SelectJoystick {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SelectJoystick::NotExists { interface_number } => {
                match get_current_lang() {
                    Language::En => write!(f, "vJoy interface '{interface_number}' doesn't exist \
                                           (create it in 'Configure vJoy' program)"),
                    Language::Tr => write!(f, "vJoy arayüzü '{interface_number}' yok ('Configure \
                                  vJoy' programında oluşturun)"),
                    Language::Ru => write!(f, "Интерфейс vJoy '{interface_number}' не существует \
                                           (создайте его в программе 'Configure vJoy')"),
                    Language::Pt => write!(f, "a interface vJoy '{interface_number}' não existe \
                                           (crie-a no programa 'Configurar vJoy')"),
                }
            },
            SelectJoystick::NotFree {
                interface_number,
                status_code,
            } => {
                use crate::{controller::vjoy_interface, i18n};
                #[allow(non_snake_case)]
                let description = match *status_code {
                    vjoy_interface::VjdStat_VJD_STAT_OWN => i18n::vjoy_already_owned_status(),
                    vjoy_interface::VjdStat_VJD_STAT_BUSY => i18n::vjoy_busy_status(),
                    vjoy_interface::VjdStat_VJD_STAT_MISS => i18n::vjoy_missing_status(),
                    vjoy_interface::VjdStat_VJD_STAT_UNKN => i18n::vjoy_other_status(),
                    code => unreachable!("Invalid status code ({code}) passed to the error"),
                };
                match get_current_lang() {
                    Language::En => write!(f, "vJoy interface '{interface_number}' has a status \
                                           code '{status_code} {description}' which states \
                                           that it is busy"),
                    Language::Tr => write!(f, "vJoy arayüzünün '{interface_number}' meşgul \
                                           olduğunu ifade eden bir durum kodu var '{status_code} \
                                           {description}'"),
                    Language::Ru => write!(f, "Интерфейс vJoy '{interface_number}' имеет код \
                                           состояния '{status_code} {description}', указывающий, \
                                           что он занят"),
                    Language::Pt => write!(f, "interface vJoy '{interface_number}' tem um código \
                                           de status '{status_code} {description}' que indica \
                                           que está ocupado"),
                }
            },
            SelectJoystick::DisabledAxis { interface_number, axis } => {
                match get_current_lang() {
                    Language::En => write!(f, "Required axis '{axis}' in the interface \
                                           '{interface_number}' is disabled"),
                    Language::Tr => write!(f, "'Arayüzdeki '{interface_number}' gerekli {axis}' \
                                           eksen devre dışı"),
                    Language::Ru => write!(f, "Требуемая ось '{axis}' в интерфейсе \
                                           '{interface_number}' отключена"),
                    Language::Pt => write!(f, "O eixo necessário '{axis}' na interface \
                                           '{interface_number}' está desabilitado"),
                }
            },
            SelectJoystick::NotEnoughButtons {
                interface_number,
                enabled_count,
                required_count,
            } => {
                match get_current_lang() {
                    Language::En => write!(f, "There are not enough buttons enabled in the \
                                           interface '{interface_number}' ({enabled_count} < \
                                           {required_count})"),
                    Language::Tr => write!(f, "Arayüzde '{interface_number}' yeterli sayıda düğme \
                                           aktif değil ({enabled_count} < {required_count})"),
                    Language::Ru => write!(f, "В интерфейсе '{interface_number}' включено \
                                           недостаточно кнопок ({enabled_count} < \
                                           {required_count})"),
                    Language::Pt => write!(f, "Não há botões suficientes ativados na interface \
                                           '{interface_number}' ({enabled_count} < \
                                           {required_count})"),
                }
            },
            _ => {
                match get_current_lang() {
                    Language::En => write!(f, "Error selecting virtual joystick interface"),
                    Language::Tr => write!(f, "Sanal oyun kolu arayüzü seçiminde hata"),
                    Language::Ru => write!(f, "Ошибка выбора интерфейса виртуального джойстика"),
                    Language::Pt => write!(f, "Erro ao selecionar a interface do joystick virtual"),
                }
            },
        }
    }
}

#[cfg(target_os = "windows")]
#[derive(Error, Debug)]
pub enum UpdateJoystick {
    InvalidHatIndex {
        index: u8,
    },
    InvalidButtonIndex {
        index: u8,
    },
    InvalidField {
        field: String,
    },
    Lua {
        #[from]
        source: mlua::Error,
    },
    TryFromInt {
        #[from]
        source: std::num::TryFromIntError,
    },
}

#[cfg(target_os = "windows")]
impl Display for UpdateJoystick {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            UpdateJoystick::InvalidHatIndex { index } => match get_current_lang() {
                Language::En => write!(f, "Hat index({}) in the script is invalid", index),
                Language::Tr => write!(f, "Betikteki şapka indisi({}) geçersiz", index),
                Language::Ru => write!(f, "Индекс шляпы ({index}) в скрипте недействителен"),
                Language::Pt => write!(f, "O índice de chapéu({}) no script é inválido", index),
            },
            UpdateJoystick::InvalidButtonIndex { index } => match get_current_lang() {
                Language::En => write!(f, "Button index({}) in the script is invalid", index),
                Language::Tr => write!(f, "Betikteki buton indisi({}) geçersiz", index),
                Language::Ru => write!(f, "Индекс кнопки ({index}) в скрипте недействителен"),
                Language::Pt => write!(f, "O índice do botão ({}) no script é inválido", index),
            },
            UpdateJoystick::InvalidField { field } => match get_current_lang() {
                Language::En => write!(f, "Invalid field({}) in the script", field),
                Language::Tr => write!(f, "Betikte geçersiz alan({})", field),
                Language::Ru => write!(f, "Недопустимое поле ({field}) в скрипте"),
                Language::Pt => write!(f, "Campo inválido({}) no script", field),
            },
            _ => match get_current_lang() {
                Language::En => write!(f, "Error updating virtual joystick"),
                Language::Tr => write!(f, "Sanal oyun kolu güncellenirken hata"),
                Language::Ru => write!(f, "Ошибка обновления виртуального джойстика"),
                Language::Pt => write!(f, "Erro ao atualizar o joystick virtual"),
            },
        }
    }
}

#[derive(Error, Debug)]
pub struct LoadAppIcon {
    #[from]
    source: FltkError,
}

impl Display for LoadAppIcon {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match get_current_lang() {
            Language::En => write!(f, "Error loading app icon"),
            Language::Tr => write!(f, "Uygulama simgesini yükleme başarısız"),
            Language::Ru => write!(f, "Ошибка загрузки значка приложения"),
            Language::Pt => write!(f, "Erro ao carregar o ícone do aplicativo"),
        }
    }
}

#[derive(Error, Debug)]
pub enum CheckUpdate {
    ParseVersionInfo {
        text: String
    },
    Reqwest {
        #[from]
        source: reqwest::Error,
    },
}

impl Display for CheckUpdate {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::ParseVersionInfo { text: _ }=> match get_current_lang() {
                Language::En => write!(f, "Failed to parse the latest version information"),
                Language::Tr => write!(f, "En son sürüm bilgileri ayrıştırılamadı"),
                Language::Ru => write!(f, "Не удалось проанализировать информацию о \
                                       последней версии"),
                Language::Pt => write!(f, "Falha ao analisar as informações da versão \
                                       mais recente"),
            }
            _ => match get_current_lang() {
                Language::En => write!(f, "Error checking update"),
                Language::Tr => write!(f, "Güncelleme kontrolü başarısız"),
                Language::Ru => write!(f, "Ошибка проверки обновления"),
                Language::Pt => write!(f, "Erro ao verificar atualização"),
            },
        }
    }
}

#[derive(Error, Debug)]
pub enum GetCurrentVersion {
    MajorNotFound {
        version_str: &'static str,
    },
    MinorNotFound {
        version_str: &'static str,
    },
    PatchNotFound {
        version_str: &'static str,
    },
    ParseInt {
        #[from]
        source: std::num::ParseIntError,
    },
}

impl Display for GetCurrentVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::MajorNotFound { version_str } => match get_current_lang() {
                Language::En => write!(f, "Current versions major number not found in \
                                       ({version_str})"),
                Language::Tr => write!(f, "Geçerli sürümün majör numarası \
                                       ({version_str}) içinde bulunamadı"),
                Language::Ru => write!(f, "Основной номер текущей версии не найден в \
                                       ({version_str})"),
                Language::Pt => write!(f, "Número principal das versões atuais não encontrado em \
                                       ({version_str})"),
            }
            Self::MinorNotFound { version_str } => match get_current_lang() {
                Language::En => write!(f, "Current versions minor number not found in \
                                       ({version_str})"),
                Language::Tr => write!(f, "Geçerli sürümün minör numarası ({version_str}) \
                                       içinde bulunamadı"),
                Language::Ru => write!(f, "Второстепенный номер текущей версии не найден в \
                                       ({version_str})"),
                Language::Pt => write!(f, "Número menor das versões atuais não encontrado em \
                                       ({version_str})"),
            }
            Self::PatchNotFound { version_str } => match get_current_lang() {
                Language::En => write!(f, "Current versions patch number not found in \
                                       ({version_str})"),
                Language::Tr => write!(f, "Geçerli sürümün yama numarası ({version_str}) \
                                       içinde bulunamadı"),
                Language::Ru => write!(f, "Номер исправления текущей версии не найден в \
                                       ({version_str})"),
                Language::Pt => write!(f, "Número do patch da versão atual não encontrado em \
                                       ({version_str})"),
            },
            _ => match get_current_lang() {
                Language::En => write!(f, "Error parsing current version from the text"),
                Language::Tr => write!(f, "Geçerli sürüm metinden ayrıştırılırken hata"),
                Language::Ru => write!(f, "Ошибка разбора текущей версии из текста"),
                Language::Pt => write!(f, "Erro ao analisar a versão atual do texto"),
            }
        }
    }
}
