#![windows_subsystem = "windows"]

mod client;
mod client_task;
mod config;
mod controller;
mod error;
mod i18n;
mod input_state;
mod joystick_task;
mod keyboard_task;
mod message;
mod mouse_task;
mod my_app;
mod net_interfaces;
mod proto_capnp;
mod ui;
mod update_check;

use fltk::app::Screen;
use fltk::dialog::alert;
use fltk::prelude::WindowExt;
use i18n::load_i18n;
use my_app::MyApp;
use std::sync::OnceLock;
use std::fmt::Write;

static DATA_DIR: OnceLock<PathBuf> = OnceLock::new();
static CHANNEL_SIZE: usize = 32;
static NETWORK_PROTOCOL_VERSION: u16 = 1;

use std::path::PathBuf;
use std::{error::Error as StdError, panic};

fn main() {
    load_i18n();
    load_data_dir();
    panic::set_hook(Box::new(|info| {
        let current_thread = std::thread::current();
        let msg_header = match current_thread.name() {
            Some(name) => i18n::unexpected_error_in_thread(name),
            None => i18n::unexpected_error().to_owned(),
        };
        let message = format!("{}:\n\n{}", msg_header, info);
        eprintln!("{}", message);
        let dialog_pos = dialog_center_screen();
        alert(dialog_pos.0, dialog_pos.1, &message);
    }));
    tokio::runtime::Runtime::new().unwrap().block_on(async {
        MyApp::run().await.unwrap();
    });
}

fn load_data_dir() {
    let data_dir = match option_env!("APP_DATA_DIR") {
        Some(path_str) => {
            let path = PathBuf::from(path_str);
            if path.is_relative() {
                let mut exe_path = std::env::current_exe().unwrap();
                exe_path.pop();
                exe_path.join(path_str)
            } else {
                path
            }
        }
        None => PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("data"),
    };
    DATA_DIR.set(data_dir).unwrap();
}

pub fn error_message_chain(err: &dyn StdError) -> String {
    let mut message = String::new();
    let mut next_error: &dyn StdError = err;
    let _ = write!(&mut message, "{}", next_error);
    while let Some(e) = next_error.source() {
        let _ = write!(&mut message, ": {}", e);
        next_error = e;
    }
    message
}

pub fn dialog_center_window(window: &dyn WindowExt) -> (i32, i32) {
    (
        (window.x() * 2 + window.w()) / 2 - 420 / 2,
        (window.y() * 2 + window.h()) / 2 - 120 / 2,
    )
}

pub fn dialog_center_screen() -> (i32, i32) {
    let screen_rect = Screen::xywh_mouse();
    (
        (screen_rect.x * 2 + screen_rect.w) / 2 - 420 / 2,
        (screen_rect.y * 2 + screen_rect.h) / 2 - 120 / 2,
    )
}
