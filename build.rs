use std::{env, path::PathBuf, str::FromStr};

fn main() {
    let proto_source = PathBuf::from_str("proto/proto.capnp").unwrap();
    println!("cargo:rerun-if-changed={}", proto_source.to_str().unwrap());
    capnpc::CompilerCommand::new()
        .src_prefix("proto")
        .file("proto/proto.capnp")
        .output_path(PathBuf::from(env::var("OUT_DIR").unwrap()))
        .run()
        .expect("schema compiler command");

    #[cfg(target_os = "windows")]
    {
        build_vjoy_interface();
        set_exe_icon();
    }
}

#[cfg(target_os = "windows")]
fn set_exe_icon() {
    let mut res = winres::WindowsResource::new();
    res.set_icon("data/image/icon.ico");
    res.compile().unwrap();
}

#[cfg(target_os = "windows")]
fn build_vjoy_interface() {
    let lib_var_name = "VJOY_LIB_DIR";
    let lib_folder = PathBuf::from(env::var(lib_var_name).unwrap_or_else(|_| {
        panic!(
            "Please specify the path to the directory which contains vJoyInterface.lib \
    (available in the vJoy SDK) in {} environment variable.",
            lib_var_name
        )
    }));
    // Tell cargo to look for shared libraries in the specified directory
    println!("cargo:rustc-link-search={}", lib_folder.to_str().unwrap());

    // Tell cargo to tell rustc to link the system bzip2
    // shared library.
    println!("cargo:rustc-link-lib=vJoyInterface");

    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=vjoy_header/wrapper.hpp");

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate
        // bindings for.
        .header("vjoy_header/wrapper.hpp")
        // Tell cargo to invalidate the built crate whenever any of the
        // included header files changed.
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/vjoy_bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("vjoy_bindings.rs"))
        .expect("Couldn't write bindings!");
}
